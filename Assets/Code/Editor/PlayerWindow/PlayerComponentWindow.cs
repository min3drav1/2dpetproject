using Code.Runtime.Logic.Player;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Code.Editor.PlayerWindow
{
  public class PlayerComponentWindow : EditorWindow
  {
    private ScrollView _abilityRoot;
    private PlayerCharacter _playerCharacter;
    private ListView _abilitiesActionListView;

    [MenuItem("Tools/AbilityWindow")]
    public static void ShowExample()
    {
      PlayerComponentWindow wnd = GetWindow<PlayerComponentWindow>();
      wnd.titleContent = new GUIContent("AbilityWindow");
    }

    public void CreateGUI()
    {
      VisualElement root = rootVisualElement;
      _abilityRoot = new ScrollView();
      root.Add(_abilityRoot);
      DrawAbilities();
      EditorApplication.playModeStateChanged += OnPlayerModeChanged;
    }

    private void OnPlayerModeChanged(PlayModeStateChange modeStateChange)
    {
      _abilitiesActionListView?.Unbind();
      _abilitiesActionListView = null;
      _abilityRoot.Clear();
    }

    private void DrawAbilities()
    {
      if (_abilityRoot.childCount > 0)
        _abilityRoot.Clear();

      if (_playerCharacter == null)
      {
        return;
      }
      
      _abilityRoot.Add(CreateHealthInspector());
    }
    
    private InspectorElement CreateHealthInspector()
    {
      SerializedObject healthSerializeObject = new SerializedObject(_playerCharacter.Health);
      InspectorElement property = new InspectorElement(healthSerializeObject);
      return property;
    }

    private void Update()
    {
      if (EditorApplication.isPlaying == false)
      {
        _abilityRoot.Clear();
        _playerCharacter = null;
        return;
      }
      
      if (_playerCharacter == null)
      {
        _abilityRoot.Clear();
        _playerCharacter = FindObjectOfType<PlayerCharacter>();
        DrawAbilities();
      }
    }
  }
}