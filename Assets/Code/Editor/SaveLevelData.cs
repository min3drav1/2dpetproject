﻿using System;
using System.Collections.Generic;
using System.IO;
using Code.Runtime.Infrastructure.Services.SceneGraph;
using Code.Runtime.Logic;
using Code.Runtime.Logic.Spawner;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using LevelCollection = Code.Runtime.Infrastructure.Services.SceneGraph.LevelCollection;

namespace Code.Editor
{
  public static class SaveLevelData
  {
    [InitializeOnLoadMethod]
    public static void TestFun()
    {
      EditorSceneManager.sceneSaved += OnSceneSaved;
    }

    private static void OnSceneSaved(Scene scene)
    {
      string sceneName = Path.GetFileNameWithoutExtension(scene.path);

      string[] guidAssets = AssetDatabase.FindAssets($"t:{typeof(LevelCollection)}");
      if (guidAssets == null || guidAssets.Length == 0)
      {
        Debug.LogError("Don't find LevelCollection path");
        return;
      }

      if (guidAssets.Length > 1)
      {
        Debug.LogError("In project several LevelCollection configs");
        return;
      }

      LevelCollection levelCollection = AssetDatabase.LoadAssetAtPath<LevelCollection>(AssetDatabase.GUIDToAssetPath(guidAssets[0]));

      LevelData levelData;
      try
      {
        levelData = levelCollection.GetLevelDataBySceneName(sceneName);
      }
      catch (ArgumentException)
      {
        return;
      }

      
      if (levelData.EnemySpawnData == null)
      {
        levelData.EnemySpawnData = new List<EnemySpawnData>();
      }
      else
      {
        levelData.EnemySpawnData.Clear();
      }

      SpawnPoint spawnPoint = null;
      foreach (GameObject rootGameObject in scene.GetRootGameObjects())
      {
        spawnPoint = rootGameObject.GetComponentInChildren<SpawnPoint>() ?? spawnPoint;
        CollectEnemySpawnData(scene, levelData, rootGameObject);
      }

      SaveSpawnPoint(spawnPoint, levelData);
      EditorUtility.SetDirty(levelCollection);
      AssetDatabase.SaveAssets();
      AssetDatabase.Refresh();
      Debug.Log($"Scene spawn point for scene:{scene.path}");
    }

    private static void SaveSpawnPoint(SpawnPoint spawnPoint, LevelData levelData)
    {
      if (spawnPoint != null)
      {
        levelData.IsHasSpawnPoint = true;
        levelData.SpawnPoint = spawnPoint.Position;
      }
      else
      {
        levelData.IsHasSpawnPoint = false;
      }
    }

    private static void CollectEnemySpawnData(Scene scene, LevelData levelData, GameObject rootGameObject)
    {


      EnemySpawner[] enemySpawners = rootGameObject.GetComponentsInChildren<EnemySpawner>();
      foreach (EnemySpawner enemySpawner in enemySpawners)
      {
        if (enemySpawner.EnemyId == EnemyId.None)
        {
          Debug.LogError($"dont setup enemy id in scene {scene.name}");
          continue;
        }

        levelData.EnemySpawnData.Add(new EnemySpawnData()
        {
          EnemyId = enemySpawner.EnemyId,
          Positions = enemySpawner.transform.position,
          PatrolPoints = enemySpawner.PatrolPoints,
        });
      }
    }
  }
}