﻿using System;
using Code.BehaviorTree.Runtime.Nodes;
using Code.BehaviorTree.Runtime.Nodes.Action;
using Code.BehaviorTree.Runtime.Nodes.Composition;
using Code.BehaviorTree.Runtime.Nodes.Decorator;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Node = Code.BehaviorTree.Runtime.Nodes.Node;

namespace Code.BehaviorTree.Editor
{
  public class NodeView : UnityEditor.Experimental.GraphView.Node
  {
    public Node Node;
    public Port Input;
    public Port Output;

    public Action<NodeView> Selected;

    private const string RootNodeStyleClassName = "root-node";
    private const string ActionNodeStyleClassName = "action-node";
    private const string DecoratorNodeStyleClassName = "decorator-node";
    private const string CompositionNodeStyleClassName = "composition-node";

    private const string RunningClassName = "running-node-state";
    private const string SuccessClassName = "cussess-node-state";
    private const string FailingClassName = "failing-node-state";

    public NodeView(Node node) : base("Assets/Code/Editor/BehaviorTree/NodeView.uxml")
    {
      Node = node;
      title = node.name;
      viewDataKey = node.Guid;

      style.left = node.Position.x;
      style.top = node.Position.y;

      CreateInputPorts();
      CreateOutputPorts();
      SetupClasses();

      Label descriptionLabel = this.Q<Label>("description");
      descriptionLabel.bindingPath = "Description";
      descriptionLabel.Bind(new SerializedObject(Node));
    }

    private void CreateInputPorts()
    {
      Input = Node switch
      {
        ActionNode =>
          InstantiatePort(Orientation.Vertical, Direction.Input, Port.Capacity.Single, typeof(bool)),
        CompositionNode =>
          InstantiatePort(Orientation.Vertical, Direction.Input, Port.Capacity.Single, typeof(bool)),
        DecoratorNode =>
          InstantiatePort(Orientation.Vertical, Direction.Input, Port.Capacity.Single, typeof(bool)),
        _ => Input
      };

      if (Input != null)
      {
        Input.portName = "";
        Input.style.flexDirection = FlexDirection.Row;
        inputContainer.Add(Input);
      }
    }

    private void CreateOutputPorts()
    {
      Output = Node switch
      {
        CompositionNode =>
          InstantiatePort(Orientation.Vertical, Direction.Output, Port.Capacity.Multi, typeof(bool)),
        DecoratorNode =>
          InstantiatePort(Orientation.Vertical, Direction.Output, Port.Capacity.Single, typeof(bool)),
        RootNode =>
          InstantiatePort(Orientation.Vertical, Direction.Output, Port.Capacity.Single, typeof(bool)),
        _ => Output
      };

      if (Output != null)
      {
        Output.portName = "";
        Output.style.flexDirection = FlexDirection.Row;
        outputContainer.Add(Output);
      }
    }

    private void SetupClasses()
    {
      switch (Node)
      {
        case RootNode:
          AddToClassList(RootNodeStyleClassName);
          break;
        case ActionNode:
          AddToClassList(ActionNodeStyleClassName);
          break;
        case CompositionNode:
          AddToClassList(CompositionNodeStyleClassName);
          break;
        case DecoratorNode:
          AddToClassList(DecoratorNodeStyleClassName);
          break;
      }
    }

    public override void SetPosition(Rect newPos)
    {
      base.SetPosition(newPos);
      Undo.RecordObject(Node, "Behaviour Tree (Set Position)");
      Node.Position.x = newPos.xMin;
      Node.Position.y = newPos.yMin;
      EditorUtility.SetDirty(Node);
    }

    public override void OnSelected()
    {
      base.OnSelected();
      Selected?.Invoke(this);
    }

    public void SortChildren()
    {
      CompositionNode compositionNode = Node as CompositionNode;

      if (compositionNode == null)
      {
        return;
      }

      compositionNode.Children.Sort(SortByHorizontalPosition);
    }

    public void UpdateState()
    {
      if (Application.isPlaying == false)
      {
        return;
      }

      RemoveFromClassList(RunningClassName);
      RemoveFromClassList(FailingClassName);
      RemoveFromClassList(SuccessClassName);
      
      switch (Node.State)
      {
        case NodeState.Running:
          if(Node.IsStart)
            AddToClassList(RunningClassName);
          break;
        case NodeState.Falling:
          AddToClassList(FailingClassName);
          break;
        case NodeState.Success:
          AddToClassList(SuccessClassName);
          break;
        default:
          throw new ArgumentOutOfRangeException();
      }
    }
    private int SortByHorizontalPosition(Node left, Node right)
    {
      return left.Position.x < right.Position.x ? -1 : 1;
    }
  }
}