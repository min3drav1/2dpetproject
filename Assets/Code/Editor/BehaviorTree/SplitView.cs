﻿using UnityEngine.UIElements;

namespace Code.BehaviorTree.Editor
{
  public class SplitView : TwoPaneSplitView
  {
    public new class UxmlFactory : UxmlFactory<SplitView, TwoPaneSplitView.UxmlTraits>
    {
    }
    
  }
}