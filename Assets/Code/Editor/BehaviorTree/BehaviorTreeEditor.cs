using System;
using Code.BehaviorTree.Runtime;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Code.BehaviorTree.Editor
{
  public class BehaviorTreeEditor : EditorWindow
  {
    [SerializeField] private VisualTreeAsset _visualTreeAsset = default;
    [SerializeField] private StyleSheet _styleSheet;
    
    private InspectorView _inspectorView;
    private BehaviorTreeView _behaviorTreeView;
    private SerializedObject _treeObject;
    private SerializedProperty _blackboardProperty;
    private IMGUIContainer _blackboardView;

    [MenuItem("Tools/BehaviourTreeEditor")]
    public static void ShowExample()
    {
      BehaviorTreeEditor wnd = GetWindow<BehaviorTreeEditor>();
      wnd.titleContent = new GUIContent("BehaviourTreeEditor");
    }

    public void CreateGUI()
    {
      VisualElement root = rootVisualElement;
      root.styleSheets.Add(_styleSheet);
      _visualTreeAsset.CloneTree(root);

      _inspectorView = root.Q<InspectorView>();
      _behaviorTreeView = root.Q<BehaviorTreeView>();
      _blackboardView = root.Q<IMGUIContainer>();
      _behaviorTreeView.SelectedNodeView = SelectedNodeView;
      _blackboardView.onGUIHandler = () =>
      {
        if (_treeObject == null || _treeObject.targetObject == null)
        {
          return;
        }
        _treeObject.Update();
        EditorGUILayout.PropertyField(_blackboardProperty);
        _treeObject.ApplyModifiedProperties();
      };
      
      OnSelectionChange();
    }

    private void OnEnable()
    {
      EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
      EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
    }

    private void OnDisable()
    {
      EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
    }

    private void OnPlayModeStateChanged(PlayModeStateChange playModeStateChange)
    {
      switch (playModeStateChange)
      {
        case PlayModeStateChange.EnteredEditMode:
          OnSelectionChange();
          break;
        case PlayModeStateChange.ExitingEditMode:
          break;
        case PlayModeStateChange.EnteredPlayMode:
          OnSelectionChange();
          break;
        case PlayModeStateChange.ExitingPlayMode:
          break;
        default:
          throw new ArgumentOutOfRangeException(nameof(playModeStateChange), playModeStateChange, null);
      }
    }

    private void SelectedNodeView(NodeView nodeView)
    {
      _inspectorView.UpdateInspector(nodeView);
    }

    private void OnSelectionChange()
    {
      Runtime.BehaviorTree behaviorTree = Selection.activeObject as Runtime.BehaviorTree;
      if (behaviorTree == null)
      {
        if (Selection.activeGameObject != null &&
            Selection.activeGameObject.TryGetComponent(out BehaviourRunner behaviourRunner))
        {
          behaviorTree = behaviourRunner.BehaviorTree;
        }
        else
        {
          return;
        }
      }

      if(behaviorTree != null &&
         (Application.isPlaying || AssetDatabase.CanOpenAssetInEditor(behaviorTree.GetInstanceID())))
      {
        _behaviorTreeView?.PopulateView(behaviorTree);
      }

      if (behaviorTree != null)
      {
        _treeObject = new SerializedObject(behaviorTree);
        _blackboardProperty = _treeObject.FindProperty("Blackboard");
      }
    }

    private void OnInspectorUpdate()
    {
      _behaviorTreeView?.UpdateNodesState();
    }
  }
}