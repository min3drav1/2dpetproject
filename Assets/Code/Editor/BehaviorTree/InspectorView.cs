﻿using UnityEngine.UIElements;

namespace Code.BehaviorTree.Editor
{
  public class InspectorView : VisualElement
  {
    private UnityEditor.Editor _editor;
    
    public new class UxmlFactory : UxmlFactory<InspectorView, VisualElement.UxmlTraits>
    {
    }
    
    public InspectorView()
    {
      
    }

    public void UpdateInspector(NodeView nodeView)
    {
      Clear();
      UnityEngine.Object.DestroyImmediate(_editor);
      _editor = UnityEditor.Editor.CreateEditor(nodeView.Node);
      IMGUIContainer container = new IMGUIContainer(() =>
      {
        if(_editor != null && _editor.target != null)
          _editor.OnInspectorGUI();
      });
      Add(container);
    }
  }
}