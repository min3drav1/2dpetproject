﻿using System;
using System.Collections.Generic;
using System.Linq;
using Code.BehaviorTree.Runtime.Nodes;
using Code.BehaviorTree.Runtime.Nodes.Action;
using Code.BehaviorTree.Runtime.Nodes.Composition;
using Code.BehaviorTree.Runtime.Nodes.Decorator;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;
using Node = Code.BehaviorTree.Runtime.Nodes.Node;

namespace Code.BehaviorTree.Editor
{
  public class BehaviorTreeView : GraphView
  {
    private Runtime.BehaviorTree _behaviorTree;

    public new class UxmlFactory : UxmlFactory<BehaviorTreeView, GraphView.UxmlTraits>
    {
    }

    public Action<NodeView> SelectedNodeView;
    public BehaviorTreeView()
    {
      Insert(0, new GridBackground());
      
      this.AddManipulator(new ContentZoomer());
      this.AddManipulator(new ContentDragger());
      this.AddManipulator(new SelectionDragger());
      this.AddManipulator(new RectangleSelector());

      Undo.undoRedoPerformed += UndoRedoPerformed;
    }

    public void PopulateView(Runtime.BehaviorTree behaviorTree)
    {
      _behaviorTree = behaviorTree;

      if (_behaviorTree.RootNode == null)
      {
        _behaviorTree.RootNode = _behaviorTree.CreateNode(typeof(RootNode)) as RootNode;
        EditorUtility.SetDirty(_behaviorTree);
        AssetDatabase.SaveAssets();
      }
      
      graphViewChanged -= OnGraphViewChanged;
      DeleteElements(graphElements);
      graphViewChanged += OnGraphViewChanged;
      foreach (Node node in _behaviorTree.Nodes)
      {
        CreateNodeView(node);
      }

      foreach (Node node in _behaviorTree.Nodes)
      {
        NodeView parentNodeView = FindNodeView(node);
        foreach (Node child in _behaviorTree.GetChildren(node))
        {
          NodeView childNodeView = FindNodeView(child);
          Edge edge = parentNodeView.Output.ConnectTo(childNodeView.Input);
          AddElement(edge);
        }
      }

    }

    public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
    {
      return ports.Where(endPoint => 
        endPoint.direction != startPort.direction &&
        endPoint.node != startPort.node).ToList();
    }

    public void UpdateNodesState()
    {
      foreach (UnityEditor.Experimental.GraphView.Node node in nodes)
      {
        if (node is NodeView nodeView)
        {
          nodeView.UpdateState();
        }
      }
    }

    public override void BuildContextualMenu(ContextualMenuPopulateEvent evt)
    {
      AddActionToContext<ActionNode>(evt);
      AddActionToContext<DecoratorNode>(evt);
      AddActionToContext<CompositionNode>(evt);
    }

    private void UndoRedoPerformed()
    {
      PopulateView(_behaviorTree);
      AssetDatabase.SaveAssets();
    }

    private NodeView FindNodeView(Node node)
    {
      return GetNodeByGuid(node.Guid) as NodeView;
    }

    private GraphViewChange OnGraphViewChanged(GraphViewChange graphViewChange)
    {
      if (graphViewChange.elementsToRemove != null)
      {
        foreach (GraphElement element in graphViewChange.elementsToRemove)
        {
          if (element is NodeView nodeView)
          {
            _behaviorTree.DeleteNode(nodeView.Node);
          }

          if (element is Edge edge)
          {
            NodeView parentNodeView = edge.output.node as NodeView;
            NodeView childNodeView = edge.input.node as NodeView;
            _behaviorTree.RemoveChild(parentNodeView.Node, childNodeView.Node);
          }
        }
      }

      if (graphViewChange.edgesToCreate != null)
      {
        foreach (Edge edge in graphViewChange.edgesToCreate)
        {
          NodeView parentNodeView = edge.output.node as NodeView;
          NodeView childNodeView = edge.input.node as NodeView;
          _behaviorTree.AddChild(parentNodeView.Node, childNodeView.Node);
        }
      }

      if (graphViewChange.movedElements != null)
      {
        foreach (UnityEditor.Experimental.GraphView.Node node in nodes)
        {
          if (node is NodeView nodeView)
          {
            nodeView.SortChildren();
          }
        }
      }
      
      return graphViewChange;
    }

    private void AddActionToContext<TNode>(ContextualMenuPopulateEvent evt)
    {
      TypeCache.TypeCollection types = TypeCache.GetTypesDerivedFrom<TNode>();
      foreach (Type type in types)
      {
        evt.menu.AppendAction($"{type.BaseType.Name}/{type.Name}", action => CreateNode(type));
      }
    }

    private void CreateNode(Type type)
    {
      Node node = _behaviorTree.CreateNode(type);
      CreateNodeView(node);
    }

    private void CreateNodeView(Node node)
    {
      NodeView nodeView = new NodeView(node);
      nodeView.Selected = SelectionChange;
      AddElement(nodeView);
    }

    private void SelectionChange(NodeView nodeView)
    {
      SelectedNodeView?.Invoke(nodeView);
    }
  }
}