﻿using System.IO;
using Code.Runtime;
using Code.Runtime.Infrastructure.Services.SaveLoad;
using UnityEditor;
using UnityEngine;

namespace Code.Editor
{
  public static class Tools
  {
    [MenuItem("Tools/Delete saved progress file")]
    public static void ClearProgress()
    {
      File.Delete(FileSaveLoadService.PersistantProgressFilePath);
      PlayerPrefs.DeleteKey(Constants.ProgressKey);
    }
  }
}