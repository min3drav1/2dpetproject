﻿using Code.Runtime.Logic;
using Code.Runtime.Logic.Spawner;
using UnityEditor;
using UnityEngine;

namespace Code.Editor
{
  [CustomEditor(typeof(SceneData))]
  public class SceneDataEditor : UnityEditor.Editor
  {
    private SerializedProperty _enemySpawners;
    private SerializedProperty _levelTransitions;
    private SerializedProperty _saveTriggers;
    private SerializedProperty _spawnPointProperty;

    private void OnEnable()
    {
      _spawnPointProperty = serializedObject.FindProperty("_spawnPoints");
      _enemySpawners = serializedObject.FindProperty("_enemySpawners");
      _levelTransitions = serializedObject.FindProperty("_levelTransitions");
      _saveTriggers = serializedObject.FindProperty("_saveTriggers");
    }

    public override void OnInspectorGUI()
    {
      serializedObject.Update();
      DrawDefaultInspector();

      if (GUILayout.Button("Collect data on level"))
      {
        CollectSceneData<SpawnPoint>(_spawnPointProperty);
        CollectSceneData<EnemySpawner>(_enemySpawners);
        CollectSceneData<LevelTrigger>(_levelTransitions);
        CollectSceneData<SaveTrigger>(_saveTriggers);
      }

      serializedObject.ApplyModifiedProperties();
    }

    private static void CollectSceneData<DataType>(SerializedProperty arrayProperty) where DataType : MonoBehaviour
    {
      DataType[] all = FindObjectsOfType<DataType>();
      arrayProperty.arraySize = all.Length;

      for (int i = 0; i < all.Length; ++i)
      {
        SerializedProperty property = arrayProperty.GetArrayElementAtIndex(i);
        property.objectReferenceValue = all[i];
      }
    }
  }
}