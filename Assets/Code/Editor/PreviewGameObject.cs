﻿using UnityEngine;

namespace Code.Editor
{
  public class PreviewGameObject
  {
    public static GameObject Preview;

    public static void InstantiatePreview(string pathAsset)
    {
      if (Preview != null)
        Object.DestroyImmediate(Preview);

      Preview = Object.Instantiate(Resources.Load<GameObject>(pathAsset));
      Preview.hideFlags = HideFlags.HideAndDontSave;
    }

    public static void DestroyPreview()
    {
      Object.DestroyImmediate(Preview);
      Preview = null;
    }
  }
}