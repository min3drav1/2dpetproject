﻿using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Code.Editor.PlayerHealth
{
  [CustomEditor(typeof(Runtime.Logic.Player.PlayerHealth))]
  public class PlayerHealthEditor : UnityEditor.Editor
  {
    [SerializeField] private VisualTreeAsset _visualTreeAsset;

    private IntegerField _addHeartsField;
    private IntegerField _removeHeartsField;
    
    private IntegerField _addMaxHeartsField;
    private IntegerField _removeMaxHeartsField;

    private SliderInt _maxHealthSlider;

    private Runtime.Logic.Player.PlayerHealth _health;


    public override VisualElement CreateInspectorGUI()
    {
      if (_visualTreeAsset == null)
      {
        return new Label("Don't have visual tree asset");
      }
      
      _health = target as Runtime.Logic.Player.PlayerHealth;
      VisualElement root = new VisualElement();
      VisualElement container = _visualTreeAsset.Instantiate();
      root.Add(container);
      
      IntegerField currentHealth = container.Q<IntegerField>("CurrentValue");
      IntegerField maxHealth = container.Q<IntegerField>("MaxValue");
      FloatField cooldownTime = container.Q<FloatField>("CooldownTime");
      RadioButton isCooldown = container.Q<RadioButton>("IsCooldown");
      
      container.Q<Button>("AddHealthButton").RegisterCallback<ClickEvent>(AddHealth);
      _addHeartsField = container.Q<IntegerField>("AddHealthCountField");

      container.Q<Button>("RemoveHealthButton").RegisterCallback<ClickEvent>(RemoveHearth);
      _removeHeartsField = container.Q<IntegerField>("RemoveHealthCountField");  
      
      container.Q<Button>("AddMaxHealthButton").RegisterCallback<ClickEvent>(AddMaxHealth);
      _addMaxHeartsField = container.Q<IntegerField>("AddMaxHealthCountField");

      container.Q<Button>("RemoveMaxHealthButton").RegisterCallback<ClickEvent>(RemoveMaxHearth);
      _removeMaxHeartsField = container.Q<IntegerField>("RemoveMaxHealthCountField");
      

      container.Q<Button>("DeadButton").RegisterCallback<ClickEvent>(DeadFox);

      currentHealth.bindingPath = "_current";
      maxHealth.bindingPath = "_maxValue";
      cooldownTime.bindingPath = "_cooldownTime";
      isCooldown.bindingPath = "<IsCooldown>k__BackingField";
      
      currentHealth.SetEnabled(false);
      maxHealth.SetEnabled(false);
      isCooldown.SetEnabled(false);

      AddDefaultInspector(root);
      
      return root;
    }
    
    private void AddHealth(ClickEvent evt)
    {
      int addHealth = Mathf.Clamp(_addHeartsField.value, 0, _health.MaxValue - _health.Current);
      _addHeartsField.value = addHealth;
      _health.AddHeart(addHealth);
    }

    private void RemoveHearth(ClickEvent evt)
    {
      int addHealth = Mathf.Clamp(_removeHeartsField.value, 0, _health.Current);
      _removeHeartsField.value = addHealth;
      _health.TakeDamage(null, addHealth);
    }

    private void RemoveMaxHearth(ClickEvent evt)
    {
      int addMaxHealth = Mathf.Clamp(_addMaxHeartsField.value, 0, 10);
      _addHeartsField.value = addMaxHealth;
      _health.MaxValue += addMaxHealth;
    }

    private void AddMaxHealth(ClickEvent evt)
    {
      int removeMaxHealth = Mathf.Clamp(_removeMaxHeartsField.value, 0, 10);
      _removeMaxHeartsField.value = removeMaxHealth;
      _health.MaxValue += removeMaxHealth;
    }

    private void DeadFox(ClickEvent evt)
    {
      _health.TakeDamage(null, _health.Current);
    }

    private void AddDefaultInspector(VisualElement root)
    {
      Foldout foldout = new Foldout();
      foldout.text = "Default inspector";
      foldout.viewDataKey = "DefaultInspectorFoxHealth";
      foldout.value = false;
      root.Add(foldout);
      InspectorElement.FillDefaultInspector(foldout, serializedObject, this);
    }
  }
}