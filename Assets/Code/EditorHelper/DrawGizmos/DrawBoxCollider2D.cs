#if UNITY_EDITOR
using UnityEngine;

namespace Code.EditorHelper.DrawGizmos
{
  [RequireComponent(typeof(BoxCollider2D))]
  public class DrawBoxCollider2D : MonoBehaviour
  {
    [SerializeField] private BoxCollider2D _box;
    [SerializeField] private Color _color;

    private void Reset()
    {
      _box = GetComponent<BoxCollider2D>();
    }

    private void OnDrawGizmos()
    {
      Gizmos.color = _color;
      Gizmos.DrawCube(
        transform.position + (Vector3)_box.offset,
        _box.size);
    }
  }
}
#endif