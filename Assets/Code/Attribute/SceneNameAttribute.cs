﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Code.Attribute
{
  public class SceneNameAttribute : PropertyAttribute
  {
  }

#if UNITY_EDITOR

  [CustomPropertyDrawer(typeof(SceneNameAttribute))]
  public class SceneNameDrawer : PropertyDrawer
  {
    private string[] _allScenes;

    public override VisualElement CreatePropertyGUI(SerializedProperty property)
    {
      VisualElement root = new VisualElement();
      root.Add(new PropertyField(property));
      if (property.type != "string")
      {
        Label label = new Label($"{property.displayName} should be string type");
        label.AddToClassList("--unity-colors-error-text");
        root.Add(label);
      }
      else
      {
        string currentSceneName = property.stringValue;
        List<string> sceneNames = new List<string>();

        int defaultIndex = CollectSceneName(property, sceneNames, currentSceneName);

        DropdownField dropdownList = new DropdownField("Scene name", sceneNames, defaultIndex, null, FormatListItem);
        dropdownList.BindProperty(property);
        root.Add(dropdownList);
      }

      return root;
    }

    private string FormatListItem(string itemScene)
    {
      return _allScenes.First(scene => Path.GetFileNameWithoutExtension(scene) == itemScene);
    }

    private int CollectSceneName(SerializedProperty property, List<string> sceneNames, string currentSceneName)
    {
      int defaultIndex = -1;
      string[] allScenes = AssetDatabase.FindAssets("t:scene", new[] { "Assets/Scenes/Levels" });
      _allScenes = allScenes.Select(scene =>
        AssetDatabase
          .GUIDToAssetPath(scene)
          .Replace("Assets/Scenes/Levels/", "")
          .Replace(".unity", ""))
        .ToArray();

      for (int i = 0; i < _allScenes.Length; i++)
      {
        string sceneName = Path.GetFileNameWithoutExtension(_allScenes[i]);
        sceneNames.Add(sceneName);
        if (currentSceneName == sceneName)
        {
          defaultIndex = i;
        }
      }

      if (defaultIndex == -1)
      {
        defaultIndex = 0;
        property.stringValue = sceneNames[0];
      }

      return defaultIndex;
    }
  }

#endif
}