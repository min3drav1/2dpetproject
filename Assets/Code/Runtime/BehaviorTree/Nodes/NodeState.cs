﻿namespace Code.BehaviorTree.Runtime.Nodes
{
  public enum NodeState
  {
    Running = 1,
    Falling = 2,
    Success = 3,
  }
}