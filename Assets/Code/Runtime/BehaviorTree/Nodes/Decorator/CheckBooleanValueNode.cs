﻿using UnityEngine;

namespace Code.BehaviorTree.Runtime.Nodes.Decorator
{
  public class CheckBooleanValueNode : DecoratorNode
  {
    [SerializeField] private bool _isTrue;
    [SerializeField] private string _key;
    
    public override void OnStart()
    {
    }

    public override void OnStop()
    {
    }

    public override NodeState OnUpdate()
    {
      if (Blackboard.TryGet(_key, out bool value))
      {
        if (value == _isTrue)
        {
          return Child.Update();
        }
      }

      return NodeState.Falling;
    }
  }
}