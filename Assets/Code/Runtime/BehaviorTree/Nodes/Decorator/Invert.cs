﻿using System;

namespace Code.BehaviorTree.Runtime.Nodes.Decorator
{
  public class Invert : DecoratorNode
  {
    public override void OnStart()
    {
    }

    public override void OnStop()
    {
    }

    public override NodeState OnUpdate()
    {
      return Child.Update() switch
      {
        NodeState.Success => NodeState.Falling,
        NodeState.Falling => NodeState.Success,
        NodeState.Running => NodeState.Running,
        _ => throw new ArgumentOutOfRangeException()
      };
    }
  }
}