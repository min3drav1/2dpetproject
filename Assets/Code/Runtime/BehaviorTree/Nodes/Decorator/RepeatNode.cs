﻿namespace Code.BehaviorTree.Runtime.Nodes.Decorator
{
  public class RepeatNode : DecoratorNode
  {
    public override void OnStart()
    {
    }

    public override void OnStop()
    {
    }

    public override NodeState OnUpdate()
    {
      Child.Update();
      return NodeState.Running;
    }
  }
}