﻿using UnityEngine;

namespace Code.BehaviorTree.Runtime.Nodes.Decorator
{
  public abstract class DecoratorNode : Node
  {
    [HideInInspector] public Node Child;
    
    public override Node Clone()
    {
      DecoratorNode node = base.Clone() as DecoratorNode;
      node.Child = Child.Clone();
      return node;
    }
  }
}