﻿using System;
using UnityEngine;

namespace Code.BehaviorTree.Runtime.Nodes.Composition
{
  public class SelectorNode : CompositionNode
  {
    private int _currentIndex;
    
    public override void OnStart()
    {
      _currentIndex = 0;
    }

    public override NodeState OnUpdate()
    {
      if (Children == null || Children.Count == 0)
      {
        Debug.LogError("Sequencer node have any child");
        return NodeState.Falling;
      }
      
      Node child = Children[_currentIndex];
      switch (child.Update())
      {
        case NodeState.Running:
          return NodeState.Running;
        case NodeState.Falling:
          _currentIndex++;
          break;
        case NodeState.Success:
          return NodeState.Success;
        default:
          throw new ArgumentOutOfRangeException();
      }
      
      return IsLastInQueue() ? NodeState.Running : NodeState.Falling ;
    }
    
    private bool IsLastInQueue() => 
      _currentIndex < Children.Count;
  }
}