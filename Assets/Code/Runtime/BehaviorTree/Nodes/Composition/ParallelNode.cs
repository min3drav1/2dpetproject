﻿using System;
using UnityEngine;

namespace Code.BehaviorTree.Runtime.Nodes.Composition
{
  public class ParallelNode : CompositionNode
  {
    public override NodeState OnUpdate()
    {
      int successCount = 0;
      int fallingCount = 0;
      if (Children == null || Children.Count == 0)
      {
        Debug.LogError("Sequencer node have any child");
        return NodeState.Falling;
      }

      foreach (Node child in Children)
      {
        if (child.IsStart && child.State == NodeState.Success)
        {
          successCount++;
          continue;
        }
        switch (child.Update())
        {
          case NodeState.Running:
            break;
          case NodeState.Falling:
            ++fallingCount;
            break;
          case NodeState.Success:
            ++successCount;
            break;
          default:
            throw new ArgumentOutOfRangeException();
        }
      }

      if (fallingCount > 0)
      {
        return NodeState.Falling;
      }
      if (successCount == Children.Count)
      {
        return NodeState.Success;
      }
      return NodeState.Running;
    }
  }
}