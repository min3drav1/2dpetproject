﻿using System.Collections.Generic;
using UnityEngine;

namespace Code.BehaviorTree.Runtime.Nodes.Composition
{
  public abstract class CompositionNode : Node
  {
    [HideInInspector] public List<Node> Children = new List<Node>();
    
    public override Node Clone()
    {
      CompositionNode node = base.Clone() as CompositionNode;
      node.Children = Children.ConvertAll(child => child.Clone());
      return node;
    }
    
    public override void OnStart()
    {
    }

    public override void OnStop()
    {
      foreach (Node child in Children)
      {
        child.ResetNode();
      }
    }
  }
}