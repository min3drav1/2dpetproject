﻿using UnityEngine;

namespace Code.BehaviorTree.Runtime.Nodes
{
  public abstract class Node : ScriptableObject
  {
    public NodeState State { get; protected set; } = NodeState.Running;
    public bool IsStart { get; private set; }
    [HideInInspector] public string Guid;
    [HideInInspector] public Vector2 Position;
    [HideInInspector] public Blackboard Blackboard;
    [HideInInspector] public AiAgent AiAgent;
    [TextArea] public string Description;

    public virtual void Initialize(Blackboard blackboard, AiAgent aiAgent)
    {
      Blackboard = blackboard;
      AiAgent = aiAgent;
    }
    
    public NodeState Update()
    {
      if (IsStart == false)
      {
        OnStart();
        IsStart = true;
      }

      State = OnUpdate();
      
      if (State == NodeState.Falling ||
          State == NodeState.Success)
      {
        OnStop();
        IsStart = false;
      }

      return State;
    }

    public virtual Node Clone()
    {
      Node clone = Instantiate(this);
      clone.name = clone.name.Replace("(Clone)", "");
      return clone;
    }

    public void ResetNode()
    {
      OnStop();
      State = NodeState.Falling;
      IsStart = false;
    }
    
    public abstract void OnStart();
    public abstract void OnStop();
    public abstract NodeState OnUpdate();
  }
}