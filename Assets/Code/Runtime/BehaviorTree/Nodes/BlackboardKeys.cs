﻿namespace Code.BehaviorTree.Runtime.Nodes
{
  public static class BlackboardKeys
  {
    public const string PatrolPoints = "PatrolPoints";
    public const string MoveToPoint = "MoveToPoint";
  }
}