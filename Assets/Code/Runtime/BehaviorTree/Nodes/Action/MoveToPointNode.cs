﻿using System;
using Code.Runtime.Logic.Enemy;
using UnityEngine;

namespace Code.BehaviorTree.Runtime.Nodes.Action
{
  public class MoveToPointNode : ActionNode
  {
    [SerializeField] private string _key;
    [SerializeField] private float _minDistanceToPoint = 0.5f;
    
    private Vector2 _targetPoint;
    private EnemyMovement Movement => AiAgent.Movement;

    
    private bool _isFindValue;

    public override void OnStart()
    {
      if (string.IsNullOrEmpty(_key))
      {
        throw new Exception($"Node MoveTo don't setup key");
      }
      
      _isFindValue = Blackboard.TryGet<Vector2>(_key, out _targetPoint);
      Vector2 moveToDirection = _targetPoint - (Vector2)Movement.transform.position;
      float horizontalDirection = Mathf.Sign(moveToDirection.x);
      Movement.Move(horizontalDirection);
    }

    public override void OnStop()
    {
      Movement.Stop();
    }

    public override NodeState OnUpdate()
    {
      if (_isFindValue == false)
      {
        return NodeState.Falling;
      }
      
      Vector2 moveToDirection = _targetPoint - (Vector2)Movement.transform.position;
      float horizontalDirection = Mathf.Sign(moveToDirection.x);
      
      if (moveToDirection.sqrMagnitude <= _minDistanceToPoint * _minDistanceToPoint)
      {
        return NodeState.Success;
      }

      Movement.Move(horizontalDirection);
      return NodeState.Running;
    }

  }
}