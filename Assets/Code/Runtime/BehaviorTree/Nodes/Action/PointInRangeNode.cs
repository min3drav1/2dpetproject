﻿using System;
using UnityEngine;

namespace Code.BehaviorTree.Runtime.Nodes.Action
{
  public class PointInRangeNode : ActionNode
  {
    [SerializeField] private string _key;
    [SerializeField] private float _range;
    
    public override void OnStart()
    {
      if (string.IsNullOrEmpty(_key))
      {
        throw new Exception("PointInRangeNode don't have key");
      }
    }

    public override void OnStop()
    {
    }

    public override NodeState OnUpdate()
    {
      float sqrDistance = (Blackboard.Get<Vector2>(_key) - (Vector2)AiAgent.Movement.transform.position).SqrMagnitude();
      return sqrDistance <= _range * _range ? NodeState.Success : NodeState.Falling;
    }
  }
}