﻿using UnityEngine;

namespace Code.BehaviorTree.Runtime.Nodes.Action
{
  public class DebugLogNode : ActionNode
  {
    [SerializeField] private string _startMessage;
    [SerializeField] private string _stopMessage;
    [SerializeField] private string _updateMessage;

    public override void OnStart()
    {
      if (string.IsNullOrEmpty(_startMessage) == false)
        Debug.Log($"OnStart:{_startMessage}");
    }

    public override void OnStop()
    {
      if (string.IsNullOrEmpty(_stopMessage) == false)
        Debug.Log($"OnStop:{_stopMessage}");
    }

    public override NodeState OnUpdate()
    {
      if (string.IsNullOrEmpty(_updateMessage) == false)
        Debug.Log($"OnUpdate:{_updateMessage}");
      return NodeState.Success;
    }
  }
}