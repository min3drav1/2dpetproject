﻿using System;
using UnityEngine;

namespace Code.BehaviorTree.Runtime.Nodes.Action
{
  public class SetPlayerPointNode : ActionNode
  {
    [SerializeField] private string _key;
    
    public override void OnStart()
    {
      if (string.IsNullOrEmpty(_key))
      {
        throw new Exception("SetPlayerPointNode don't have key");
      }
    }

    public override void OnStop()
    {
    }

    public override NodeState OnUpdate()
    {
      if (AiAgent.PlayerDetection.IsDetected == false)
      {
        return NodeState.Falling;
      }
      Blackboard.Set<Vector2>(_key, AiAgent.PlayerDetection.Target.position);
      return NodeState.Success;
    }
  }
}