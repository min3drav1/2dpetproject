﻿using UnityEngine;

namespace Code.BehaviorTree.Runtime.Nodes.Action
{
  public class WaitNode : ActionNode
  {
    public float Duration = 1;
    private float _startTime;
    
    public override void OnStart()
    {
      _startTime = Time.time;
    }

    public override void OnStop()
    {
    }

    public override NodeState OnUpdate()
    {
      return IsFinishWait() ? NodeState.Success : NodeState.Running;
    }

    private bool IsFinishWait() => 
      Time.time - _startTime >= Duration;
  }
}