﻿namespace Code.BehaviorTree.Runtime.Nodes.Action
{
  public class StopMoveNode : ActionNode
  {
    public override void OnStart()
    {
    }

    public override void OnStop()
    {
    }

    public override NodeState OnUpdate()
    {
      AiAgent.Movement.Stop();
      return NodeState.Success;
    }
  }
}