﻿using Code.Runtime.Logic.Enemy;
using Code.Runtime.Logic.Player;
using UnityEngine;

namespace Code.BehaviorTree.Runtime.Nodes.Action
{
  public class OneJumpToPointNode : ActionNode
  {
    [SerializeField] private string _key;
    
    private EnemyMovement Movement => AiAgent.Movement;
    private GroundChecker GroundChecker => AiAgent.GroundChecker;

    private Vector2 _targetPoint;

    private bool _isJumpDone = false;
    private const float HorizontalMinDistance = 0.25f;

    public override void OnStart()
    {
      _isJumpDone = false;
    }

    public override void OnStop()
    {
    }

    public override NodeState OnUpdate()
    {
      if (_isJumpDone == false)
      {
        if(GroundChecker.IsGrounded == false)
        {
          return NodeState.Falling;
        }
        
        Movement.Jump();
        _isJumpDone = true;
        return NodeState.Running;
      }

      if (GroundChecker.IsGrounded)
      {
        return NodeState.Success;
      }

      Vector2 targetPoint = Blackboard.Get<Vector2>(_key);
      Vector2 moveToDirection = targetPoint - (Vector2)Movement.transform.position;
      float horizontalDirection = Mathf.Sign(moveToDirection.x);
      
      if(Mathf.Abs(moveToDirection.x) > HorizontalMinDistance)
      {
        Movement.Move(horizontalDirection);
      } 

      
      return NodeState.Running;
    }
  }
}