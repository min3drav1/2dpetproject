﻿using Code.Runtime.Logic.Enemy;

namespace Code.BehaviorTree.Runtime.Nodes.Action
{
  public class PlayerDetectionNode : ActionNode
  {
    private PlayerDetection PlayerDetection => AiAgent.PlayerDetection;
    
    public override void OnStart()
    {
    }

    public override void OnStop()
    {
    }

    public override NodeState OnUpdate()
    {
      
      return PlayerDetection.IsDetected ? NodeState.Success : NodeState.Falling;
    }
  }
}