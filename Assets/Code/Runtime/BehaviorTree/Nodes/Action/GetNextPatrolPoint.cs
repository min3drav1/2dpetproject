﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Code.BehaviorTree.Runtime.Nodes.Action
{
  public class GetNextPatrolPoint : ActionNode
  {
    [SerializeField] private string _key;
    private int _pointIndex = 0;
    private List<Vector2> _points;

    private const float MinDistance = 0.5f;

    public override void Initialize(Blackboard blackboard, AiAgent aiAgent)
    {
      base.Initialize(blackboard, aiAgent);
      _points = blackboard.Get<List<Vector2>>(BlackboardKeys.PatrolPoints);
    }
    
    public override void OnStart()
    {
      if (string.IsNullOrEmpty(_key))
      {
        throw new Exception($"Node GetNextPatrolPoint don't setup key");
      }
    }

    public override void OnStop()
    {
    }

    public override NodeState OnUpdate()
    {
      Vector2 transformPosition = _points[_pointIndex] - (Vector2) AiAgent.Movement.transform.position;
      float sqrMagnitude = Vector2.SqrMagnitude(transformPosition);
      if (sqrMagnitude < MinDistance)
      {
        _pointIndex = (_pointIndex + 1) % _points.Count;
      }

      Blackboard.Set<Vector2>(_key, _points[_pointIndex]);
      
      return NodeState.Success;
    }
  }
}