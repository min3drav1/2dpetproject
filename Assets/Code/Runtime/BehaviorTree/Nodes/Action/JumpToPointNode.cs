﻿using Code.Runtime.Logic.Enemy;
using Code.Runtime.Logic.Player;
using UnityEngine;

namespace Code.BehaviorTree.Runtime.Nodes.Action
{
  public class JumpToPointNode : ActionNode
  {
    [SerializeField] private float _minDistanceToPoint = 0.5f;
    [SerializeField] private string _key;
    private EnemyMovement Movement => AiAgent.Movement;
    private GroundChecker GroundChecker => AiAgent.GroundChecker;

    private Vector2 _targetPoint;
    private const float HorizontalMinDistance = 0.25f;

    public override void OnStart()
    {
      _targetPoint = Blackboard.Get<Vector2>(_key);
    }

    public override void OnStop()
    {
    }

    public override NodeState OnUpdate()
    {
      Vector2 moveToDirection = _targetPoint - (Vector2)Movement.transform.position;
      float horizontalDirection = Mathf.Sign(moveToDirection.x);

      if (GroundChecker.IsGrounded)
      {
        if (moveToDirection.sqrMagnitude < _minDistanceToPoint * _minDistanceToPoint)
        {
          Movement.Stop();
          return NodeState.Success;
        }

        Movement.Jump();
      }

      if(Mathf.Abs(moveToDirection.x) > HorizontalMinDistance)
      {
        Movement.Move(horizontalDirection);
      }
      
      return NodeState.Running;
    }
  }
}