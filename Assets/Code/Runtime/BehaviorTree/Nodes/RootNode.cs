﻿namespace Code.BehaviorTree.Runtime.Nodes
{
  public class RootNode : Node
  {
    public Node Child;
    
    public override void OnStart()
    {
    }

    public override void OnStop()
    {
    }

    public override NodeState OnUpdate()
    {
      Child.Update();
      return NodeState.Running;
    }

    public override Node Clone()
    {
      RootNode node = base.Clone() as RootNode;
      node.Child = Child.Clone();
      return node;
    }
  }
}