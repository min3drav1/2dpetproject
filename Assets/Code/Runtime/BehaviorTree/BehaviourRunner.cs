﻿using UnityEngine;

namespace Code.BehaviorTree.Runtime
{
  public class BehaviourRunner : MonoBehaviour
  {
    [SerializeField] private AiAgent _aiAgent;
    [field: SerializeField] public BehaviorTree BehaviorTree { get; private set; }
    [field: SerializeField] public Blackboard Blackboard { get; private set; }

    private bool _isRun;

    private void Awake()
    {
      enabled = false;
    }

    public void Run()
    {
      BehaviorTree = BehaviorTree.Clone();
      BehaviorTree.Bind(_aiAgent, Blackboard);
      enabled = true;
    }

    private void Update()
    {
      BehaviorTree.Update();
    }
  }
}