﻿using System;
using System.Collections.Generic;
using Code.BehaviorTree.Runtime.Nodes;
using Code.BehaviorTree.Runtime.Nodes.Composition;
using Code.BehaviorTree.Runtime.Nodes.Decorator;
using UnityEditor;
using UnityEngine;

namespace Code.BehaviorTree.Runtime
{
  [CreateAssetMenu(fileName = "New Behaviour Tree", menuName = "BehaviourTree/BehaviourTree")]
  public class BehaviorTree : ScriptableObject
  {
    public Node RootNode;
    public NodeState TreeState = NodeState.Running;
    public List<Node> Nodes = new List<Node>();
    public Blackboard Blackboard;

    public NodeState Update()
    {
      if (RootNode.State == NodeState.Running)
      {
        TreeState = RootNode.Update();
      }

      return TreeState;
    }

    public BehaviorTree Clone()
    {
      BehaviorTree behaviorTree = Instantiate(this);
      behaviorTree.RootNode = RootNode.Clone();
      behaviorTree.Nodes = new List<Node>();
      Traverse(behaviorTree.RootNode, node =>
        behaviorTree.Nodes.Add(node));

      return behaviorTree;
    }

    public void Bind(AiAgent aiAgent, Blackboard blackboard)
    {
      Blackboard = blackboard;
      foreach (Node node in Nodes)
      {
        node.Initialize(blackboard, aiAgent);
      }
    }

    private void Traverse(Node node, Action<Node> visitor)
    {
      if (node == null)
      {
        return;
      }

      visitor.Invoke(node);
      foreach (Node child in GetChildren(node))
      {
        Traverse(child, visitor);
      }
    }

    public Node CreateNode(Type type)
    {
      Node node = CreateInstance(type) as Node;
      node.name = type.Name;
      node.Guid = GUID.Generate().ToString();
      Undo.RecordObject(this, "Behaviour Tree (CreateNode)");
      Nodes.Add(node);

      if (Application.isPlaying == false)
      {
        AssetDatabase.AddObjectToAsset(node, this);
      }

      Undo.RegisterCreatedObjectUndo(node, "Behaviour Tree (CreateNode)");
      AssetDatabase.SaveAssets();
      return node;
    }

    public void DeleteNode(Node node)
    {
      Undo.RecordObject(this, "Behaviour Tree (DeleteNode)");
      Nodes.Remove(node);
      Undo.DestroyObjectImmediate(node);
      AssetDatabase.SaveAssets();
    }

    public void AddChild(Node parent, Node child)
    {
      if (parent is DecoratorNode decoratorNode)
      {
        Undo.RecordObject(decoratorNode, "Behaviour Tree (AddChild)");
        decoratorNode.Child = child;
        EditorUtility.SetDirty(decoratorNode);
      }

      if (parent is RootNode rootNode)
      {
        Undo.RecordObject(rootNode, "Behaviour Tree (AddChild)");
        rootNode.Child = child;
        EditorUtility.SetDirty(rootNode);
      }

      if (parent is CompositionNode compositionNode)
      {
        Undo.RecordObject(compositionNode, "Behaviour Tree (AddChild)");
        compositionNode.Children.Add(child);
        EditorUtility.SetDirty(compositionNode);
      }
    }

    public void RemoveChild(Node parent, Node child)
    {
      if (parent is DecoratorNode decoratorNode)
      {
        Undo.RecordObject(decoratorNode, "Behaviour Tree (RemoveChild)");
        decoratorNode.Child = null;
        EditorUtility.SetDirty(decoratorNode);
      }

      if (parent is RootNode rootNode)
      {
        Undo.RecordObject(rootNode, "Behaviour Tree (RemoveChild)");
        rootNode.Child = null;
        EditorUtility.SetDirty(rootNode);
      }


      if (parent is CompositionNode compositionNode)
      {
        Undo.RecordObject(compositionNode, "Behaviour Tree (RemoveChild)");
        compositionNode.Children.Remove(child);
        EditorUtility.SetDirty(compositionNode);
      }
    }

    public List<Node> GetChildren(Node parent)
    {
      List<Node> children = new();
      if (parent is DecoratorNode decoratorNode && decoratorNode.Child != null)
      {
        children.Add(decoratorNode.Child);
      }

      if (parent is RootNode rootNode && rootNode.Child != null)
      {
        children.Add(rootNode.Child);
      }


      if (parent is CompositionNode compositionNode)
      {
        return compositionNode.Children;
      }

      return children;
    }
  }
}