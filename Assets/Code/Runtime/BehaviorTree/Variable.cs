﻿namespace Code.BehaviorTree.Runtime
{
  public class Variable<T> : BaseVariable
  {
    public T Value;
  }
}