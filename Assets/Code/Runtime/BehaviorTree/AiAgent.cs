﻿using Code.Runtime.Logic.Enemy;
using Code.Runtime.Logic.Player;
using UnityEngine;

namespace Code.BehaviorTree.Runtime
{
  public class AiAgent : MonoBehaviour
  {
    [field: SerializeField] public EnemyMovement Movement { get; private set; }
    [field: SerializeField] public PlayerDetection PlayerDetection{ get; private set; }
    [field: SerializeField] public GroundChecker GroundChecker { get; private set; }
  }
}