﻿using System;
using System.Collections.Generic;

namespace Code.BehaviorTree.Runtime
{
  [Serializable]
  public class Blackboard
  {
    private Dictionary<string, BaseVariable> _variables = new();

    public TValue Get<TValue>(string key)
    {
      if (_variables.TryGetValue(key, out BaseVariable baseVariable) == false)
      {
        throw new ArgumentException($"Blackboard don't have value with key:{key}");
      }
      Variable<TValue> variable = baseVariable as Variable<TValue>;

      if (variable == null)
      {
        throw new Exception($"Value with key {key} has type{baseVariable.GetType().Name} but try get {typeof(TValue)} type");
      }
      
      return variable.Value;
    }

    public bool TryGet<TValue>(string key, out TValue value)
    {
      if (_variables.TryGetValue(key, out BaseVariable baseVariable) == false)
      {
        value = default;
        return false;
      }
      Variable<TValue> variable = baseVariable as Variable<TValue>;

      if (variable == null)
      {
        throw new Exception($"Value with key {key} has type{baseVariable.GetType().Name} but try get {typeof(TValue)} type");
      }

      value = variable.Value;
      return true;
    }
    

    public void Set<TValue>(string key, TValue value)
    {
      Variable<TValue> variable;
      if (_variables.TryGetValue(key, out BaseVariable baseVariable))
      {
        variable = baseVariable as Variable<TValue>;

        if (variable == null)
        {
          throw new Exception($"Value with key {key} has type{baseVariable.GetType().Name} but try get {typeof(TValue)} type");
        }
      }
      else
      {
        variable = new Variable<TValue>();
        _variables[key] = variable;
      }
      
      variable.Value = value;
      
    }
  }
}