﻿using System;
using Code.Runtime.StaticData;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Runtime.UI
{
  public class PurchaseItem : MonoBehaviour
  {
    [SerializeField] private Image _icon;
    [SerializeField] private TextMeshProUGUI _text;
    [SerializeField] private TextMeshProUGUI _price;
    [SerializeField] private Button _button;

    public event Action<PurchaseItem> OnClicked;

    public int Price { get; private set; }
    public PurchaseActionId ActionId { get; private set; }

    private void OnDestroy()
    {
      _button.onClick.RemoveListener(BuyAbility);
    }

    public void Initialize(string text, int price, PurchaseActionId actionId, Sprite sprite = null)
    {
      Price = price;
      if (sprite != null)
        _icon.sprite = sprite;
      ActionId = actionId;
      _text.text = text;

      _price.text = Price.ToString();
      _button.onClick.AddListener(BuyAbility);
    }

    private void BuyAbility()
    {
      OnClicked?.Invoke(this);
    }

    public void SetInteractable(bool interactable)
    {
      _button.interactable = interactable;
    }
  }
}