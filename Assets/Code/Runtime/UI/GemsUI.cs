﻿using Code.Runtime.Infrastructure.Services.PersistantProgress.Data;
using TMPro;
using UnityEngine;

namespace Code.Runtime.UI
{
  public class GemsUI : MonoBehaviour
  {
    [SerializeField] private TextMeshProUGUI _text;
    private CollectedData _collectedData;

    private void OnDestroy()
    {
      _collectedData.Changed -= ChangedGems;
    }

    public void Constructor(CollectedData collectedData)
    {
      _collectedData = collectedData;

      _collectedData.Changed += ChangedGems;
      ChangedGems();
    }

    private void ChangedGems()
    {
      _text.text = _collectedData.Gems.ToString();
    }
  }
}