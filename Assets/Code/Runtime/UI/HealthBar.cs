﻿using System.Collections.Generic;
using Code.Runtime.Logic.Player;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Runtime.UI
{
  public class HealthBar : MonoBehaviour
  {
    [SerializeField] private Image _heartInstance;
    [SerializeField] private Color _activeColor;
    [SerializeField] private Color _inactiveColor;
    [SerializeField] private int _heartWidth = 100;

    private readonly List<Image> _hearts = new();
    private PlayerHealth _playerHealth;
    private int _healthValue = 0;
    private int _maxHealthValue = 0;

    private void OnDestroy()
    {
      if (_playerHealth == null)
        return;
      
      _playerHealth.HpChanged -= UpdateCurrentHealthValue;
      _playerHealth.MaxHpChanged -= UpdateMaxHearts;

    }

    public void Constructor(PlayerHealth playerHealth)
    {
      _playerHealth = playerHealth;
      _playerHealth.HpChanged += UpdateCurrentHealthValue;
      _playerHealth.MaxHpChanged += UpdateMaxHearts;
      
      Reset();
    }

    public void Reset()
    {
      UpdateMaxHearts();
      UpdateCurrentHealthValue();
    }

    private void UpdateCurrentHealthValue()
    {
      if (_playerHealth.Current == _healthValue)
      {
        return;
      }

      if (_playerHealth.Current < _healthValue)
      {
        for (int i = _playerHealth.Current; i < _healthValue; i++)
        {
          _hearts[i].color = _inactiveColor;
        }
      }
      else
      {
        for (int i = _healthValue; i < _playerHealth.Current; i++)
        {
          _hearts[i].color = _activeColor;
        }
      }

      _healthValue = _playerHealth.Current;
    }

    private void UpdateMaxHearts()
    {
      if (_playerHealth.MaxValue == _hearts.Count)
      {
        return;
      }

      if (_playerHealth.MaxValue < _maxHealthValue)
      {
        for (int i = _playerHealth.MaxValue; i < _maxHealthValue; i++)
        {
          _hearts[i].gameObject.SetActive(false);
        }
      }
      else
      {
        for (int i = _maxHealthValue; i < _playerHealth.MaxValue; i++)
        {
          if (i < _hearts.Count)
          {
            _hearts[i].gameObject.SetActive(true);
          }
          else
          {
            Image heart = CreateNewHeart(new Vector3(_heartWidth * i, 0, 0));
            _hearts.Add(heart);
          }
        }
      }

      _maxHealthValue = _playerHealth.MaxValue;
      UpdateCurrentHealthValue();
    }

    private Image CreateNewHeart(Vector3 position)
    {
      Image heart = Instantiate(_heartInstance);
      heart.transform.localPosition = position;
      heart.transform.SetParent(transform, false);
      return heart;
    }
  }
}