﻿using System;
using Code.Runtime.Infrastructure.Services.Input;
using Code.Runtime.Infrastructure.Services.PersistantProgress.Data;
using Code.Runtime.Infrastructure.Services.Windows;
using Code.Runtime.Logic.Player;
using Code.Runtime.StaticData;
using UnityEngine;

namespace Code.Runtime.UI
{
  public class Hud : MonoBehaviour
  {
    private IInputSystem _input;
    private IWindowsService _windowsService;

    [SerializeField] private GemsUI _gemsUI;
    [SerializeField] private HealthBar _healthBar;

    public void Constructor(IWindowsService windowsService, IInputSystem inputSystem, PlayerHealth playerHealth, CollectedData collectedData)
    {
      _input = inputSystem;
      _windowsService = windowsService;
      
      _input.PressAbilityWindow += OnPressedOpenAbility;
      _input.PressMenuWindow += OnPressedOpenMenu;
      
      _gemsUI.Constructor(collectedData);
      _healthBar.Constructor(playerHealth);
    }

    private void OnDestroy()
    {
      _input.PressAbilityWindow -= OnPressedOpenAbility;
      _input.PressMenuWindow -= OnPressedOpenMenu;
    }

    public void Reset()
    {
      _healthBar.Reset();
    }

    private void OnPressedOpenAbility()
    {
      _windowsService.Open(WindowID.Abilities);
    }

    private void OnPressedOpenMenu()
    {
      _windowsService.Open(WindowID.Menu);
    }
  }
}