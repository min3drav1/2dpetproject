﻿using Code.Runtime.Infrastructure.StateMachine;
using Code.Runtime.Infrastructure.StateMachine.State;

namespace Code.Runtime.UI.Window
{
  public class GameOverWindow : BaseWindow
  {
    private IGameStateMachine _stateMachine;

    public void Constructor(IGameStateMachine stateMachine)
    {
      _stateMachine = stateMachine;
    }

    protected override void Subscribe()
    {
      base.Subscribe();
      _closeButton.onClick.AddListener(ReloadGame);
    }

    protected override void Unsubscribe()
    {
      base.Unsubscribe();
      _closeButton.onClick.RemoveListener(ReloadGame);
    }

    private void ReloadGame()
    {
      _stateMachine.Enter<LoadProgressState>();
    }
  }
}