﻿using System.Collections.Generic;
using Code.Runtime.Infrastructure.Services.PersistantProgress.Data;
using Code.Runtime.Infrastructure.Services.StaticData;
using Code.Runtime.Purchase;
using Code.Runtime.StaticData;
using UnityEngine;

namespace Code.Runtime.UI.Window
{
  public class PurchaseWindow : BaseWindow
  {
    [SerializeField] private PurchaseItem _purchaseItemInstance;
    [SerializeField] private Transform _content;

    private readonly List<PurchaseItem> _items = new();
    private PlayerProgress _playerProgress;

    private IPurchaseService _purchaseService;
    private IStaticDataService _staticDataService;

    public void Initialize(IStaticDataService staticDataService, PlayerProgress playerPrefs,
      IPurchaseService purchaseService)
    {
      _staticDataService = staticDataService;
      _playerProgress = playerPrefs;
      _purchaseService = purchaseService;
    }

    protected override void OnOpen()
    {
      InitializeItems(_staticDataService.GetAbilityBuyItems());
      SetInteractableItems();
    }

    protected override void Subscribe()
    {
      base.Subscribe();
      foreach (PurchaseItem item in _items)
        item.OnClicked += BuyAbility;
    }

    protected override void Unsubscribe()
    {
      base.Unsubscribe();
      foreach (PurchaseItem item in _items)
        item.OnClicked -= BuyAbility;
    }

    private void InitializeItems(IEnumerable<PurchaseActionData> abilitiesData)
    {
      foreach (PurchaseActionData abilityData in abilitiesData)
      {
        PurchaseItem item = Instantiate(_purchaseItemInstance, _content);

        item.Initialize(
          abilityData.Text,
          abilityData.Price,
          abilityData.ActionId,
          abilityData.Icon);

        _items.Add(item);
      }
    }

    private void SetInteractableItems()
    {
      foreach (PurchaseItem item in _items)
        item.SetInteractable(CanBuyItem(item));
    }

    private void BuyAbility(PurchaseItem item)
    {
      if (_purchaseService.IsValid(item.ActionId) && IsEnoughGems(item))
      {
        _purchaseService.Execute(item.ActionId);
        Payment(item);
        SetInteractableItems();
      }
    }

    private void Payment(PurchaseItem item)
    {
      _playerProgress.CollectedData.ChangeGemCountAt(-item.Price);
    }

    private bool IsEnoughGems(PurchaseItem item)
    {
      return _playerProgress.CollectedData.Gems >= item.Price;
    }

    private bool CanBuyItem(PurchaseItem item)
    {
      return IsEnoughGems(item) && _purchaseService.IsValid(item.ActionId);
    }
  }
}