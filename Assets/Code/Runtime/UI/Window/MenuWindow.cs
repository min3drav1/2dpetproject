﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.UI;

namespace Code.Runtime.UI.Window
{
  public class MenuWindow : BaseWindow
  {
    [SerializeField] private Button _gameEnd;

    protected override void OnOpen()
    {
      Time.timeScale = 0;
    }

    protected override void OnClose()
    {
      Time.timeScale = 1;
    }

    protected override void Subscribe()
    {
      base.Subscribe();
      _gameEnd.onClick.AddListener(ExitGame);
    }

    private void ExitGame()
    {
#if UNITY_EDITOR
      EditorApplication.isPlaying = false;
#else
      Application.Quit();
#endif
    }

    protected override void Unsubscribe()
    {
      base.Unsubscribe();
      _gameEnd.onClick.RemoveListener(ExitGame);
    }
  }
}