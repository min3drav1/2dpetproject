﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Runtime.UI.Window
{
  public abstract class BaseWindow : MonoBehaviour
  {
    [SerializeField] protected Button _closeButton;

    private void Awake()
    {
      Close();
    }

    public void Open()
    {
      gameObject.SetActive(true);
      OnOpen();
      Subscribe();
    }

    public void Close()
    {
      gameObject.SetActive(false);
      OnClose();
      Unsubscribe();
    }

    public bool IsOpen()
    {
      return gameObject.activeSelf;
    }


    protected virtual void OnOpen()
    {
    }

    protected virtual void OnClose()
    {
    }

    protected virtual void Subscribe()
    {
      _closeButton.onClick.AddListener(Close);
    }

    protected virtual void Unsubscribe()
    {
      _closeButton.onClick.RemoveListener(Close);
    }
  }
}