﻿using UnityEngine;

namespace Code.Runtime.Logic
{
  [RequireComponent(typeof(Collider2D))]
  public class SingTrigger : MonoBehaviour
  {
    [SerializeField] private GameObject _sign;
    private int _playerLayer;

    private void Awake()
    {
      _playerLayer = LayerMask.NameToLayer("Player");
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
      if (other.gameObject.layer == _playerLayer)
      {
        _sign.SetActive(true);
      }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
      if (other.gameObject.layer == _playerLayer)
      {
        _sign.SetActive(false);
      }
    }
  }
}