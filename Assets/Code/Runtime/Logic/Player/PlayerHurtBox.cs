﻿using Code.Runtime.Logic.Enemy;
using UnityEngine;

namespace Code.Runtime.Logic.Player
{
  public class PlayerHurtBox : MonoBehaviour
  {
    [SerializeField] private Color _color;
    [SerializeField] private Vector3 _offset;
    [SerializeField] private float _radius;
    [SerializeField] private LayerMask _layerMask;
    [SerializeField] private bool _drawGizmos = false;

    private readonly Collider2D[] _collisionColliders = new Collider2D[8];

    private Vector3 HitPosition => transform.position + Vector3.Scale(_offset, transform.localScale);

    private void Hit()
    {
      int count = Physics2D.OverlapCircleNonAlloc(HitPosition, _radius, _collisionColliders,
        _layerMask);
      if (count == 0)
      {
        return;
      }

      for (int i = 0; i < count; i++)
      {
        if (_collisionColliders[i].attachedRigidbody != null &&
            _collisionColliders[i].attachedRigidbody.TryGetComponent(out IEnemyHealth enemyHealth))
        {
          enemyHealth.TakeDamage();
        }
      }
    }

    private void OnDrawGizmos()
    {
      if (_drawGizmos == false)
      {
        return;
      }
      Gizmos.color = _color;
      Gizmos.DrawSphere(HitPosition, _radius);
    }
  }
}