﻿using UnityEngine;

namespace Code.Runtime.Logic.Player
{
  public interface IPlayerHealth
  {
    void TakeDamage(GameObject damageCauser, int decreaseValue = 1);
  }
}