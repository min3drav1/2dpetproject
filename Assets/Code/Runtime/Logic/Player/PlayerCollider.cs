﻿using UnityEngine;

namespace Code.Runtime.Logic.Player
{
  public class PlayerCollider : MonoBehaviour
  {
    [SerializeField] private CapsuleCollider2D _collider;
    [SerializeField] private LayerMask _layerMask;
    [field: SerializeField] public Vector2 CrouchSize { get; private set; }
    [field: SerializeField] public Vector2 CrouchOffset { get; private set; }
    [field: SerializeField] public CapsuleDirection2D CrouchDirection { get; private set; }

    private Vector2 _standSize;
    private Vector2 _standOffset;
    private CapsuleDirection2D _standDirection;

    private readonly Collider2D[] _result = new Collider2D[1];
    private readonly Vector2 _sizeOffset = new(0f, 0.1f);


    private void Awake()
    {
      _standSize = _collider.size;
      _standOffset = _collider.offset;
      _standDirection = _collider.direction;
    }

    public void Stand()
    {
      _collider.size = _standSize;
      _collider.offset = _standOffset;
      _collider.direction = _standDirection;
    }

    public void Crouch()
    {
      _collider.size = CrouchSize;
      _collider.offset = CrouchOffset;
      _collider.direction = CrouchDirection;
    }

    public bool CanGetUp()
    {
      Vector2 origin = (Vector2)transform.position + _standOffset + _sizeOffset;
      int count = Physics2D.OverlapCapsuleNonAlloc(origin, _standSize, _standDirection, 0f, _result, _layerMask);

      return count == 0;
    }
  }
}