﻿using System.Collections;
using UnityEngine;

namespace Code.Runtime.Logic.Player
{
  public class HurtBlink : MonoBehaviour
  {
    [SerializeField] private PlayerHealth _health;
    [SerializeField] private SpriteRenderer _sprite;
    [SerializeField] private float _blinkTime;
    [SerializeField] private Color _hurtColor;

    private Coroutine _coroutine;
    private Color _originColor;

    private void Awake()
    {
      _originColor = _sprite.color;
      _health.HpChanged += StartBlink;
    }

    private void OnDestroy()
    {
      _health.HpChanged -= StartBlink;
    }

    private void StartBlink()
    {
      if (_health.IsDeath() || _health.DamageCauser == null)
        return;

      if (_coroutine != null)
      {
        _sprite.color = _originColor;
        StopCoroutine(_coroutine);
      }

      _coroutine = StartCoroutine(Blinking());
    }

    private IEnumerator Blinking()
    {
      float timer = _blinkTime;
      float seconds = 0.1f;
      WaitForSeconds waitTime = new WaitForSeconds(seconds);
      while (timer >= 0f)
      {
        _sprite.color = _hurtColor;
        timer -= seconds;
        yield return waitTime;

        _sprite.color = _originColor;
        timer -= seconds;
        yield return waitTime;
      }

      _sprite.color = _originColor;
    }
  }
}