﻿using System;
using System.Collections;
using UnityEngine;

namespace Code.Runtime.Logic.Player
{
  public class PlayerHealth : MonoBehaviour, IPlayerHealth
  {
    [SerializeField] private float _cooldownTime = 1f;
    [SerializeField] private int _maxValue;
    [SerializeField] private int _current;
    [field: SerializeField] public bool IsCooldown { get; private set; }

    public int Current
    {
      get => _current;
      set
      {
        _current = Mathf.Clamp(value, 0, _maxValue);
        HpChanged?.Invoke();
      }
    }

    public int MaxValue
    {
      get => _maxValue;
      set
      {
        int delta = Mathf.Clamp(value - _maxValue, 0, value);
        _maxValue = value;
        _current = Mathf.Clamp(_current + delta, 0, _maxValue);

        MaxHpChanged?.Invoke();
        HpChanged?.Invoke();
      }
    }

    public GameObject DamageCauser { get; private set; }

    public event Action HpChanged;
    public event Action MaxHpChanged;
    public event Action Death;


    public void TakeDamage(GameObject damageCauser, int decreaseValue = 1)
    {
      if (Current == 0 || IsCooldown)
        return;

      IsCooldown = true;
      DamageCauser = damageCauser;

      Current = Mathf.Clamp(Current - decreaseValue, 0, MaxValue);

      if (IsDeath())
      {
        Death?.Invoke();
        Current = 0;
      }
      else
      {
        StartCoroutine(DamageCooldown());
      }

      HpChanged?.Invoke();
    }

    public bool IsDeath()
    {
      return Current <= 0;
    }

    public void AddHeart(int addValue = 1)
    {
      Current += addValue;
    }

    public bool CanAddHeart(int addValue = 1)
    {
      return Current + addValue <= MaxValue && Current != 0;
    }

    private IEnumerator DamageCooldown()
    {
      yield return new WaitForSeconds(_cooldownTime);
      IsCooldown = false;
      DamageCauser = null;
    }
  }
}