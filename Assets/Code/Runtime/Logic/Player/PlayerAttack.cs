﻿using UnityEngine;

namespace Code.Runtime.Logic.Player
{
  public class PlayerAttack : MonoBehaviour
  {
    [SerializeField] private PlayerAnimator _animator;
    [SerializeField] private float _cooldownTime = 5f;
    
    private float _lastTimeAttack;

    private void Awake()
    {
      _lastTimeAttack = -_cooldownTime;
    }

    public void Attack()
    {
      if (Time.time - _lastTimeAttack < _cooldownTime)
      {
        return;
      }

      _lastTimeAttack = Time.time;

      _animator.Attack();
    }
  }
}