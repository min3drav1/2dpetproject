﻿using System.Collections;
using UnityEngine;

namespace Code.Runtime.Logic.Player
{
  public class HurtReaction : MonoBehaviour
  {
    [SerializeField] private PlayerHealth _health;
    [SerializeField] private Rigidbody2D _rigidbody;
    [SerializeField] private PlayerAnimator _animator;

    [SerializeField] private float _reactForce;

    private void Awake()
    {
      _health.HpChanged += React;
    }

    private void OnDestroy()
    {
      _health.HpChanged -= React;
    }

    private void React()
    {
      if (_health.IsDeath())
        return;

      JumpFromEnemy(_health.DamageCauser);
      StartCoroutine(DamageCooldown());
    }

    private void JumpFromEnemy(GameObject damageCauser)
    {
      if (damageCauser == null)
        return;

      Vector2 reactDirection = _rigidbody.transform.position - damageCauser.transform.position;

      if (reactDirection.x > 0)
        reactDirection = new Vector2(0.5f, 0.86f);
      else
        reactDirection = new Vector2(-0.5f, 0.86f);

      _rigidbody.velocity = Vector2.zero;
      _rigidbody.AddForce(reactDirection * _reactForce, ForceMode2D.Impulse);
    }


    private IEnumerator DamageCooldown()
    {
      _animator.SetDefault();
      DisableMoveAndAnimator();
      yield return new WaitForSeconds(0.5f);
      ActivateMoveAndAnimator();
    }

    private void DisableMoveAndAnimator()
    {
      _animator.enabled = false;
    }

    private void ActivateMoveAndAnimator()
    {
      _animator.enabled = true;
    }
  }
}