﻿using UnityEngine;

namespace Code.Runtime.Logic.Player
{
  [RequireComponent(typeof(Animator))]
  public class PlayerAnimator : MonoBehaviour
  {
    [SerializeField] private Animator _animator;
    [SerializeField] private Rigidbody2D _rigidbody;
    [SerializeField] private SpriteRenderer _player;

    private readonly int _crouch = Animator.StringToHash("Crouch");
    private readonly int _death = Animator.StringToHash("Death");

    private readonly int _horizontalSpeed = Animator.StringToHash("HorizontalSpeed");
    private readonly int _verticalSpeed = Animator.StringToHash("VerticalSpeed");
    private readonly int _attackHash = Animator.StringToHash("Attack");

    private bool _lookRight = true;

    private void Update()
    {
      UpdateFlipSprite();
      UpdateHorizontal();
      UpdateVertical();
    }

    public void SetCrouch(bool active)
    {
      _animator.SetBool(_crouch, active);
    }

    public void Die()
    {
      _animator.SetTrigger(_death);
    }

    public void Attack()
    {
      _animator.SetTrigger(_attackHash);
    }

    public void SetDefault()
    {
      _animator.SetFloat(_horizontalSpeed, 0f);
      _animator.SetFloat(_verticalSpeed, 0f);
      SetCrouch(false);
    }

    private void UpdateFlipSprite()
    {
      if (Mathf.Abs(_rigidbody.velocity.x) < 0.1f)
        return;

      if (_rigidbody.velocity.x < 0 && _lookRight)
      {
        _lookRight = false;
        transform.localScale = new Vector3(-1, 1, 1);
      }

      if (_rigidbody.velocity.x > 0 && _lookRight == false)
      {
        _lookRight = true;
        transform.localScale = new Vector3(1, 1, 1);
      }
    }

    private void UpdateHorizontal()
    {
      _animator.SetFloat(_horizontalSpeed, Mathf.Abs(_rigidbody.velocity.x));
    }

    private void UpdateVertical()
    {
      _animator.SetFloat(_verticalSpeed, _rigidbody.velocity.y);
    }
  }
}