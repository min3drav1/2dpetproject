﻿using Code.Runtime.Infrastructure.Services.Input;
using Code.Runtime.Logic.PlayerStateMachine;
using UnityEngine;

namespace Code.Runtime.Logic.Player
{
  public class PlayerCharacter : MonoBehaviour
  {
    [field: SerializeField] public Rigidbody2D Rigidbody { get; private set; }
    [field: SerializeField] public PlayerHealth Health { get; private set; }
    [field: SerializeField] public GroundChecker GroundChecker { get; private set; }
    [field: SerializeField] public PlayerAnimator Animator { get; private set; }
    [field: SerializeField] public PlayerCollider PlayerCollider { get; private set; }
    [field: SerializeField] public PlayerDeath Death { get; private set; }
    [field: SerializeField] public PlayerFsm StateMachine { get; private set; }
    [field: SerializeField] public PlayerProgressSaveLoad ProgressSaveLoad { get; private set; }
    [field: SerializeField] public PlayerAttack PlayerAttack { get; private set; }

    public IInputSystem Input { get; private set; }
    public ActCounter AirJumpCounter { get; private set; } = new();
    public ActCounter DashCounter { get; private set; } = new();

    public void SetInput(IInputSystem input)
    {
      Input = input;
    }
  }
}