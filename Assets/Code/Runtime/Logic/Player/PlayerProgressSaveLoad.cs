﻿using Code.Runtime.Infrastructure.Services.PersistantProgress.Data;
using Code.Runtime.Infrastructure.Services.SaveLoad;
using Code.Runtime.Infrastructure.Services.SceneGraph;
using Code.Runtime.Utility;
using UnityEngine;

namespace Code.Runtime.Logic.Player
{
  public class PlayerProgressSaveLoad : MonoBehaviour, ISave, ILoad
  {
    [SerializeField] private PlayerCharacter playerCharacter;
    private ISceneGraphService _sceneGraphService;

    public void Initialize(ISceneGraphService sceneGraphService)
    {
      _sceneGraphService = sceneGraphService;
    }

    public void UpdateProgress(PlayerProgress progress)
    {
      progress.Stats.CurrentHp = playerCharacter.Health.Current;
      progress.Stats.MaxHp = playerCharacter.Health.MaxValue;

      progress.Stats.ActiveStates = playerCharacter.StateMachine.ActiveStates;
      progress.Stats.MaxJumpInAir = playerCharacter.AirJumpCounter.MaxCount;
      progress.Stats.MaxDashCount = playerCharacter.DashCounter.MaxCount;

      progress.PositionOnLevel.Position = playerCharacter.Rigidbody.position.AsVector2Data();
      progress.PositionOnLevel.LevelName = _sceneGraphService.CurrentLevel.SceneName;
    }

    public void LoadProgress(PlayerProgress progress)
    {
      playerCharacter.Health.MaxValue = progress.Stats.MaxHp;
      playerCharacter.Health.Current = progress.Stats.CurrentHp;

      playerCharacter.StateMachine.ActiveStates = progress.Stats.ActiveStates;
      playerCharacter.AirJumpCounter.MaxCount = progress.Stats.MaxJumpInAir;
      playerCharacter.DashCounter.MaxCount = progress.Stats.MaxDashCount;
    }
  }
}