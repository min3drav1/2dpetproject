﻿using System.Collections;
using Code.Runtime.Infrastructure.StateMachine;
using Code.Runtime.Infrastructure.StateMachine.State;
using UnityEngine;

namespace Code.Runtime.Logic.Player
{
  public class PlayerDeath : MonoBehaviour
  {
    [SerializeField] private PlayerHealth _playerHealth;
    [SerializeField] private PlayerAnimator _animator;
    [SerializeField] private Rigidbody2D _rigidbody;
    [SerializeField] private GameObject _fxInstance;

    private IGameStateMachine _stateMachine;

    public void Initialize(IGameStateMachine stateMachine)
    {
      _stateMachine = stateMachine;
    }

    public void Awake()
    {
      _playerHealth.Death += Die;
    }

    private void OnDestroy()
    {
      _playerHealth.Death -= Die;
    }

    public void Reset()
    {
      SetComponentsActive(true);
    }

    private void Die()
    {
      SetComponentsActive(false);
      _rigidbody.velocity = Vector2.zero;
      _animator.Die();

      StartCoroutine(ShowFXAndDestroy());
    }

    private void SetComponentsActive(bool enable)
    {
      _playerHealth.enabled = enable;
      _rigidbody.isKinematic = !enable;
    }

    private IEnumerator ShowFXAndDestroy()
    {
      yield return new WaitForSeconds(0.5f);
      GameObject fx = Instantiate(_fxInstance, transform.position, Quaternion.identity);
      Destroy(fx, 3f);
      _stateMachine.Enter<GameOverState>();
    }
  }
}