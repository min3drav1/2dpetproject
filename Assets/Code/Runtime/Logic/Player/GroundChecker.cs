﻿using Code.EditorHelper.Attribute;
using UnityEngine;

namespace Code.Runtime.Logic.Player
{
  public class GroundChecker : MonoBehaviour
  {
    [SerializeField, ReadOnly] private bool _isGrounded;
    [SerializeField] private LayerMask _groundLayer;
    [SerializeField] private float _distance;
    [SerializeField] private Vector2 _boxSize;

    private readonly Vector2 _direction = Vector2.down;
    private readonly RaycastHit2D[] _result = new RaycastHit2D[1];

    private Vector2 _collisionPoint;
    private Vector2 _groundNormal;

    public bool IsGrounded => _isGrounded;

    public void FixedUpdate()
    {
      _isGrounded = CheckGrounded();
    }

    private bool CheckGrounded()
    {
      int count = Physics2D.BoxCastNonAlloc(transform.position, _boxSize, 0f, _direction, _result, _distance,
        _groundLayer);

      if (count == 0)
        return false;

      _groundNormal = _result[0].normal;
      _collisionPoint = _result[0].point;

      return true;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
      if (_isGrounded)
        Gizmos.color = Color.blue;
      else
        Gizmos.color = Color.red;

      Gizmos.DrawLine(transform.position, transform.position + (Vector3)_direction * _distance);
      Gizmos.DrawWireCube(transform.position, _boxSize);

      if (_isGrounded)
      {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(_collisionPoint, _collisionPoint + _groundNormal);
      }
    }
#endif
  }
}