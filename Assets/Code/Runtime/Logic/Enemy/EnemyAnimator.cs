﻿using System;
using UnityEngine;

namespace Code.Runtime.Logic.Enemy
{
  public class EnemyAnimator : MonoBehaviour
  {
    [SerializeField] private Animator _animator;
    [SerializeField] private Rigidbody2D _rigidbody;
    
    private readonly int _horizontalSpeed = Animator.StringToHash("HorizontalSpeed");
    private readonly int _verticalSpeed = Animator.StringToHash("VerticalSpeed");

    private void Update()
    {
      _animator.SetFloat(_horizontalSpeed, Mathf.Abs(_rigidbody.velocity.x));
      _animator.SetFloat(_verticalSpeed, _rigidbody.velocity.y);
    }
  }
}