﻿using System.Collections.Generic;
using UnityEngine;
using Color = UnityEngine.Color;

namespace Code.Runtime.Logic.Enemy
{
  public class PatrolPoints : MonoBehaviour
  {
    [field:SerializeField] public List<Vector2> Points { get; private set; }
    
    private void OnDrawGizmos()
    {
      if (Points == null || Points.Count == 0)
      {
        return;
      }
      
      Gizmos.color = Color.green;
      
      foreach (Vector2 point in Points)
      {
        Gizmos.DrawSphere(point, 0.25f);
      }
    }
  }
}