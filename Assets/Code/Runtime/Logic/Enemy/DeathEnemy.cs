﻿using System;
using Code.Runtime.Infrastructure.Services.AssetManager;
using UnityEngine;

namespace Code.Runtime.Logic.Enemy
{
  public class DeathEnemy : MonoBehaviour, IEnemyHealth
  {
    private IReleaseAsset _releaseAsset;

    public void Initialize(IReleaseAsset releaseAsset)
    {
      _releaseAsset = releaseAsset;
    }
    
    public void TakeDamage()
    {
      Death();
    }

    public event Action ActionDead;

    private void Death()
    {
      ActionDead?.Invoke();
      _releaseAsset.Release(gameObject);
    }
  }
}