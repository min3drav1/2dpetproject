﻿namespace Code.Runtime.Logic.Enemy
{
  public interface IEnemyHealth
  {
    void TakeDamage();
  }
}