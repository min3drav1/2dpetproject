﻿using Code.Runtime.Logic.Player;
using UnityEngine;

namespace Code.Runtime.Logic.Enemy
{
  public class EnemyAttack : MonoBehaviour
  {
    [SerializeField] private HurtBox _hurtBox;

    private void Awake()
    {
      _hurtBox.OnTrigger += OnTrigger;
    }

    private void OnTrigger(Collider2D obj)
    {
      GameObject fox = obj.attachedRigidbody.gameObject;
      if (fox.TryGetComponent(out IPlayerHealth foxHealth))
        foxHealth.TakeDamage(gameObject);
    }
  }
}