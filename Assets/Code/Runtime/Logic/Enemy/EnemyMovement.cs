﻿using Code.Runtime.Logic.Player;
using Code.Runtime.Utility;
using UnityEngine;

namespace Code.Runtime.Logic.Enemy
{
  public class EnemyMovement : MonoBehaviour
  {
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private Rigidbody2D _rigidbody;
    [SerializeField] private GroundChecker _groundChecker;
    
    [Header("Horizontal movement")]
    [SerializeField] private float _speed;
    
    [Header("Jump")]
    [SerializeField] private float _jumpMaxHeight = 1;
    

    public void Move(float horizontalDirection)
    {
      UpdateForward(horizontalDirection);
      _rigidbody.SetHorizontalVelocity(horizontalDirection * _speed);
    }

    public void Jump()
    {
      if (_groundChecker.IsGrounded)
      {
        _rigidbody.SetJumpVelocityByHeight(_jumpMaxHeight);
      }
    }

    public void Stop()
    {
      _rigidbody.SetHorizontalVelocity(0f);
    }

    private void UpdateForward(float sign)
    {
      if (sign == -1 && _spriteRenderer.flipX)
      {
        _spriteRenderer.flipX = false;
      }

      if (sign == 1 && _spriteRenderer.flipX == false)
      {
        _spriteRenderer.flipX = true;
      }
    }
  }
}