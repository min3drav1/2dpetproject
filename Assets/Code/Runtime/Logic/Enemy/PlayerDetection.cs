﻿using Code.EditorHelper.Attribute;
using UnityEngine;

namespace Code.Runtime.Logic.Enemy
{
  public class PlayerDetection : MonoBehaviour
  {
    [SerializeField] private Vector2 _size;
    [SerializeField] private LayerMask _layerMask;
    [field:SerializeField, ReadOnly] public bool IsDetected { get; private set; }
    public Transform Target { get; private set; }

    private readonly Collider2D[] _result = new Collider2D[1];
    
    private void FixedUpdate()
    {
      if (Time.frameCount % 3 != 0)
      {
        return;
      }
      
      int count = Physics2D.OverlapBoxNonAlloc(transform.position, _size, 0, _result, _layerMask);
      IsDetected = count == 1;
      Target = IsDetected ? _result[0].attachedRigidbody.transform : null;
    }

    private void OnDrawGizmos()
    {
      Gizmos.color = Color.yellow;
      Gizmos.DrawWireCube(transform.position, _size);
    }
  }
}