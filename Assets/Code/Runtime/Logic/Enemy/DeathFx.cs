﻿using UnityEngine;

namespace Code.Runtime.Logic.Enemy
{
  public class DeathFx : MonoBehaviour
  {
    [SerializeField] private DeathEnemy _death;
    [SerializeField] private GameObject _fx;
    [SerializeField] private float _fxLifetime;

    private void Awake()
    {
      _death.ActionDead += SpawnFx;
    }

    private void OnDestroy()
    {
      _death.ActionDead -= SpawnFx;
    }

    private void SpawnFx()
    {
      GameObject fx = Instantiate(_fx, _death.transform.position, Quaternion.identity);
      Destroy(fx, _fxLifetime);
    }
  }
}