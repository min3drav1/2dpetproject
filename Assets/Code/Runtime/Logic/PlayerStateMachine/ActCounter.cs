﻿using System;
using UnityEngine;

namespace Code.Runtime.Logic.PlayerStateMachine
{
  [Serializable]
  public class ActCounter
  {
    [field: SerializeField] public int Count { get; private set; } = 0;
    [field: SerializeField] public int MaxCount { get; set; }
    
    public void Reset()
    {
      Count = 0;
    }

    public void Add()
    {
      ++Count;
    }

    public bool CanAct()
    {
      return Count < MaxCount;
    }
  }
}