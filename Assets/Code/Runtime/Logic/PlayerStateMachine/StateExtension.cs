﻿using System;

namespace Code.Runtime.Logic.PlayerStateMachine
{
  public static class StateExtension
  {
    public static int StateIdToIndex(StateID stateID)
    {
      return stateID switch
      {
        StateID.Move => 0,
        StateID.Crouch => 1,
        StateID.Jump => 2,
        StateID.Fall => 3,
        StateID.AirJump => 4,
        StateID.Dash => 5,
        _ => throw new Exception($"Cant convert state id {stateID} to index"),
      };
    }
    
    public static int GetStateIdCount()
    {
      return Enum.GetNames(typeof(StateID)).Length - 1;
    }
  }
}