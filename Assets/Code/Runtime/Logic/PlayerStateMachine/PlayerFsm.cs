﻿using Code.EditorHelper.Attribute;
using Code.Runtime.Logic.PlayerStateMachine.States;
using Code.Runtime.Logic.PlayerStateMachine.Transitions;
using UnityEngine;

namespace Code.Runtime.Logic.PlayerStateMachine
{
  public class PlayerFsm : MonoBehaviour
  {
#if UNITY_EDITOR
    [SerializeField, ReadOnly] private string _currentStateName = "";
#endif
    [field: SerializeField] public StateID ActiveStates { get; set; }
    [SerializeReference] private State[] _states;
    private State _currentState = null;

    private void Awake()
    {
      enabled = false;
    }

    public void Initialize(State[] states)
    {
      _states = states;
      enabled = true;
    }

    public void Launch(StateID stateID)
    {
      SetCurrentState(stateID);
    }

    private void Update()
    {
      StateID nextState = CheckNextState(_currentState.Transitions);
      if (nextState != StateID.None && (nextState & ActiveStates) != 0)
      {
        SetCurrentState(nextState);
      }

      _currentState.Update(Time.deltaTime);
    }

    private void FixedUpdate()
    {
      _currentState.FixedUpdate(Time.fixedDeltaTime);
    }

    private void SetCurrentState(StateID stateID)
    {
      _currentState?.Exit();
      _currentState = GetStateById(stateID);
      _currentState.Entry();

#if UNITY_EDITOR
      _currentStateName = _currentState.StateID.ToString();
#endif
    }

    private State GetStateById(StateID stateID)
    {
      return _states[StateExtension.StateIdToIndex(stateID)];
    }

    private StateID CheckNextState(Transition[] transitions)
    {
      foreach (Transition transition in transitions)
      {
        if ((transition.NextState & ActiveStates) != 0 &&
            transition.Check())
        {
          return transition.NextState;
        }
      }

      return StateID.None;
    }
  }
}