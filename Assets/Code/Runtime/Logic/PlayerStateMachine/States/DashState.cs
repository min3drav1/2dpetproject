﻿using System;
using Code.Runtime.Infrastructure.Services.Input;
using UnityEngine;

namespace Code.Runtime.Logic.PlayerStateMachine.States
{
  [Serializable]
  public class DashState : State
  {
    private readonly Rigidbody2D _rigidbody2D;
    private readonly IInputSystem _inputSystem;
    
    [SerializeField] private float _dashSpeed;
    [SerializeField] private float _dashTime;
    [SerializeField] private ActCounter _dashCounter;

    private float _timer = 0;
    private Vector2 _moveVelocity;

    public sealed override StateID StateID => StateID.Dash;

    public DashState(Rigidbody2D rigidbody2D, IInputSystem inputSystem, float dashSpeed, float dashTime, ActCounter dashCounter)
    {
      _rigidbody2D = rigidbody2D;
      _inputSystem = inputSystem;
      _dashSpeed = dashSpeed;
      _dashTime = dashTime;
      _dashCounter = dashCounter;
    }

    public override void Entry()
    {
      _dashCounter.Add();
      IsLockTransitions = true;
      _timer = _dashTime;
      _moveVelocity = new Vector2(_inputSystem.HorizontalDirection * _dashSpeed, 0f);
    }

    public override void Update(float deltaTime)
    {
      _rigidbody2D.velocity = _moveVelocity;
      _timer -= deltaTime;
      if (_timer <= 0)
      {
        IsLockTransitions = false;
      }
    }
  }
}