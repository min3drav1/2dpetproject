﻿using System;
using Code.Runtime.Infrastructure.Services.Input;
using Code.Runtime.Logic.Player;
using Code.Runtime.Utility;
using UnityEngine;

namespace Code.Runtime.Logic.PlayerStateMachine.States
{
  [Serializable]
  public class CrouchState : State
  {
    private readonly PlayerAnimator _animator;
    private readonly Rigidbody2D _rigidbody;
    private readonly IInputSystem _input;
    private readonly PlayerCollider _playerCollider;
    [SerializeField] private float _speed;
    
    private float _moveDirection;

    public override StateID StateID => StateID.Crouch;

    public CrouchState(PlayerAnimator animator, Rigidbody2D rigidbody, IInputSystem input, PlayerCollider playerCollider, float speed)
    {
      _animator = animator;
      _rigidbody = rigidbody;
      _input = input;
      _playerCollider = playerCollider;
      _speed = speed;
    }

    public override void Entry()
    {
      _animator.SetCrouch(true);
      _playerCollider.Crouch();
    }

    public override void Update(float deltaTime)
    {
      _moveDirection = _input.HorizontalDirection;
    }

    public override void FixedUpdate(float fixedDeltaTime)
    {
      _rigidbody.SetHorizontalVelocity(_moveDirection * _speed);
    }

    public override void Exit()
    {
      _animator.SetCrouch(false);
      _playerCollider.Stand();
    }
  }
}