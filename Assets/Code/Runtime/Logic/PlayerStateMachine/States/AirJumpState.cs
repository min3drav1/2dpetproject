﻿using System;
using Code.Runtime.Infrastructure.Services.Input;
using Code.Runtime.Utility;
using UnityEngine;

namespace Code.Runtime.Logic.PlayerStateMachine.States
{
  [Serializable]
  public class AirJumpState : State
  {
    public override StateID StateID => StateID.AirJump;

    [SerializeField] private float _height;
    [SerializeField] private float _velocity;
    [SerializeField] private ActCounter _airJumpCounter;
    [SerializeField] private float _jumpGravity;
    [SerializeField] private float _fallGravity;

    private readonly Rigidbody2D _rigidbody;
    private readonly IInputSystem _inputSystem;
    private float _direction;
    private bool _isPressedJump;

    public AirJumpState(Rigidbody2D rigidbody, IInputSystem inputSystem, float height, float velocity,
      float jumpGravity, float fallGravity, ActCounter airJumpCounter)
    {
      _rigidbody = rigidbody;
      _inputSystem = inputSystem;
      _height = height;
      _velocity = velocity;
      _airJumpCounter = airJumpCounter;
      _jumpGravity = jumpGravity;
      _fallGravity = fallGravity;
    }

    public override void Entry()
    {
      _rigidbody.gravityScale = _jumpGravity;
      _isPressedJump = true;
      _airJumpCounter.Add();
      Jump();
    }

    public override void Update(float deltaTime)
    {
      _direction = _inputSystem.HorizontalDirection;

      if (_isPressedJump && _inputSystem.IsJumpUp)
      {
        _rigidbody.gravityScale = _fallGravity;
        _isPressedJump = false;
      }
    }

    public override void FixedUpdate(float fixedDeltaTime)
    {
      _rigidbody.SetHorizontalVelocity(_direction * _velocity);
    }

    private void Jump()
    {
      _rigidbody.SetJumpVelocityByHeight(_height);
    }
  }
}