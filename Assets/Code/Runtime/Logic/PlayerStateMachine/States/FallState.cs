﻿using System;
using Code.Runtime.Infrastructure.Services.Input;
using Code.Runtime.Logic.Player;
using Code.Runtime.Utility;
using UnityEngine;

namespace Code.Runtime.Logic.PlayerStateMachine.States
{
  [Serializable]
  public class FallState : State
  {
    public override StateID StateID => StateID.Fall;
    
    [SerializeField] private float _velocity;
    [SerializeField] private float _fallGravity;
    
    private readonly Rigidbody2D _rigidbody;
    private readonly IInputSystem _inputSystem;
    private float _direction;
    private PlayerAttack _playerAttack;

    public FallState(Rigidbody2D rigidbody, PlayerAttack playerAttack, IInputSystem inputSystem,
      float velocity, float fallGravity)
    {
      _rigidbody = rigidbody;
      _playerAttack = playerAttack;
      _inputSystem = inputSystem;
      _velocity = velocity;
      _fallGravity = fallGravity;
    }

    public override void Entry()
    {
      _rigidbody.gravityScale = _fallGravity;
    }

    public override void Update(float deltaTime)
    {
      _direction = _inputSystem.HorizontalDirection;

      if (_inputSystem.IsAttack)
      {
        Debug.Log("attack");
        _playerAttack.Attack();
      }

    }

    public override void FixedUpdate(float fixedDeltaTime)
    {
      _rigidbody.SetHorizontalVelocity(_direction * _velocity);
    }
  }
}