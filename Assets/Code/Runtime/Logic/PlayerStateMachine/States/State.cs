﻿using System;
using Code.EditorHelper.Attribute;
using Code.Runtime.Logic.PlayerStateMachine.Transitions;
using UnityEngine;

namespace Code.Runtime.Logic.PlayerStateMachine.States
{
  [Serializable]
  public class State
  {
    [field:SerializeField, ReadOnly] public string Name { get; protected set; }
    public Transition[] Transitions { get; private set; }
    public virtual StateID StateID { get; }
    public bool IsLockTransitions { get; protected set; } = false;

    protected State()
    {
      Name = StateID.ToString();
    }

    public void SetTransitions(Transition[] transitions)
    {
      Transitions = transitions;
    }

    public virtual void Entry()
    {
    }

    public virtual void Exit()
    {
    }

    public virtual void Update(float deltaTime)
    {
    }

    public virtual void FixedUpdate(float fixedDeltaTime)
    {
    }
  }
}