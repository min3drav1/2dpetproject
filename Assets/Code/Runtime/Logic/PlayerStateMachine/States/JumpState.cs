﻿using System;
using Code.Runtime.Infrastructure.Services.Input;
using Code.Runtime.Logic.Player;
using Code.Runtime.Utility;
using UnityEngine;

namespace Code.Runtime.Logic.PlayerStateMachine.States
{
  [Serializable]
  public class JumpState : State
  {
    [SerializeField] private float _height;
    [SerializeField] private float _velocity;
    [SerializeField] private float _jumpGravity;
    [SerializeField] private float _fallGravity;
    
    private readonly Rigidbody2D _rigidbody;
    private readonly IInputSystem _inputSystem;
    private readonly PlayerAttack _playerAttack;
    
    private float _direction;
    private bool _isPressedJump;
    

    public override StateID StateID => StateID.Jump;

    public JumpState(Rigidbody2D rigidbody, PlayerAttack playerAttack, IInputSystem inputSystem,
      float height, float velocity, float jumpGravity, float fallGravity)
    {
      _rigidbody = rigidbody;
      _playerAttack = playerAttack;
      _inputSystem = inputSystem;
      _height = height;
      _velocity = velocity;
      _jumpGravity = jumpGravity;
      _fallGravity = fallGravity;
    }

    public override void Entry()
    {
      _rigidbody.gravityScale = _jumpGravity;
      _isPressedJump = true;
      Jump();
    }

    public override void Update(float deltaTime)
    {
      _direction = _inputSystem.HorizontalDirection;
      
      if (_inputSystem.IsAttack)
      {
        _playerAttack.Attack();
      }

      if (_isPressedJump && _inputSystem.IsJumpUp)
      {
        _rigidbody.gravityScale = _fallGravity;
        _isPressedJump = false;
      }
    }

    public override void FixedUpdate(float fixedDeltaTime)
    {
      _rigidbody.SetHorizontalVelocity(_direction * _velocity);
    }

    private void Jump()
    {
      _rigidbody.SetJumpVelocityByHeight(_height);
    }
  }
}