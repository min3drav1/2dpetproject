﻿using System;
using Code.Runtime.Infrastructure.Services.Input;
using Code.Runtime.Logic.Player;
using Code.Runtime.Utility;
using UnityEngine;

namespace Code.Runtime.Logic.PlayerStateMachine.States
{
  [Serializable]
  public class MoveState : State
  {
    [SerializeField] private float _speed;

    private readonly Rigidbody2D _rigidbody;
    private readonly IInputSystem _input;
    private readonly ActCounter _airJumpCounter;
    private readonly ActCounter _dashCounter;
    private readonly PlayerAttack _playerAttack;

    private float _moveDirection;

    public override StateID StateID => StateID.Move;

    public MoveState(Rigidbody2D rigidbody,
      IInputSystem input,
      ActCounter airJumpCounter,
      ActCounter dashCounter,
      PlayerAttack playerAttack,
      float speed)
    {
      _dashCounter = dashCounter;
      _rigidbody = rigidbody;
      _airJumpCounter = airJumpCounter;
      _input = input;
      _speed = speed;
      _playerAttack = playerAttack;
    }

    public override void Entry()
    {
      _airJumpCounter?.Reset();
      _dashCounter?.Reset();
    }

    public override void Update(float deltaTime)
    {
      _moveDirection = _input.HorizontalDirection;
      if (_input.IsAttack)
      {
        _playerAttack.Attack();
      }
    }

    public override void FixedUpdate(float fixedDeltaTime)
    {
      _rigidbody.SetHorizontalVelocity(_moveDirection * _speed);
    }
  }
}