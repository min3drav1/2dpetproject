﻿using System;

namespace Code.Runtime.Logic.PlayerStateMachine
{
  [Flags]
  public enum StateID
  {
    None = 0,
    Move = 1,
    Crouch = 2,
    Jump = 4,
    Fall = 8,
    AirJump = 16,
    Dash = 32,
  }
}