﻿using UnityEngine;

namespace Code.Runtime.Logic.PlayerStateMachine
{
  [CreateAssetMenu(fileName = "FSMData")]
  public class PlayerFsmData : ScriptableObject
  {
    [field:Header("Move state")]
    [field: SerializeField] public float MoveSpeed { get; private set; } = 5f;

    [field:Header("Jump state")]
    [field: SerializeField] public float JumpHeight { get; private set; } = 3f;
    [field: SerializeField] public float JumpHorizontalVelocity { get; private set; } = 4f;
    [field: SerializeField] public float JumpGravity { get; private set; } = 1f;
    
    [field:Header("Crouch state")]
    [field: SerializeField] public float CrouchSpeed { get; private set; } = 3f;
    
    [field:Header("Fall state")]
    [field: SerializeField] public float FallHorizontalVelocity { get; private set; } = 4f;
    [field: SerializeField] public float FallGravity { get; private set; } = 3f;
    
    [field:Header("AirJump state")]
    [field: SerializeField] public int MaxAirJumpCount { get; private set; } = 2;
    [field: SerializeField] public float AirJumpHeight { get; private set; } = 3;
    [field: SerializeField] public float AirJumpHorizontalVelocity { get; private set; } = 4;

    [field:Header("Dash state")]
    [field: SerializeField] public int MaxDashCount { get; private set; } = 1;
    [field: SerializeField] public float DashSpeed { get; private set; } = 20;
    [field: SerializeField] public float DashTime { get; private set; } = 0.5f;
  }
}