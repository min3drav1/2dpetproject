﻿using Code.Runtime.Logic.Player;
using UnityEngine;

namespace Code.Runtime.Logic.PlayerStateMachine.Transitions
{
  public class GroundedTransition : Transition
  {
    private readonly Rigidbody2D _rigidbody;
    private readonly GroundChecker _groundChecker;
    public sealed override StateID NextState { get; protected set; }

    public GroundedTransition(GroundChecker groundChecker, StateID nextState)
    {
      _groundChecker = groundChecker;
      NextState = nextState;
    }
    
    public override bool Check()
    {
      return _groundChecker.IsGrounded;
    }
  }
}