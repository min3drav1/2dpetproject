﻿using Code.Runtime.Infrastructure.Services.Input;

namespace Code.Runtime.Logic.PlayerStateMachine.Transitions
{
  public class JumpInAirTransition : Transition
  {
    private readonly ActCounter _airJumpCounter;
    private readonly IInputSystem _inputSystem;
    public sealed override StateID NextState { get; protected set; }

    public JumpInAirTransition(ActCounter airJumpCounter, IInputSystem inputSystem, StateID nextState)
    {
      _airJumpCounter = airJumpCounter;
      _inputSystem = inputSystem;
      NextState = nextState;
    }

    public override bool Check()
    {
      return _inputSystem.IsJumpDown && _airJumpCounter.CanAct();
    }
  }
}