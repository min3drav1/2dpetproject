﻿using Code.Runtime.Infrastructure.Services.Input;

namespace Code.Runtime.Logic.PlayerStateMachine.Transitions
{
  public class CrouchPressedTransition : Transition
  {
    private readonly IInputSystem _inputSystem;
    public sealed override StateID NextState { get; protected set; }

    public CrouchPressedTransition(IInputSystem inputSystem, StateID nextState)
    {
      _inputSystem = inputSystem;
      NextState = nextState;
    }
    
    public override bool Check()
    {
      return _inputSystem.IsCrouch;
    }
  }
}