﻿using Code.Runtime.Infrastructure.Services.Input;
using Code.Runtime.Utility;

namespace Code.Runtime.Logic.PlayerStateMachine.Transitions
{
  public class DashTransition : Transition
  {
    private readonly ActCounter _dashCounter;
    private readonly IInputSystem _inputSystem;
    public sealed override StateID NextState { get; protected set; }

    public DashTransition(ActCounter dashCounter, IInputSystem inputSystem, StateID nextState)
    {
      _dashCounter = dashCounter;
      _inputSystem = inputSystem;
      NextState = nextState;
    }
    
    public override bool Check()
    {
      return _inputSystem.IsDash && _inputSystem.HorizontalDirection.IsNotNull() && _dashCounter.CanAct();
    }
  }
}