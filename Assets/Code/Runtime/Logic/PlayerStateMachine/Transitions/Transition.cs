﻿namespace Code.Runtime.Logic.PlayerStateMachine.Transitions
{
  public abstract class Transition
  {
    public abstract StateID NextState { get; protected set; }
    public abstract bool Check();
  }
}