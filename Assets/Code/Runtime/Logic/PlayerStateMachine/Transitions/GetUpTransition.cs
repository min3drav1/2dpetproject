﻿using Code.Runtime.Infrastructure.Services.Input;
using Code.Runtime.Logic.Player;

namespace Code.Runtime.Logic.PlayerStateMachine.Transitions
{
  public class GetUpTransition : Transition
  {
    private readonly IInputSystem _inputSystem;
    private readonly PlayerCollider _playerCollider;
    public sealed override StateID NextState { get; protected set; }

    public GetUpTransition(IInputSystem inputSystem, PlayerCollider playerCollider, StateID nextState)
    {
      _playerCollider = playerCollider;
      _inputSystem = inputSystem;
      NextState = nextState;
    }
    
    public override bool Check()
    {
      return _inputSystem.IsCrouch == false && _playerCollider.CanGetUp();
    }
  }
}