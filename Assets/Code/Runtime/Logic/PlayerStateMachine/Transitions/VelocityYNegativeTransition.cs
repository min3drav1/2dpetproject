﻿using Code.Runtime.Logic.Player;
using UnityEngine;

namespace Code.Runtime.Logic.PlayerStateMachine.Transitions
{
  public class VelocityYNegativeTransition : Transition
  {
    private readonly Rigidbody2D _rigidbody;
    private readonly GroundChecker _groundChecker;
    public sealed override StateID NextState { get; protected set; }

    public VelocityYNegativeTransition(Rigidbody2D rigidbody, GroundChecker groundChecker, StateID nextState)
    {
      _rigidbody = rigidbody;
      _groundChecker = groundChecker;
      NextState = nextState;
    }
    
    public override bool Check()
    {
      return _groundChecker.IsGrounded == false && _rigidbody.velocity.y < 0;
    }
  }
}