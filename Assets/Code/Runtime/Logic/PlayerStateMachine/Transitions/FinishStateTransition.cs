﻿using Code.Runtime.Logic.PlayerStateMachine.States;

namespace Code.Runtime.Logic.PlayerStateMachine.Transitions
{
  public class FinishStateTransition : Transition
  {
    public sealed override StateID NextState { get; protected set; }
    private readonly State _state;

    public FinishStateTransition(State state, StateID nextState)
    {
      _state = state;
      NextState = nextState;
    }

    public override bool Check()
    {
      return _state.IsLockTransitions == false;
    }
  }
}