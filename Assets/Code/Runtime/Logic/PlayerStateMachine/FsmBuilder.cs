﻿using Code.Runtime.Infrastructure.Services.Input;
using Code.Runtime.Logic.Player;
using Code.Runtime.Logic.PlayerStateMachine.States;
using Code.Runtime.Logic.PlayerStateMachine.Transitions;

namespace Code.Runtime.Logic.PlayerStateMachine
{
  public class FsmBuilder
  {
    private readonly State[] _states;
    private readonly PlayerCharacter _playerCharacter;
    private readonly IInputSystem _inputSystem;
    private readonly PlayerFsmData _data;
    private readonly ActCounter _airJumpCounter;
    private readonly ActCounter _dashCounter;


    public FsmBuilder(PlayerCharacter playerCharacter, IInputSystem inputSystem, PlayerFsmData data)
    {
      _data = data;
      _inputSystem = inputSystem;
      _playerCharacter = playerCharacter;
      _states = new State[StateExtension.GetStateIdCount()];

      _airJumpCounter = _playerCharacter.AirJumpCounter;
      _airJumpCounter.MaxCount = _data.MaxAirJumpCount;

      _dashCounter = playerCharacter.DashCounter;
      _dashCounter.MaxCount = _data.MaxDashCount;
    }

    public FsmBuilder MoveState()
    {
      State state = new MoveState(_playerCharacter.Rigidbody, _inputSystem, _airJumpCounter, _dashCounter,
        _playerCharacter.PlayerAttack, _data.MoveSpeed);
      state.SetTransitions(new Transition[]
      {
        new VelocityYNegativeTransition(_playerCharacter.Rigidbody, _playerCharacter.GroundChecker, StateID.Fall),
        new JumpPressedTransition(_inputSystem, StateID.Jump),
        new CrouchPressedTransition(_inputSystem, StateID.Crouch),
        new DashTransition(_dashCounter, _inputSystem, StateID.Dash),
      });
      SetState(state);
      return this;
    }

    public FsmBuilder FallState()
    {
      State state = new FallState(_playerCharacter.Rigidbody, _playerCharacter.PlayerAttack, _inputSystem,
        _data.FallHorizontalVelocity, _data.FallGravity);
      state.SetTransitions(new Transition[]
      {
        new GroundedTransition(_playerCharacter.GroundChecker, StateID.Move),
        new JumpInAirTransition(_airJumpCounter, _inputSystem, StateID.AirJump),
        new DashTransition(_dashCounter, _inputSystem, StateID.Dash)
      });
      SetState(state);
      return this;
    }

    public FsmBuilder JumpState()
    {
      State state = new JumpState(_playerCharacter.Rigidbody, _playerCharacter.PlayerAttack, _inputSystem,
        _data.JumpHeight, _data.JumpHorizontalVelocity, _data.JumpGravity, _data.FallGravity);
      state.SetTransitions(new Transition[]
      {
        new VelocityYNegativeTransition(_playerCharacter.Rigidbody, _playerCharacter.GroundChecker, StateID.Fall),
        new DashTransition(_dashCounter, _inputSystem, StateID.Dash),
        new JumpInAirTransition(_airJumpCounter, _inputSystem, StateID.AirJump),
      });
      SetState(state);
      return this;
    }

    public FsmBuilder AirJumpState()
    {
      State state = new AirJumpState(_playerCharacter.Rigidbody, _inputSystem, _data.AirJumpHeight,
        _data.AirJumpHorizontalVelocity, _data.JumpGravity, _data.FallGravity, _airJumpCounter);
      state.SetTransitions(new Transition[]
      {
        new VelocityYNegativeTransition(_playerCharacter.Rigidbody, _playerCharacter.GroundChecker, StateID.Fall),
      });
      SetState(state);
      return this;
    }

    public FsmBuilder CrouchState()
    {
      State state = new CrouchState(
        _playerCharacter.Animator,
        _playerCharacter.Rigidbody,
        _playerCharacter.Input,
        _playerCharacter.PlayerCollider,
        _data.CrouchSpeed);
      state.SetTransitions(new Transition[]
      {
        new VelocityYNegativeTransition(_playerCharacter.Rigidbody, _playerCharacter.GroundChecker, StateID.Fall),
        new GetUpTransition(_inputSystem, _playerCharacter.PlayerCollider, StateID.Move),
      });
      SetState(state);
      return this;
    }

    public FsmBuilder DashState()
    {
      State state = new DashState(_playerCharacter.Rigidbody, _inputSystem, _data.DashSpeed, _data.DashTime,
        _dashCounter);

      state.SetTransitions(new Transition[]
      {
        new FinishStateTransition(state, StateID.Fall),
      });
      SetState(state);
      return this;
    }

    public State[] Build()
    {
      return _states;
    }

    public static State[] DefaultStates(PlayerCharacter playerCharacter, IInputSystem inputSystem, PlayerFsmData data)
    {
      FsmBuilder builder = new FsmBuilder(playerCharacter, inputSystem, data);
      builder.MoveState();
      builder.FallState();
      builder.JumpState();
      builder.CrouchState();
      builder.AirJumpState();
      builder.DashState();

      return builder.Build();
    }

    private void SetState(State state)
    {
      _states[StateExtension.StateIdToIndex(state.StateID)] = state;
    }
  }
}