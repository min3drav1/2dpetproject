﻿using UnityEngine;

namespace Code.Runtime.Logic
{
  public class SpawnPoint : MonoBehaviour
  {
    public Vector3 Position => transform.position;
  }
}