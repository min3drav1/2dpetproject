﻿using Code.Runtime.Infrastructure.Services.SaveLoad;
using Code.Runtime.Utility;
using UnityEngine;
using Zenject;

namespace Code.Runtime.Logic
{
  [RequireComponent(typeof(Collider2D))]
  public class SaveTrigger : MonoBehaviour
  {
    [SerializeField] private LayerMask _layer;
    private ISaveLoadService _saveLoadService;

    [Inject]
    private void Construct(ISaveLoadService saveLoadService)
    {
      _saveLoadService = saveLoadService;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
      if (LayerUtility.LayerMashNotIncludeLayer(_layer, other.gameObject.layer))
      {
        return;
      }
      
      Debug.Log("SAVE PROGRESS");

      _saveLoadService.Save();
      gameObject.SetActive(false);
    }
  }
}