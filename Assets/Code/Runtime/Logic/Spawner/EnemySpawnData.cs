﻿using System;
using UnityEngine;

namespace Code.Runtime.Logic.Spawner
{
  [Serializable]
  public class EnemySpawnData
  {
    public EnemyId EnemyId;
    public Vector2[] PatrolPoints;
    public Vector2 Positions;

  }
}