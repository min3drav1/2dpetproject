﻿namespace Code.Runtime.Logic.Spawner
{
  public enum EnemyId
  {
    None = 0,
    Frog = 1,
    Opossum = 2,
  }
}