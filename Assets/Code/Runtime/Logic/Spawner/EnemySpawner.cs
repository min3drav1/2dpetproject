﻿using UnityEngine;

namespace Code.Runtime.Logic.Spawner
{
  public class EnemySpawner : MonoBehaviour
  {
    [field: SerializeField] public EnemyId EnemyId { get; private set; }
    [SerializeField] private Vector2[] _patrolPoints;

    public Vector2[] PatrolPoints
    {
      get
      {
        if (_patrolPoints == null || _patrolPoints.Length == 0)
        {
          return null;
        }
        Vector2[] points = new Vector2[_patrolPoints.Length];
        for (var i = 0; i < _patrolPoints.Length; i++)
        {
          points[i] = (Vector2)transform.position + _patrolPoints[i];
        }

        return points;
      }
    }

    private void OnDrawGizmos()
    {
      if (_patrolPoints == null || _patrolPoints.Length == 0)
      {
        return;
      }

      Gizmos.color = Color.green;

      foreach (Vector2 point in _patrolPoints)
      {
        Gizmos.DrawSphere(transform.position + (Vector3)point, 0.25f);
      }
    }
  }
}