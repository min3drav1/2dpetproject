﻿using System;
using UnityEngine;

namespace Code.Runtime.Logic
{
  public class HurtBox : MonoBehaviour
  {
    [SerializeField] private LayerMask _layerMask;
    [SerializeField] private Vector2 _boxSize;

    [Header("Debug")] [SerializeField] private Color _color;

    private readonly Collider2D[] _allocCollider2D = new Collider2D[1];

    private void Reset()
    {
      _layerMask = LayerMask.NameToLayer("HealBox");
    }

    private void FixedUpdate()
    {
      int count = Physics2D.OverlapBoxNonAlloc(transform.position, _boxSize, 0f, _allocCollider2D, _layerMask);
      if (count == 0)
        return;

      CallTriggerAction();
    }

    private void OnDrawGizmos()
    {
      Gizmos.color = _color;
      Gizmos.DrawCube(transform.position, _boxSize);
    }

    public event Action<Collider2D> OnTrigger;

    private void CallTriggerAction()
    {
      OnTrigger?.Invoke(_allocCollider2D[0]);
    }
  }
}