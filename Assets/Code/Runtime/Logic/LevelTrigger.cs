﻿using Code.Attribute;
using Code.Runtime.Infrastructure.StateMachine;
using Code.Runtime.Infrastructure.StateMachine.State;
using UnityEngine;
using Zenject;

namespace Code.Runtime.Logic
{
  [RequireComponent(typeof(Collider2D))]
  public class LevelTrigger : MonoBehaviour
  {
    [SerializeField, SceneName] private string _nextLevel;

    private IGameStateMachine _stateMachine;

    [Inject]
    private void Construct(IGameStateMachine stateMachine)
    {
      _stateMachine = stateMachine;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
      _stateMachine.Enter<UpdateLevelState, string>(_nextLevel);
    }
  }
}