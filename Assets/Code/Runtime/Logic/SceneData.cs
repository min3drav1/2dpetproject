﻿using UnityEngine;

namespace Code.Runtime.Logic
{
  public class SceneData : MonoBehaviour
  {
    [field: SerializeField] public PolygonCollider2D CameraBoundShape { get; private set; }
  }
}