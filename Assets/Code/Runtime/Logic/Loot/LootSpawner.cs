﻿using Code.Runtime.Infrastructure.Services.Pools.Gem;
using Code.Runtime.Logic.Enemy;
using UnityEngine;

namespace Code.Runtime.Logic.Loot
{
  public class LootSpawner : MonoBehaviour
  {
    [SerializeField] private DeathEnemy _death;

    private IGemPool _gemPool;

    public void Initialize(IGemPool gemPool)
    {
      _gemPool = gemPool;
    }

    private void Awake()
    {
      _death.ActionDead += Spawn;
    }

    private void OnDestroy()
    {
      _death.ActionDead -= Spawn;
    }

    private void Spawn()
    {
      _gemPool.GetGem(transform.position, Quaternion.identity);
    }
  }
}