﻿using Code.Runtime.Infrastructure.Services.PersistantProgress.Data;
using Code.Runtime.Infrastructure.Services.Pools.Gem;
using UnityEngine;

namespace Code.Runtime.Logic.Loot
{
  [RequireComponent(typeof(Collider2D))]
  public class LootPiece : MonoBehaviour
  {
    private CollectedData _collectedData;
    private bool _picked;
    private int _playerLayer;
    private IReleaseGemElement _release;

    private void Awake()
    {
      _playerLayer = LayerMask.NameToLayer("Player");
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
      if (other.gameObject.layer == _playerLayer)
        PickUp();
    }

    public void Initialize(CollectedData collectedData, IReleaseGemElement release)
    {
      _collectedData = collectedData;
      _release = release;
    }

    public void ResetPicked()
    {
      _picked = false;
    }

    private void PickUp()
    {
      if (_picked)
        return;
      _picked = true;
      
      AddGemToCollectedData();
      _release.Release(this);
    }

    private void AddGemToCollectedData()
    {
      _collectedData.ChangeGemCountAt(1);
    }
  }
}