﻿namespace Code.Runtime
{
  public static class Constants
  {
    public const string ProgressKey = "ProgressKey";
  }

  public static class SceneName
  {
    public const string Bootstrap = "BootstraptScene";
    public const string Forest = "Forest";
    public const string Carve = "Carve";
    public const string Main = "BootstrapGameplay";
  }
}