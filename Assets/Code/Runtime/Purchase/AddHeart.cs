﻿using Code.Runtime.Logic.Player;

namespace Code.Runtime.Purchase
{
  public sealed class AddHeart : BasePurchaseAction
  {
    private readonly PlayerHealth _playerHeal;

    public AddHeart(PlayerHealth playerHeal)
    {
      _playerHeal = playerHeal;
    }

    public override void Execute()
    {
      _playerHeal.AddHeart();
    }

    public override bool IsValid()
    {
      return _playerHeal.CanAddHeart();
    }
  }
}