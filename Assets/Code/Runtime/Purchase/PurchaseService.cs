﻿using System;
using System.Collections.Generic;
using Code.Runtime.Logic.Player;
using Code.Runtime.StaticData;

namespace Code.Runtime.Purchase
{
  public class PurchaseService : IPurchaseService
  {
    private Dictionary<PurchaseActionId, BasePurchaseAction> _actions;

    public void Initialize(PlayerCharacter playerCharacter)
    {
      _actions = new Dictionary<PurchaseActionId, BasePurchaseAction>
      {
        [PurchaseActionId.AddHeart] = new AddHeart(playerCharacter.Health),
        [PurchaseActionId.DoubleJump] = new AddJumpInAir(playerCharacter.StateMachine, playerCharacter.AirJumpCounter, 1),
        [PurchaseActionId.TripleJump] = new AddJumpInAir(playerCharacter.StateMachine, playerCharacter.AirJumpCounter,2),
        [PurchaseActionId.Dash] = new AddDash(playerCharacter.StateMachine, playerCharacter.DashCounter),
      };
    }

    public void Execute(PurchaseActionId actionId)
    {
      if (_actions.TryGetValue(actionId, out BasePurchaseAction purchaseAction))
        purchaseAction.Execute();
      else
        throw new Exception($"Dont find action {actionId}");
    }

    public bool IsValid(PurchaseActionId actionId)
    {
      if (_actions.TryGetValue(actionId, out BasePurchaseAction purchaseAction))
        return purchaseAction.IsValid();

      throw new Exception($"Dont find action {actionId}");
    }
  }
}