﻿using Code.Runtime.Logic.PlayerStateMachine;

namespace Code.Runtime.Purchase
{
  public sealed class AddJumpInAir : BasePurchaseAction
  {
    private readonly PlayerFsm _playerFsm;
    private readonly ActCounter _airJumpCounter;
    private readonly int _jumpCount;

    public AddJumpInAir(PlayerFsm playerFsm, ActCounter airJumpCounter, int jumpCount)
    {
      _jumpCount = jumpCount;
      _airJumpCounter = airJumpCounter;
      _playerFsm = playerFsm;
    }

    public override void Execute()
    {
      if (_jumpCount == 1)
      {
        _playerFsm.ActiveStates |= StateID.AirJump;
      }

      _airJumpCounter.MaxCount = _jumpCount;
    }

    public override bool IsValid()
    {
      return _airJumpCounter.MaxCount < _jumpCount;
    }
  }
}