﻿using Code.Runtime.Logic.Player;
using Code.Runtime.StaticData;

namespace Code.Runtime.Purchase
{
  public interface IPurchaseService
  {
    void Execute(PurchaseActionId actionId);
    bool IsValid(PurchaseActionId actionId);
    void Initialize(PlayerCharacter playerCharacter);
  }
}