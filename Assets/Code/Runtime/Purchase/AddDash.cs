﻿using Code.Runtime.Logic.PlayerStateMachine;

namespace Code.Runtime.Purchase
{
  public sealed class AddDash : BasePurchaseAction
  {
    private readonly PlayerFsm _playerFsm;
    private readonly ActCounter _dashCounter;
    
    public AddDash(PlayerFsm playerFsm, ActCounter dashCounter)
    {
      _playerFsm = playerFsm;
      _dashCounter = dashCounter;
    }

    public override void Execute()
    {
      _playerFsm.ActiveStates |= StateID.Dash;
      _dashCounter.MaxCount = 1;
    }

    public override bool IsValid()
    {
      return (_playerFsm.ActiveStates ^ StateID.Dash) == 0;
    }
  }
}