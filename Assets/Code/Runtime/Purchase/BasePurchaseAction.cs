﻿namespace Code.Runtime.Purchase
{
  public abstract class BasePurchaseAction
  {
    public abstract void Execute();
    public abstract bool IsValid();
  }
}