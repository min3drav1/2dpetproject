﻿using System;
using System.Collections.Generic;
using Code.Runtime.Infrastructure.Services.EnemyCollection;
using Code.Runtime.Infrastructure.Services.Factory.Player;
using Code.Runtime.Infrastructure.Services.PersistantProgress;
using Code.Runtime.Infrastructure.Services.PersistantProgress.Data;
using Code.Runtime.Infrastructure.Services.SaveLoad;
using Code.Runtime.Infrastructure.Services.SceneGraph;
using Code.Runtime.Infrastructure.Services.SceneLoad;
using Code.Runtime.Infrastructure.UI;
using Code.Runtime.Logic.Player;
using Code.Runtime.Utility;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

namespace Code.Runtime.Infrastructure.StateMachine.State
{
  public class LoadLevelState : IParameterEnter<LoadLevelState.Options>
  {
    private readonly ICurtainLoader _curtain;
    private readonly IPlayerRegistry _playerRegistry;
    private readonly IPersistantProgressService _persistant;
    private readonly ISaveLoadService _saveLoadService;
    private readonly IGameStateMachine _stateMachine;
    private readonly ISceneGraphService _sceneGraphService;
    private readonly ISceneLoader _sceneLoader;
    private readonly IEnemyCollectionService _enemyCollection;

    private Options _options;
    private IReadOnlyList<LevelData> _addScenes;

    public LoadLevelState(
      IGameStateMachine stateMachine,
      ICurtainLoader curtain,
      IPlayerRegistry playerRegistry,
      ISaveLoadService saveLoadService,
      IPersistantProgressService persistant,
      ISceneGraphService sceneGraphService,
      ISceneLoader sceneLoader,
      IEnemyCollectionService enemyCollection)
    {
      _stateMachine = stateMachine;
      _curtain = curtain;
      _playerRegistry = playerRegistry;
      _saveLoadService = saveLoadService;
      _persistant = persistant;
      _sceneGraphService = sceneGraphService;
      _sceneLoader = sceneLoader;
      _enemyCollection = enemyCollection;
    }

    public void Enter(Options options)
    {
      Debug.Log("Load level state");
      _curtain.Show();

      _options = options;
      string currentLevelName = _options.LevelName;
      Assert.AreNotEqual(currentLevelName, SceneName.Main);

      OnLevelLoaded(currentLevelName).Forget();
    }

    public void Exit()
    {
    }

    private async UniTaskVoid OnLevelLoaded(string currentLevelName)
    {
      _sceneGraphService.CleanUp();
      IReadOnlyList<LevelData> levelData = _sceneGraphService.SetCurrentLevel(currentLevelName);
      try
      {
        await LoadSceneData(levelData);
        SetupWorld();
      }
      catch (Exception exception)
      {
        Debug.LogError(exception);
        throw;
      }


      _stateMachine.Enter<GameLoopState>();
    }

    private async UniTask LoadSceneData(IReadOnlyList<LevelData> sceneDataList)
    {
      _enemyCollection.CleanUp();
      foreach (LevelData levelData in sceneDataList)
      {
        await _sceneLoader.LoadAsync(levelData.SceneName, LoadSceneMode.Additive, true);
        await _enemyCollection.LoadForScene(levelData.SceneName, levelData.EnemySpawnData, false);
      }
    }

    private void SetupWorld()
    {
      InitWorld();
      _saveLoadService.Load();
      _curtain.Hide();
    }


    private void InitWorld()
    {
      SetFoxPosition();
    }

    private void SetFoxPosition()
    {
      PlayerCharacter player = _playerRegistry.Player;
      Vector3 position = GetStartPlayPosition();
      player.gameObject.SetActive(false);
      player.transform.position = position;
      player.gameObject.SetActive(true);
    }

    private Vector3 GetStartPlayPosition()
    {
      PositionOnLevel positionOnLevel = _persistant.PlayerProgress.PositionOnLevel;
      Vector3 position;

      switch (_options.PositionType)
      {
        case Options.PositionsType.StartGame:
          if (_sceneGraphService.CurrentLevel.IsHasSpawnPoint == false)
          {
            throw new Exception("cant find scene data");
          }

          position = _sceneGraphService.CurrentLevel.SpawnPoint;
          break;
        case Options.PositionsType.Saved:
          position = positionOnLevel.Position.AsUnityVector();
          break;
        default:
          throw new ArgumentOutOfRangeException();
      }

      return position;
    }

    public class Options
    {
      public enum PositionsType
      {
        StartGame = 0,
        Saved = 1
      }

      public string LevelName;
      public PositionsType PositionType;
    }
  }
}