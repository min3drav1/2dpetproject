﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Code.Runtime.Infrastructure.Services.EnemyCollection;
using Code.Runtime.Infrastructure.Services.SceneGraph;
using Code.Runtime.Infrastructure.Services.SceneLoad;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Code.Runtime.Infrastructure.StateMachine.State
{
  public class UpdateLevelState : IParameterEnter<string>
  {
    private readonly ISceneGraphService _sceneGraphService;
    private readonly ISceneLoader _sceneLoader;
    private readonly IGameStateMachine _stateMachine;
    private readonly IEnemyCollectionService _enemyCollection;

    private readonly List<LevelData> _addLevels = new();
    private readonly List<LevelData> _removeLevels = new();

    private int _added = 0;

    public UpdateLevelState(ISceneGraphService sceneGraphService,
      ISceneLoader sceneLoader,
      IGameStateMachine stateMachine,
      IEnemyCollectionService enemyCollection
    )
    {
      _sceneGraphService = sceneGraphService;
      _sceneLoader = sceneLoader;
      _stateMachine = stateMachine;
      _enemyCollection = enemyCollection;
    }

    public void Enter(string sceneName)
    {
#pragma warning disable 4014
      UpdateLevels(sceneName);
#pragma warning restore 4014

      _added = 0;
      _addLevels.Clear();
      _removeLevels.Clear();

      _sceneGraphService.UpdateCurrentLevel(sceneName, _addLevels, _removeLevels);

      foreach (LevelData levelData in _removeLevels)
      {
        _sceneLoader.Unload(levelData.SceneName);
      }

      foreach (LevelData levelData in _addLevels)
      {
        ++_added;
        _sceneLoader.Load(levelData.SceneName, OnSceneLoaded, LoadSceneMode.Additive);
      }
    }

    private async UniTaskVoid UpdateLevels(string sceneName)
    {
      List<LevelData> addLevels = new();
      List<LevelData> removeLevels = new();

      _sceneGraphService.UpdateCurrentLevel(sceneName, addLevels, removeLevels);

      try
      {
        foreach (LevelData removeLevel in removeLevels)
        {
          _enemyCollection.UnloadForScene(removeLevel.SceneName);
          await _sceneLoader.UnloadAsync(removeLevel.SceneName);
        }

        foreach (LevelData addLevel in addLevels)
        {
          await _sceneLoader.LoadAsync(addLevel.SceneName, LoadSceneMode.Additive, true);
          await _enemyCollection.LoadForScene(addLevel.SceneName, addLevel.EnemySpawnData, true);
        }
      }
      catch (Exception exception)
      {
        Debug.LogError(exception);
        throw;
      }
    }

    private void OnSceneLoaded()
    {
      --_added;
      if (_added == 0)
      {
        _stateMachine.Enter<GameLoopState>();
      }
    }

    public void Exit()
    {
    }
  }
}