﻿using System;
using Code.Runtime.DebugServices;
using Code.Runtime.Infrastructure.Services.Factory.Camera;
using Code.Runtime.Infrastructure.Services.Factory.Hud;
using Code.Runtime.Infrastructure.Services.Factory.Player;
using Code.Runtime.Infrastructure.Services.PersistantProgress;
using Code.Runtime.Logic.Player;
using Code.Runtime.Purchase;
using Cysharp.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace Code.Runtime.Infrastructure.StateMachine.State
{
  public class LoadPlayerState : IEnter
  {
    private readonly IPlayerFactory _playerFactory;
    private readonly IPersistantProgressService _persistant;
    private readonly IGameStateMachine _stateMachine;
    private readonly IPurchaseService _purchaseService;
    private readonly IHudFactory _hudFactory;
    private readonly ICameraFactory _cameraFactory;
    

#if UNITY_EDITOR
    [InjectOptional] private DebugLoadScene _debugLoadScene;
#endif

    public LoadPlayerState(
      IGameStateMachine stateMachine,
      IPlayerFactory playerFactory,
      IPersistantProgressService persistant,
      IPurchaseService purchaseService,
      IHudFactory hudFactory,
      ICameraFactory cameraFactory
    )
    {
      _stateMachine = stateMachine;
      _playerFactory = playerFactory;
      _persistant = persistant;
      _purchaseService = purchaseService;
      _hudFactory = hudFactory;
      _cameraFactory = cameraFactory;
    }

    private string PersistantLevelName
      => _persistant.PlayerProgress.PositionOnLevel.LevelName;

    private bool FirstLoad =>
      _persistant.PlayerProgress.PositionOnLevel.Position == null;

    public void Enter()
    {
      Debug.Log("Entry LoadPlayerState");
      InitPlayerStaff().Forget();
    }

    private async UniTaskVoid InitPlayerStaff()
    {
      try
      {
        PlayerCharacter player = await InitFox();
        await InitHud(player);
        await CreateCamera(player);

        _purchaseService.Initialize(player);
      }
      catch (Exception exception)
      {
        Debug.LogError(exception);
        throw;
      }

      LoadLevelState.Options options = new LoadLevelState.Options
      {
        LevelName = PersistantLevelName,
        PositionType = FirstLoad
          ? LoadLevelState.Options.PositionsType.StartGame
          : LoadLevelState.Options.PositionsType.Saved
      };

#if UNITY_EDITOR

      if (_debugLoadScene.IsLoadDebugScene)
      {
        if (string.IsNullOrEmpty(_debugLoadScene.DebugScene))
        {
          Debug.LogError("Debug scene is not setup");
          return;
        }

        options = new LoadLevelState.Options
        {
          LevelName = _debugLoadScene.DebugScene,
          PositionType = LoadLevelState.Options.PositionsType.StartGame,
        };
      }
#endif

      _stateMachine.Enter<LoadLevelState, LoadLevelState.Options>(options);
    }

    public void Exit()
    {
    }

    private async UniTask<PlayerCharacter> InitFox()
    {
      PlayerCharacter player = await _playerFactory.Create();
      player.gameObject.SetActive(false);
      return player;
    }

    private async UniTask InitHud(PlayerCharacter player)
    {
      if (_hudFactory.HUD != null)
      {
        _hudFactory.HUD.Reset();
        return;
      }

      await _hudFactory.CreateHud(player.Health);
    }

    private async UniTask CreateCamera(PlayerCharacter player)
    {
      if (_cameraFactory.Camera)
      {
        return;
      }

      await _cameraFactory.CreateCamera(player.transform);
    }
  }
}