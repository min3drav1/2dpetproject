﻿using Code.Runtime.Infrastructure.Services.Windows;
using Code.Runtime.StaticData;

namespace Code.Runtime.Infrastructure.StateMachine.State
{
  public class GameOverState : IEnter
  {
    private readonly IWindowsService _windowsService;

    public GameOverState(IWindowsService windowsService)
    {
      _windowsService = windowsService;
    }

    public void Enter()
    {
      _windowsService.Open(WindowID.GameOver);
    }

    public void Exit()
    {
    }
  }
}