﻿using UnityEngine;

namespace Code.Runtime.Infrastructure.StateMachine.State
{
  public class GameLoopState : IEnter
  {
    public GameLoopState()
    {
    }

    public void Enter()
    {
      Debug.Log("Game loop state");
    }

    public void Exit()
    {
    }
  }
}