﻿using System;
using Code.Runtime.Infrastructure.Services;
using Code.Runtime.Infrastructure.Services.AssetManager;
using Code.Runtime.Infrastructure.Services.Pools.Gem;
using Code.Runtime.Infrastructure.Services.Windows;
using Code.Runtime.StaticData;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Code.Runtime.Infrastructure.StateMachine.State
{
  public class WarmUpServicesState : IEnter
  {
    private readonly IGameStateMachine _stateMachine;
    private readonly IAssetProvider _assetProvider;
    private readonly IWindowsService _windowService;
    private readonly IGemPool _gemPool;
    private readonly IInitialize _initialize;
    private readonly IAsyncPreInitialize _asyncInitialize;

    public WarmUpServicesState(IGameStateMachine stateMachine,
      IAssetProvider assetProvider,
      IWindowsService windowService,
      IGemPool gemPool,
      IInitialize initialize,
      IAsyncPreInitialize asyncInitialize)
    {
      _stateMachine = stateMachine;
      _assetProvider = assetProvider;
      _windowService = windowService;
      _gemPool = gemPool;
      _initialize = initialize;
      _asyncInitialize = asyncInitialize;
    }

    public void Enter()
    {
      Debug.Log("Entry WarmUpServicesState");
      InitializeServices().Forget();
    }

    private async UniTaskVoid InitializeServices()
    {
      try
      {
        await _asyncInitialize.AsyncInitialize();
        _initialize.Initialize();
        await _assetProvider.Initialize();
        await _gemPool.Initialize();

        await _windowService.WarmUp(new[] { WindowID.Abilities, WindowID.Menu, WindowID.GameOver });

      }
      catch (Exception exception)
      {
        Debug.LogError(exception);
        throw;
      }

      _stateMachine.Enter<LoadPlayerState>();
    }

    public void Exit()
    {
    }
  }
}