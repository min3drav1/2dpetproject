﻿using Code.Runtime.Infrastructure.Services.PersistantProgress;
using Code.Runtime.Infrastructure.Services.PersistantProgress.Data;
using Code.Runtime.Infrastructure.Services.SaveLoad;
using UnityEngine;

namespace Code.Runtime.Infrastructure.StateMachine.State
{
  public class LoadProgressState : IEnter
  {
    private readonly IPersistantProgressService _persistant;
    private readonly ISaveLoadService _saveLoadServices;
    private readonly IGameStateMachine _stateMachine;

    public LoadProgressState(
      IGameStateMachine stateMachine,
      ISaveLoadService saveLoadServices,
      IPersistantProgressService persistant)
    {
      _stateMachine = stateMachine;
      _saveLoadServices = saveLoadServices;
      _persistant = persistant;
    }

    public void Enter()
    {
      Debug.Log("Load progress state");
      UploadProgress();
      _stateMachine.Enter<WarmUpServicesState>();
    }

    public void Exit()
    {
    }

    private void UploadProgress()
    {
      _persistant.PlayerProgress = _saveLoadServices.UploadProgress() ?? NewProgress();
    }

    private PlayerProgress NewProgress()
    {
      PlayerProgress playerProgress = new();

      playerProgress.PositionOnLevel.LevelName = SceneName.Forest;
      playerProgress.Stats.CurrentHp = 3;
      playerProgress.Stats.MaxHp = 3;

      return playerProgress;
    }
  }
}