﻿namespace Code.Runtime.Infrastructure.StateMachine.State
{
  public interface IState
  {
    void Exit();
  }

  public interface IEnter : IState
  {
    void Enter();
  }

  public interface IParameterEnter<TPlayload> : IState
  {
    void Enter(TPlayload parameter);
  }
}