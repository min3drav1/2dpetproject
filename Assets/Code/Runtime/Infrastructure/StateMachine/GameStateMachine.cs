﻿using System;
using System.Collections.Generic;
using Code.Runtime.Infrastructure.StateMachine.State;
using UnityEngine.Assertions;
using Zenject;

namespace Code.Runtime.Infrastructure.StateMachine
{
  public class GameStateMachine : IGameStateMachine
  {
    private IState _currentState;
    private Dictionary<Type, IState> _states;

    [Inject]
    public void AddStates(
      WarmUpServicesState warmUpServicesState,
      LoadProgressState loadProgressState,
      LoadPlayerState loadPlayerState,
      LoadLevelState loadLevelState,
      GameLoopState gameLoopState,
      GameOverState gameOverState,
      UpdateLevelState updateLevelState)
    {
      _states = new Dictionary<Type, IState>
      {
        [typeof(WarmUpServicesState)] = warmUpServicesState,
        [typeof(LoadProgressState)] = loadProgressState,
        [typeof(LoadPlayerState)] = loadPlayerState,
        [typeof(LoadLevelState)] = loadLevelState,
        [typeof(GameLoopState)] = gameLoopState,
        [typeof(GameOverState)] = gameOverState,
        [typeof(UpdateLevelState)] = updateLevelState,
      };
    }

    public void Enter<TState>() where TState : IEnter
    {
      _currentState?.Exit();
      _currentState = _states[typeof(TState)];
      ((IEnter)_currentState).Enter();
    }

    public void Enter<TState, TPayload>(TPayload payload) where TState : IParameterEnter<TPayload>
    {
      _currentState?.Exit();
      Assert.IsTrue(_states.ContainsKey(typeof(TState)), $"Game state machine don't has state {typeof(TState)}");
      _currentState = _states[typeof(TState)];
      ((IParameterEnter<TPayload>)_currentState).Enter(payload);
    }
  }
}