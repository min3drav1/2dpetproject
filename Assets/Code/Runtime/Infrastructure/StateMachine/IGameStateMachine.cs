﻿using Code.Runtime.Infrastructure.StateMachine.State;

namespace Code.Runtime.Infrastructure.StateMachine
{
  public interface IGameStateMachine
  {
    void Enter<TState>() where TState : IEnter;
    void Enter<TState, TPayload>(TPayload payload) where TState : IParameterEnter<TPayload>;
  }
}