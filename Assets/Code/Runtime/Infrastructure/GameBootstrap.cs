using Code.Runtime.Infrastructure.StateMachine;
using Code.Runtime.Infrastructure.StateMachine.State;
using UnityEngine;
using Zenject;

namespace Code.Runtime.Infrastructure
{
  public class GameBootstrap : MonoBehaviour
  {
    [Inject] private IGameStateMachine _stateMachine;

    private void Start()
    {
      _stateMachine.Enter<LoadProgressState>();
    }
  }
}