﻿namespace Code.Runtime.Infrastructure.UI
{
  public interface ICurtainLoader
  {
    void Show();
    void Hide();
  }
}