﻿using System.Collections;
using UnityEngine;

namespace Code.Runtime.Infrastructure.UI
{
  public class CurtainLoader : MonoBehaviour, ICurtainLoader
  {
    private const float DeltaAlpha = 0.01f;
    [SerializeField] private CanvasGroup _curtain;

    private Coroutine _fadeInOut;

    public void Awake()
    {
      DontDestroyOnLoad(this);
    }

    public void Show()
    {
      StopPreviousFade();
      _fadeInOut = StartCoroutine(Fadeout());
    }

    public void Hide()
    {
      StopPreviousFade();
      _fadeInOut = StartCoroutine(Fadein());
    }

    private void StopPreviousFade()
    {
      if (_fadeInOut != null)
        StopCoroutine(_fadeInOut);
    }

    private IEnumerator Fadein()
    {
      yield return FadeInOrOut(-1, false);
    }

    private IEnumerator Fadeout()
    {
      yield return FadeInOrOut(1, true);
    }

    private IEnumerator FadeInOrOut(float sing, bool finalActiveState)
    {
      while (_curtain.alpha > 0)
      {
        _curtain.alpha += sing * DeltaAlpha;
        yield return null;
      }

      _curtain.gameObject.SetActive(finalActiveState);
    }
  }
}