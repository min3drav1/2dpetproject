﻿using Code.Runtime.Infrastructure.StateMachine;
using Code.Runtime.Infrastructure.StateMachine.State;
using Zenject;

namespace Code.Runtime.Infrastructure.Installers
{
  public class StatesSubContainer : Installer
  {
    public override void InstallBindings()
    {
      Container
        .Bind<WarmUpServicesState>()
        .AsSingle();

      Container
        .Bind<LoadProgressState>()
        .AsSingle();

      Container
        .Bind<LoadPlayerState>()
        .AsSingle();

      Container
        .Bind<LoadLevelState>()
        .AsSingle();

      Container
        .Bind<UpdateLevelState>()
        .AsSingle();

      Container
        .Bind<GameLoopState>()
        .AsSingle();

      Container
        .Bind<GameOverState>()
        .AsSingle();

      Container
        .Bind<IGameStateMachine>()
        .To<GameStateMachine>()
        .AsSingle();
    }
  }
}