﻿using Code.Runtime.DebugServices;
using Code.Runtime.Infrastructure.Services;
using Code.Runtime.Infrastructure.Services.AssetManager;
using Code.Runtime.Infrastructure.Services.EnemyCollection;
using Code.Runtime.Infrastructure.Services.Factory.Camera;
using Code.Runtime.Infrastructure.Services.Factory.Enemy;
using Code.Runtime.Infrastructure.Services.Factory.Hud;
using Code.Runtime.Infrastructure.Services.Factory.Player;
using Code.Runtime.Infrastructure.Services.Factory.UI;
using Code.Runtime.Infrastructure.Services.Input;
using Code.Runtime.Infrastructure.Services.PersistantProgress;
using Code.Runtime.Infrastructure.Services.Pools.Gem;
using Code.Runtime.Infrastructure.Services.SaveLoad;
using Code.Runtime.Infrastructure.Services.SceneGraph;
using Code.Runtime.Infrastructure.Services.SceneLoad;
using Code.Runtime.Infrastructure.Services.StaticData;
using Code.Runtime.Infrastructure.Services.Windows;
using Code.Runtime.Infrastructure.StateMachine;
using Code.Runtime.Infrastructure.UI;
using Code.Runtime.Purchase;
using UnityEngine;
using Zenject;

namespace Code.Runtime.Infrastructure.Installers
{
  public class ApplicationInstaller : MonoInstaller
  {
    [SerializeField] private CurtainLoader _curtain;

#if UNITY_EDITOR
    [SerializeField] private DebugLoadScene _debugLoadScene;
#endif

    public override void InstallBindings()
    {
#if UNITY_EDITOR
      Container
        .Bind<DebugLoadScene>()
        .FromInstance(_debugLoadScene)
        .AsSingle();
#endif
      
#if ENABLE_INPUT_SYSTEM
      Container
        .Bind<IInputSystem>()
        .To<NewInputSystem>()
        .AsSingle();
#else
      Container
        .Bind(typeof(IInputSystem), typeof(ITickable))
        .To<OldInputSystem>()
        .AsSingle();
#endif

      Container
        .Bind<IGemPool>()
        .To<GemPool>()
        .AsSingle();

      Container
        .Bind(typeof(IAssetProvider), typeof(IReleaseAsset))
        .To<AssetProvider>()
        .AsSingle();

      Container
        .Bind<IAssetsPath>()
        .To<AssetsPath>()
        .AsSingle();

      Container
        .Bind(typeof(IAsyncPreInitialize),typeof(IStaticDataService))
        .To<StaticDataService>()
        .AsSingle();

      Container
        .Bind<IPersistantProgressService>()
        .To<PersistantProgressService>()
        .AsSingle();

      Container
        .Bind<ISaveLoadService>()
        .To<FileSaveLoadService>()
        .AsSingle();

      Container
        .Bind<IPurchaseService>()
        .To<PurchaseService>()
        .AsSingle();

      Container
        .Bind<IUIFactory>()
        .To<UIFactory>()
        .AsSingle();

      Container
        .Bind<IWindowsService>()
        .To<WindowsService>()
        .AsSingle();

      Container
        .Bind(typeof(IPlayerFactory), typeof(IPlayerRegistry))
        .To<PlayerFactory>()
        .AsSingle();

      Container
        .Bind<IEnemyFactory>()
        .To<EnemyFactory>()
        .AsSingle();

      Container
        .Bind<ICurtainLoader>()
        .To<CurtainLoader>()
        .FromInstance(Instantiate(_curtain))
        .AsSingle();

      Container
        .Bind<ISceneLoader>()
        .To<SceneLoader>()
        .AsSingle();
      
      Container
        .Bind<IHudFactory>()
        .To<HudFactory>()
        .AsSingle();

      Container
        .Bind<ICameraFactory>()
        .To<CameraFactory>()
        .AsSingle();

      Container
        .Bind(typeof(ISceneGraphService), typeof(IInitialize))
        .To<SceneGraphService>()
        .AsSingle();
      
      Container
        .Bind<IEnemyCollectionService>()
        .To<EnemyCollectionService>()
        .AsSingle();

      Container
        .Bind<IGameStateMachine>()
        .FromSubContainerResolve()
        .ByInstaller<StatesSubContainer>()
        .AsSingle();
    }
  }
}