﻿using System;
using System.Collections.Generic;
using System.Linq;
using Code.Runtime.Infrastructure.Services.AssetManager;
using Code.Runtime.Infrastructure.Services.Factory.Enemy;
using Code.Runtime.Logic.Spawner;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Code.Runtime.Infrastructure.Services.EnemyCollection
{
  class EnemyCollectionService : IEnemyCollectionService
  {
    private readonly Dictionary<string, GameObject[]> _storage = new();
    private readonly IEnemyFactory _enemyFactory;
    private readonly IReleaseAsset _releaseAsset;

    public EnemyCollectionService(IEnemyFactory enemyFactory, IReleaseAsset releaseAsset)
    {
      _enemyFactory = enemyFactory;
      _releaseAsset = releaseAsset;
    }

    public async UniTask LoadForScene(string sceneName, List<EnemySpawnData> enemySpawnData, bool isActive)
    {
      GameObject[] enemies = new GameObject[enemySpawnData.Count];
      for (var i = 0; i < enemySpawnData.Count; i++)
      {
        if (enemySpawnData[i].PatrolPoints == null || enemySpawnData[i].PatrolPoints.Length == 0)
        {
          throw new Exception("Don't setup patrol points");
        }

        GameObject enemy = await _enemyFactory.SpawnEnemy(enemySpawnData[i].EnemyId, enemySpawnData[i].Positions, enemySpawnData[i].PatrolPoints.ToList());
        enemies[i] = enemy;
      }

      _storage.Add(sceneName, enemies);
    }

    public void UnloadForScene(string sceneName)
    {
      if (_storage.TryGetValue(sceneName, out GameObject[] enemies) == false)
      {
        throw new Exception($"Dont storage enemy in collection for scene {sceneName}");
      }

      ReleaseEnemies(enemies);
      _storage.Remove(sceneName);
    }

    public void CleanUp()
    {
      foreach (GameObject[] enemies in _storage.Values)
      {
        ReleaseEnemies(enemies);
      }

      _storage.Clear();
    }

    private void ReleaseEnemies(GameObject[] enemies)
    {
      for (int i = 0; i < enemies.Length; i++)
      {
        if (enemies[i] == null)
        {
          continue;
        }

        _releaseAsset.Release(enemies[i]);
      }
    }
  }
}