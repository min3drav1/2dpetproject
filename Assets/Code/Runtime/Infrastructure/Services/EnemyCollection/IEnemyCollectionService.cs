﻿using System.Collections.Generic;
using Code.Runtime.Logic.Spawner;
using Cysharp.Threading.Tasks;

namespace Code.Runtime.Infrastructure.Services.EnemyCollection
{
  public interface IEnemyCollectionService
  {
    UniTask LoadForScene(string sceneName, List<EnemySpawnData> enemySpawnData, bool isActive);
    void UnloadForScene(string sceneName);
    void CleanUp();
  }
}