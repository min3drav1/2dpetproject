﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace Code.Runtime.Infrastructure.Services.SceneLoad
{
  public class SceneLoader : ISceneLoader
  {
    private readonly Dictionary<AsyncOperation, Action> _loadCompletedCallbacks = new();
    private readonly Dictionary<string, AsyncOperationHandle<SceneInstance>> _sceneHandles = new();
    private readonly HashSet<string> _loadedScene = new();

    public void Load(string levelName, Action loadCompleted = null, LoadSceneMode mode = LoadSceneMode.Single)
    {
      if (_loadedScene.Contains(levelName))
      {
        loadCompleted?.Invoke();
        return;
      }

      _loadedScene.Add(levelName);
      AsyncOperation handle = SceneManager.LoadSceneAsync(levelName, mode);
      if (loadCompleted != null)
      {
        handle.completed += LevelLoaded;
        _loadCompletedCallbacks.Add(handle, loadCompleted);
      }
    }

    public async UniTask LoadAsync(string levelName, LoadSceneMode mode = LoadSceneMode.Single,
      bool activateOnLoad = false)
    {
      if (_sceneHandles.TryGetValue(levelName, out AsyncOperationHandle<SceneInstance> storageHandle))
      {
        if (storageHandle.IsDone)
        {
          return;
        }

        await storageHandle.Task;
      }

      AsyncOperationHandle<SceneInstance> handle = Addressables.LoadSceneAsync(levelName, mode, activateOnLoad);
      _sceneHandles.Add(levelName, handle);
      await handle.Task;
    }

    public void Unload(string sceneName)
    {
      if (_loadedScene.Contains(sceneName) == false)
      {
        Debug.LogError($"{sceneName} is not loaded");
        return;
      }

      _loadedScene.Remove(sceneName);
      SceneManager.UnloadSceneAsync(sceneName);
    }
    
    public async UniTask UnloadAsync(string sceneName)
    {
      if (_sceneHandles.TryGetValue(sceneName, out AsyncOperationHandle<SceneInstance> storageHandle) == false)
      {
        Debug.LogError($"{sceneName} is not loaded");
        return;

      }

      _sceneHandles.Remove(sceneName);
      AsyncOperationHandle<SceneInstance> handle = Addressables.UnloadSceneAsync(storageHandle);
      await handle.Task;
    }

    public void SetSceneActive(string sceneName)
    {
      SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
    }

    private void LevelLoaded(AsyncOperation handle)
    {
      if (_loadCompletedCallbacks.TryGetValue(handle, out Action callback) == false)
      {
        return;
      }

      callback?.Invoke();
      _loadCompletedCallbacks.Remove(handle);
    }
  }
}