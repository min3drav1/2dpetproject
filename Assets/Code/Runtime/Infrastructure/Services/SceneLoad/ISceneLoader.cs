﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine.SceneManagement;

namespace Code.Runtime.Infrastructure.Services.SceneLoad
{
  public interface ISceneLoader
  {
    void Load(string levelName, Action loadCompleted = null, LoadSceneMode mode = LoadSceneMode.Single);
    void Unload(string sceneName);
    void SetSceneActive(string sceneName);
    UniTask LoadAsync(string levelName, LoadSceneMode mode = LoadSceneMode.Single, bool activateOnLoad = false);
    UniTask UnloadAsync(string sceneName);
  }
}