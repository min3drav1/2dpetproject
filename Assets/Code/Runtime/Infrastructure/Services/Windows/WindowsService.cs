﻿using System.Collections.Generic;
using Code.Runtime.Infrastructure.Services.Factory.UI;
using Code.Runtime.StaticData;
using Code.Runtime.UI.Window;
using Cysharp.Threading.Tasks;

namespace Code.Runtime.Infrastructure.Services.Windows
{
  public class WindowsService : IWindowsService
  {
    private readonly IUIFactory _uiFactory;

    private readonly Dictionary<WindowID, BaseWindow> _windowCache = new Dictionary<WindowID, BaseWindow>();
    private BaseWindow _currentOpenWindow;

    public WindowsService(IUIFactory uiFactory)
    {
      _uiFactory = uiFactory;
    }

    public BaseWindow Open(WindowID windowID)
    {
      BaseWindow window = GetWindow(windowID);
      
      UpdateCurrentWindowState(window);

      return _currentOpenWindow;
    }

    public async UniTask WarmUp(IEnumerable<WindowID> windowIds)
    {
      await _uiFactory.CreateUIRoot();
      foreach (WindowID windowId in windowIds)
      {
        if (_windowCache.ContainsKey(windowId))
        {
          continue;
        }
        BaseWindow window = _uiFactory.Create(windowId);
        window.Close();
        _windowCache.Add(windowId, window);
      }
    }

    public void CleanUp()
    {
      foreach (BaseWindow window in _windowCache.Values)
      {
        _uiFactory.Release(window);
      }
      
      _windowCache.Clear();
    }

    private BaseWindow GetWindow(WindowID windowID)
    {
      if (_windowCache.TryGetValue(windowID, out BaseWindow window) == false)
      {
        window = _uiFactory.Create(windowID);
        _windowCache[windowID] = window;
      }
      
      return window;
    }

    private void UpdateCurrentWindowState(BaseWindow window)
    {
      if (_currentOpenWindow == window)
      {
        if (_currentOpenWindow.IsOpen())
        {
          _currentOpenWindow.Close();
        }
        else
        {
          _currentOpenWindow.Open();
        }
      }
      else
      {
        if (_currentOpenWindow != null && _currentOpenWindow.IsOpen())
        {
          _currentOpenWindow.Close();
        }

        _currentOpenWindow = window;
        _currentOpenWindow.Open();
      }
    }
  }
}