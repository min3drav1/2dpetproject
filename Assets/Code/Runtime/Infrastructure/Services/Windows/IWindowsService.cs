﻿using System.Collections.Generic;
using Code.Runtime.StaticData;
using Code.Runtime.UI.Window;
using Cysharp.Threading.Tasks;

namespace Code.Runtime.Infrastructure.Services.Windows
{
  public interface IWindowsService
  {
    BaseWindow Open(WindowID windowID);
    UniTask WarmUp(IEnumerable<WindowID> windowIds);
    void CleanUp();
  }
}