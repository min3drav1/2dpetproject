﻿using System;

namespace Code.Runtime.Infrastructure.Services.Input
{
  public interface IInputSystem
  {
    event Action PressMenuWindow;
    event Action PressAbilityWindow;
    
    float HorizontalDirection { get; }
    bool IsJumpDown { get; }
    bool IsCrouch { get; }
    bool UpPressed { get; }
    bool IsDash { get; }
    bool IsAttack { get; }
    bool IsJumpUp { get; }
  }
}