﻿using System;
using UnityEngine;
using Zenject;

#if !ENABLE_INPUT_SYSTEM
namespace Code.Runtime.Infrastructure.Services.Input
{
  public class OldInputSystem : IInputSystem, ITickable
  {
    private const string HorizontalAxis = "Horizontal";

    private const KeyCode JumpKey = KeyCode.Space;
    private const KeyCode CrouchKey = KeyCode.S;
    private const KeyCode UpKey = KeyCode.W;
    private const KeyCode MenuKey = KeyCode.Escape;
    private const KeyCode AbilityKey = KeyCode.J;

    public event Action PressMenuWindow;
    public event Action PressAbilityWindow;

    public float HorizontalDirection =>
      UnityEngine.Input.GetAxis(HorizontalAxis);

    public bool IsJumpDown =>
      UnityEngine.Input.GetKeyDown(JumpKey);

    public bool IsCrouch =>
      UnityEngine.Input.GetKey(CrouchKey);

    public bool UpPressed =>
      UnityEngine.Input.GetKey(UpKey);

    public bool IsMenuWindowPress =>
      UnityEngine.Input.GetKeyDown(MenuKey);

    public bool IsAbilityWindowPress =>
      UnityEngine.Input.GetKeyDown(AbilityKey);

    public void Tick()
    {
      if (IsMenuWindowPress)
      {
        PressMenuWindow?.Invoke();
      }

      if (IsAbilityWindowPress)
      {
        PressAbilityWindow?.Invoke();
      }
    }
  }
}
#endif