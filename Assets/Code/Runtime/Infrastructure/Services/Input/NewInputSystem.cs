﻿using System;
using UnityEngine.InputSystem;

#if ENABLE_INPUT_SYSTEM
namespace Code.Runtime.Infrastructure.Services.Input
{
  public class NewInputSystem : IInputSystem
  {
    private readonly NewInput _input;

    public event Action PressMenuWindow;
    public event Action PressAbilityWindow;

    public float HorizontalDirection => _input.Gameplya.HorizontalMove.ReadValue<float>();
    public bool IsJumpDown => _input.Gameplya.Jump.WasPerformedThisFrame();
    public bool IsJumpUp => _input.Gameplya.Jump.WasReleasedThisFrame();
    public bool IsCrouch => _input.Gameplya.Crouch.IsPressed();
    public bool UpPressed => _input.Gameplya.Up.IsPressed();
    public bool IsDash => _input.Gameplya.Dash.IsPressed();
    public bool IsAttack => _input.Gameplya.Attack.IsPressed();

    public NewInputSystem()
    {
      _input = new NewInput();
      _input.Enable();
      _input.Gameplya.MenuWindow.performed += OnOpenMenuWindowPressed;
      _input.Gameplya.AbilityWindow.performed += OnOpenAbilityWindowPressed;
    }

    private void OnOpenMenuWindowPressed(InputAction.CallbackContext obj)
    {
      PressMenuWindow?.Invoke();
    }

    private void OnOpenAbilityWindowPressed(InputAction.CallbackContext obj)
    {
      PressAbilityWindow?.Invoke();
    }

  }
}
#endif