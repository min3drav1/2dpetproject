﻿namespace Code.Runtime.Infrastructure.Services
{
  public interface IInitialize
  {
    void Initialize();
  }
}