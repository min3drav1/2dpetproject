﻿using System.Collections.Generic;
using Code.Runtime.Infrastructure.Services.PersistantProgress.Data;

namespace Code.Runtime.Infrastructure.Services.SaveLoad
{
  public interface ISaveLoadService
  {
    List<ISave> ProgressWriters { get; }
    List<ILoad> ProgressReaders { get; }
    void Save();
    void Load();
    PlayerProgress UploadProgress();
  }
}