﻿using Code.Runtime.Infrastructure.Services.PersistantProgress.Data;

namespace Code.Runtime.Infrastructure.Services.SaveLoad
{
  public interface ILoad
  {
    public void LoadProgress(PlayerProgress progress);
  }
}