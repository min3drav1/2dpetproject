﻿using Code.Runtime.Infrastructure.Services.PersistantProgress.Data;

namespace Code.Runtime.Infrastructure.Services.SaveLoad
{
  public interface ISave
  {
    public void UpdateProgress(PlayerProgress progress);
  }
}