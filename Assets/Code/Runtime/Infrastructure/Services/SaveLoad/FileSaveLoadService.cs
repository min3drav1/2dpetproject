﻿using System.Collections.Generic;
using System.IO;
using Code.Runtime.Infrastructure.Services.PersistantProgress;
using Code.Runtime.Infrastructure.Services.PersistantProgress.Data;
using Code.Runtime.Utility;
using UnityEngine;

namespace Code.Runtime.Infrastructure.Services.SaveLoad
{
  public class FileSaveLoadService : ISaveLoadService
  {
    private static readonly string SaveFileName = "SaveFile.txt";
    private readonly IPersistantProgressService _progress;

    public FileSaveLoadService(IPersistantProgressService progress)
    {
      _progress = progress;
    }

    public static string PersistantProgressFilePath =>
      Path.Combine(Application.persistentDataPath, SaveFileName);

    public List<ISave> ProgressWriters { get; } = new();
    public List<ILoad> ProgressReaders { get; } = new();

    public void Save()
    {
      foreach (ISave save in ProgressWriters)
        save.UpdateProgress(_progress.PlayerProgress);

      File.WriteAllText(PersistantProgressFilePath, _progress.PlayerProgress.ToJson());
    }

    public void Load()
    {
      foreach (ILoad load in ProgressReaders)
        load.LoadProgress(_progress.PlayerProgress);
    }

    public PlayerProgress UploadProgress()
    {
      if (File.Exists(PersistantProgressFilePath) == false)
      {
        return null;
      }
      return File.ReadAllText(PersistantProgressFilePath).FromJson<PlayerProgress>();
    }
  }
}