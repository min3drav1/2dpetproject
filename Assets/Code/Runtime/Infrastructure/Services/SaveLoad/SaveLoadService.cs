﻿using System.Collections.Generic;
using Code.Runtime.Infrastructure.Services.PersistantProgress;
using Code.Runtime.Infrastructure.Services.PersistantProgress.Data;
using Code.Runtime.Utility;
using UnityEngine;

namespace Code.Runtime.Infrastructure.Services.SaveLoad
{
  public class SaveLoadService : ISaveLoadService
  {
    private readonly IPersistantProgressService _progress;

    public SaveLoadService(IPersistantProgressService progress)
    {
      _progress = progress;
    }

    public string ProgressKey => Constants.ProgressKey;

    public List<ISave> ProgressWriters { get; } = new();
    public List<ILoad> ProgressReaders { get; } = new();

    public void Save()
    {
      foreach (ISave save in ProgressWriters)
        save.UpdateProgress(_progress.PlayerProgress);

      PlayerPrefs.SetString(ProgressKey, _progress.PlayerProgress.ToJson());
    }

    public void Load()
    {
      foreach (ILoad load in ProgressReaders)
        load.LoadProgress(_progress.PlayerProgress);
    }

    public PlayerProgress UploadProgress()
    {
      return PlayerPrefs.GetString(ProgressKey).FromJson<PlayerProgress>();
    }
  }
}