﻿using System.Collections.Generic;
using Code.Runtime.Infrastructure.Services.SceneGraph;
using Code.Runtime.Logic.PlayerStateMachine;
using Code.Runtime.StaticData;

namespace Code.Runtime.Infrastructure.Services.StaticData
{
  public interface IStaticDataService
  {
    WindowData GetWindow(WindowID windowId);
    List<PurchaseActionData> GetAbilityBuyItems();
    PlayerFsmData GetFsmData();
    LevelCollection GetLevelCollection();
  }
}