﻿using System;
using System.Collections.Generic;
using System.Linq;
using Code.Runtime.Infrastructure.Services.AssetManager;
using Code.Runtime.Infrastructure.Services.SceneGraph;
using Code.Runtime.Logic.PlayerStateMachine;
using Code.Runtime.StaticData;
using Cysharp.Threading.Tasks;
using UnityEngine.AddressableAssets;

namespace Code.Runtime.Infrastructure.Services.StaticData
{
  public class StaticDataService : IStaticDataService, IAsyncPreInitialize
  {
    private readonly IAssetsPath _assetsPath;
    private Dictionary<WindowID, WindowData> _windows;
    private List<PurchaseActionData> _purchaseData;
    private PlayerFsmData _playerFsmData;
    private LevelCollection _levelCollection;

    public StaticDataService(IAssetsPath assetsPath)
    {
      _assetsPath = assetsPath;
    }

    public async UniTask AsyncInitialize()
    {
      IList<WindowData> windowData =
        await Addressables.LoadAssetsAsync<WindowData>(key: _assetsPath.WindowsDataPath, callback: null).Task;
      _windows = windowData.ToDictionary(data => data.WindowID);

      IList<PurchaseActionData> purchaseActionData = await Addressables
        .LoadAssetsAsync<PurchaseActionData>(key: _assetsPath.PurchaseDataPath, callback: null).Task;
      _purchaseData = purchaseActionData.ToList();

      _playerFsmData = await Addressables.LoadAssetAsync<PlayerFsmData>(_assetsPath.PlayerFsmData).Task;
      _levelCollection = await Addressables.LoadAssetAsync<LevelCollection>(_assetsPath.LevelCollectionData).Task;
    }

    public WindowData GetWindow(WindowID windowId)
    {
      if (_windows.TryGetValue(windowId, out WindowData window))
        return window;

      throw new Exception($"Don't find {windowId} in windows static data");
    }

    public List<PurchaseActionData> GetAbilityBuyItems()
    {
      return _purchaseData;
    }

    public PlayerFsmData GetFsmData()
    {
      return _playerFsmData;
    }

    public LevelCollection GetLevelCollection()
    {
      return _levelCollection;
    }
  }
}