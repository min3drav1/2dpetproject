﻿using Code.Runtime.Logic.Loot;

namespace Code.Runtime.Infrastructure.Services.Pools.Gem
{
  public interface IReleaseGemElement
  {
    void Release(LootPiece gem);
  }
}