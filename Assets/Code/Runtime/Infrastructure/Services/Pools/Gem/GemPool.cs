﻿using Code.Runtime.Infrastructure.Services.AssetManager;
using Code.Runtime.Infrastructure.Services.PersistantProgress;
using Code.Runtime.Infrastructure.Services.PersistantProgress.Data;
using Code.Runtime.Logic.Loot;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Code.Runtime.Infrastructure.Services.Pools.Gem
{
  public class GemPool : Pool<LootPiece>, IGemPool
  {
    private readonly IAssetProvider _assetProvider;
    private readonly IAssetsPath _assetsPath;
    private readonly IPersistantProgressService _persistantProgressService;

    private LootPiece _instance;

    private CollectedData CollectionData =>
      _persistantProgressService.PlayerProgress.CollectedData;

    public GemPool(IAssetProvider assetProvider, IAssetsPath assetsPath, IPersistantProgressService persistantProgressService)
    {
      _assetProvider = assetProvider;
      _assetsPath = assetsPath;
      _persistantProgressService = persistantProgressService;
    }

    public async UniTask Initialize()
    {
      if (_instance == null)
      {
        GameObject gameObjectInstance = await _assetProvider.Load<GameObject>(_assetsPath.Gem);
        _instance = gameObjectInstance.GetComponent<LootPiece>();
        base.Initialize(_instance, "GemPool");
      }
    }

    public LootPiece GetGem(Vector3 position, Quaternion rotation)
    {
      return base.GetElement(position, rotation);
    }

    public void Release(LootPiece gem)
    {
      base.ReleaseElement(gem);
    }
    
    public override void CleanUp()
    {
      _assetProvider.Release(_assetsPath.Gem);
      base.CleanUp();
      _instance = null;
    }

    protected override LootPiece CreateElement()
    {
      LootPiece lootPiece = base.CreateElement();
      lootPiece.Initialize(CollectionData, this);
      return lootPiece;
    }

    protected override void OnTakeFromPool(LootPiece element)
    {
      base.OnTakeFromPool(element);
      element.ResetPicked();
    }
  }
}