﻿using Code.Runtime.Logic.Loot;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Code.Runtime.Infrastructure.Services.Pools.Gem
{
  public interface IGemPool : IReleaseGemElement
  {
    UniTask Initialize();
    LootPiece GetGem(Vector3 position, Quaternion rotation);
    void CleanUp();
  }
}