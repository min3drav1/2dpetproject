﻿using UnityEngine;
using UnityEngine.Pool;

namespace Code.Runtime.Infrastructure.Services.Pools
{
  public class Pool<TElement> where TElement : MonoBehaviour
  {
    private ObjectPool<TElement> _pool;
    private TElement _instance;
    private Transform _poolParent;

    public void Initialize(TElement instance, string name)
    {
      _poolParent = (new GameObject(name)).transform;
      Object.DontDestroyOnLoad(_poolParent.gameObject);
      _pool = new ObjectPool<TElement>(CreateElement, OnTakeFromPool, OnReturnToPool, OnDestroyElement);
      _instance = instance;
    }

    public virtual TElement GetElement(Vector3 position, Quaternion rotation)
    {
      TElement gameObject = _pool.Get();
      gameObject.transform.position = position;
      gameObject.transform.rotation = rotation;
      return gameObject;
    }

    public virtual void ReleaseElement(TElement element)
    {
      _pool.Release(element);
    }

    public virtual void CleanUp()
    {
      _pool.Clear();
      Object.Destroy(_poolParent.gameObject);
    }

    protected virtual TElement CreateElement()
    {
      return Object.Instantiate(_instance, _poolParent);
    }

    protected virtual void OnTakeFromPool(TElement element)
    {
      element.gameObject.SetActive(true);
    }

    protected virtual void OnReturnToPool(TElement element)
    {
      element.gameObject.SetActive(false);
    }

    protected virtual void OnDestroyElement(TElement element)
    {
      Object.Destroy(element.gameObject);
    }
  }
}