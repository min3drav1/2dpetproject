﻿using System;
using UnityEngine;

namespace Code.Runtime.Infrastructure.Services.SceneGraph
{
  [CreateAssetMenu(fileName = "LevelCollection", menuName = "LevelCollection")]
  public class LevelCollection : ScriptableObject
  {
    [SerializeField] private LevelData[] _levelData;

    public LevelData GetLevelDataBySceneName(string sceneName)
    {
      int levelDataIndex = Array.FindIndex(_levelData, data => data.SceneName == sceneName);
      if (levelDataIndex == -1)
      {
        throw new ArgumentException($"Cant find level data with name {sceneName}");
      }

      return _levelData[levelDataIndex];
    }
  }
}