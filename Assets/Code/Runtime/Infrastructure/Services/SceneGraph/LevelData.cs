﻿using System;
using System.Collections.Generic;
using Code.Attribute;
using Code.EditorHelper.Attribute;
using Code.Runtime.Logic.Spawner;
using UnityEngine;

namespace Code.Runtime.Infrastructure.Services.SceneGraph
{
  [Serializable]
  public class LevelData
  {
#if UNITY_EDITOR
    [ReadOnly] public bool IsLoaded;
#endif
    [field: SerializeField, SceneName] public string SceneName { get; private set; }
    [field: SerializeField, SceneName] public string[] NeighborScenes { get; private set; }

    public bool IsHasSpawnPoint;
    public Vector3 SpawnPoint;
    public List<EnemySpawnData> EnemySpawnData;
  }
}