﻿using System.Collections.Generic;

namespace Code.Runtime.Infrastructure.Services.SceneGraph
{
  public interface ISceneGraphService
  {
    IReadOnlyList<LevelData> SetCurrentLevel(string sceneName);
    void UpdateCurrentLevel(string sceneName, in List<LevelData> added, in List<LevelData> removed);
    LevelData CurrentLevel { get; }
    void CleanUp();
  }
}