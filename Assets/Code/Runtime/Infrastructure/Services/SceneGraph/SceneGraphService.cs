﻿using System.Collections.Generic;
using System.Linq;
using Code.Runtime.Infrastructure.Services.StaticData;

namespace Code.Runtime.Infrastructure.Services.SceneGraph
{
  public class SceneGraphService : ISceneGraphService, IInitialize
  {
    private readonly List<LevelData> _activeLevels = new List<LevelData>(6);
    private LevelCollection _levelCollection;
    private IStaticDataService _staticDataService;

    public LevelData CurrentLevel { get; private set; }

    public SceneGraphService(IStaticDataService staticDataService)
    {
      _staticDataService = staticDataService;
    }

    public void Initialize()
    {
      _levelCollection = _staticDataService.GetLevelCollection();
    }

    public IReadOnlyList<LevelData> SetCurrentLevel(string sceneName)
    {
      CurrentLevel = _levelCollection.GetLevelDataBySceneName(sceneName);
      _activeLevels.Add(CurrentLevel);
      foreach (string neighborScene in CurrentLevel.NeighborScenes)
      {
        _activeLevels.Add(_levelCollection.GetLevelDataBySceneName(neighborScene));
      }

#if UNITY_EDITOR
      foreach (LevelData levelData in _activeLevels)
      {
        levelData.IsLoaded = true;
      }
#endif
      return _activeLevels;
    }

    public void UpdateCurrentLevel(string sceneName, in List<LevelData> added, in List<LevelData> removed)
    {
      if (CurrentLevel.SceneName == sceneName)
      {
        return;
      }
      
      CurrentLevel = _activeLevels.Find(levelData => levelData.SceneName == sceneName);
      
      for (int i = 0; i < _activeLevels.Count;)
      {
        LevelData levelData = _activeLevels[i];
        if (levelData != CurrentLevel && IsNeighborLevel(levelData) == false)
        {
          _activeLevels.Remove(levelData);
          removed.Add(levelData);

#if UNITY_EDITOR
          levelData.IsLoaded = false;
#endif
        }
        else
        {
          ++i;
        }
      }

      foreach (string neighborScene in CurrentLevel.NeighborScenes)
      {
        if (_activeLevels.Any(levelData => levelData.SceneName == neighborScene) == false)
        {
          LevelData levelData = _levelCollection.GetLevelDataBySceneName(neighborScene);
          _activeLevels.Add(levelData);
          added.Add(levelData);
#if UNITY_EDITOR
          levelData.IsLoaded = true;
#endif
        }
      }
    }

    public void CleanUp()
    {
#if UNITY_EDITOR
      foreach (LevelData activeLevel in _activeLevels)
      {
        activeLevel.IsLoaded = false;
      }
#endif
      
      _activeLevels.Clear();
      CurrentLevel = null;
    }

    private bool IsNeighborLevel(LevelData levelData)
    {
      return CurrentLevel.NeighborScenes.Any(sceneName => sceneName == levelData.SceneName);
    }
  }
}