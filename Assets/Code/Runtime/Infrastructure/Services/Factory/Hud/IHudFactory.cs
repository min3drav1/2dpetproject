﻿using Code.Runtime.Logic.Player;
using Cysharp.Threading.Tasks;

namespace Code.Runtime.Infrastructure.Services.Factory.Hud
{
  public interface IHudFactory
  {
    Runtime.UI.Hud HUD { get; }
    UniTask<Runtime.UI.Hud> CreateHud(PlayerHealth health);
    void ClearHud();
  }
}