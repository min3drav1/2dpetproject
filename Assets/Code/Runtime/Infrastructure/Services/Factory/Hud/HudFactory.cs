﻿using System;
using Code.Runtime.Infrastructure.Services.AssetManager;
using Code.Runtime.Infrastructure.Services.Input;
using Code.Runtime.Infrastructure.Services.PersistantProgress;
using Code.Runtime.Infrastructure.Services.Windows;
using Code.Runtime.Logic.Player;
using Cysharp.Threading.Tasks;

namespace Code.Runtime.Infrastructure.Services.Factory.Hud
{
  public class HudFactory : IHudFactory
  {
    private readonly IAssetProvider _assetProvider;
    private readonly IAssetsPath _assetPath;
    private readonly IWindowsService _windowsService;
    private readonly IInputSystem _input;
    private readonly IPersistantProgressService _persistant;
    
    public Runtime.UI.Hud HUD { get; private set; }

    public HudFactory(IAssetProvider assetProvider,
      IAssetsPath assetPath,
      IWindowsService windowsService,
      IInputSystem input, 
      IPersistantProgressService persistant)
    {
      _windowsService = windowsService;
      _assetProvider = assetProvider;
      _assetPath = assetPath;
      _input = input;
      _persistant = persistant;
    }

    public async UniTask<Runtime.UI.Hud> CreateHud(PlayerHealth health)
    {
      if(HUD != null)
      {
        throw new Exception("Hud already created");
      } 
      
      HUD = await _assetProvider.Instantiate<Runtime.UI.Hud>(_assetPath.Hud);
      HUD.Constructor(_windowsService, _input, health, _persistant.PlayerProgress.CollectedData);
      return HUD;
    }

    public void ClearHud()
    {
      if (HUD != null)
      {
        _assetProvider.Release(HUD.gameObject);
        HUD = null;
      }
    }

  }
}