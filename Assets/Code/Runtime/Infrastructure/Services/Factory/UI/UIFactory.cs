﻿using System;
using Code.Runtime.Infrastructure.Services.AssetManager;
using Code.Runtime.Infrastructure.Services.PersistantProgress;
using Code.Runtime.Infrastructure.Services.StaticData;
using Code.Runtime.Infrastructure.StateMachine;
using Code.Runtime.Purchase;
using Code.Runtime.StaticData;
using Code.Runtime.UI.Window;
using Cysharp.Threading.Tasks;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Code.Runtime.Infrastructure.Services.Factory.UI
{
  public class UIFactory : IUIFactory
  {
    private readonly IAssetProvider _assetProvider;
    private readonly IAssetsPath _assetsPath;
    private readonly IPersistantProgressService _persistantProgressService;
    private readonly IPurchaseService _purchaseService;
    private readonly IGameStateMachine _stateMachine;
    private readonly IStaticDataService _staticData;

    private Transform _uiRoot;

    public UIFactory(
      IAssetProvider assetProvider,
      IAssetsPath assetsPath,
      IStaticDataService staticData,
      IPersistantProgressService persistantProgressService,
      IPurchaseService purchaseService,
      IGameStateMachine stateMachine)
    {
      _assetProvider = assetProvider;
      _assetsPath = assetsPath;
      _staticData = staticData;
      _persistantProgressService = persistantProgressService;
      _purchaseService = purchaseService;
      _stateMachine = stateMachine;
    }

    public async UniTask CreateUIRoot()
    {
      if (_uiRoot != null)
      {
        return;
      }
      GameObject gameObject = await _assetProvider.Instantiate(_assetsPath.UIRoot);
      _uiRoot = gameObject.transform;
    }
    
    public BaseWindow Create(WindowID windowID)
    {
      switch (windowID)
      {
        case WindowID.Unknown:
          throw new ArgumentException("window id is Unknown");
        case WindowID.Abilities:
          return OpenPurchaseWindow();
        case WindowID.GameOver:
          return OpenGameOverWindow();
        case WindowID.Menu:
          return OpenMenuWindow();
        default:
          throw new ArgumentOutOfRangeException(nameof(windowID), windowID, null);
      }
    }

    private BaseWindow OpenPurchaseWindow()
    {
      PurchaseWindow window = CreateWindow<PurchaseWindow>(WindowID.Abilities);
      window.Initialize(_staticData, _persistantProgressService.PlayerProgress, _purchaseService);
      return window;
    }

    private BaseWindow OpenGameOverWindow()
    {
      GameOverWindow window = CreateWindow<GameOverWindow>(WindowID.GameOver);
      window.Constructor(_stateMachine);
      return window;
    }

    private BaseWindow OpenMenuWindow()
    {
      MenuWindow window = CreateWindow<MenuWindow>(WindowID.Menu);
      return window;
    }

    public void Release(BaseWindow window)
    {
      Object.Destroy(window.gameObject);
    }

    private TWindow CreateWindow<TWindow>(WindowID windowID) where TWindow : BaseWindow
    {
      WindowData windowData = _staticData.GetWindow(windowID);
      return Object.Instantiate(windowData.WindowPrefab, parent: _uiRoot) as TWindow;
    }
  }
}