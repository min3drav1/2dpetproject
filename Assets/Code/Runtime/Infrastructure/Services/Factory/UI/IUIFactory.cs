﻿using Code.Runtime.StaticData;
using Code.Runtime.UI.Window;
using Cysharp.Threading.Tasks;

namespace Code.Runtime.Infrastructure.Services.Factory.UI
{
  public interface IUIFactory
  {
    UniTask CreateUIRoot();
    void Release(BaseWindow window);
    BaseWindow Create(WindowID windowID);
  }
}