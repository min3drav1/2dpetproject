﻿using Code.Runtime.Infrastructure.Services.AssetManager;
using Code.Runtime.Infrastructure.Services.Input;
using Code.Runtime.Infrastructure.Services.SaveLoad;
using Code.Runtime.Infrastructure.Services.SceneGraph;
using Code.Runtime.Infrastructure.Services.StaticData;
using Code.Runtime.Infrastructure.StateMachine;
using Code.Runtime.Logic.Player;
using Code.Runtime.Logic.PlayerStateMachine;
using Code.Runtime.Logic.PlayerStateMachine.States;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Code.Runtime.Infrastructure.Services.Factory.Player
{
  public sealed class PlayerFactory : IPlayerFactory, IPlayerRegistry
  {
    private readonly IAssetProvider _assetProvider;
    private readonly IAssetsPath _assetPath;
    private readonly ISaveLoadService _saveLoadService;
    private readonly IInputSystem _inputSystem;
    private readonly IGameStateMachine _stateMachine;
    private readonly IStaticDataService _staticDataService;
    private readonly ISceneGraphService _sceneGraphService;

    public PlayerFactory(IAssetProvider assetProvider,
      IAssetsPath assetPath,
      ISaveLoadService saveLoadService,
      IInputSystem inputSystem,
      IGameStateMachine stateMachine,
      IStaticDataService staticDataService,
      ISceneGraphService sceneGraphService)
    {
      _assetProvider = assetProvider;
      _assetPath = assetPath;
      _saveLoadService = saveLoadService;
      _stateMachine = stateMachine;
      _inputSystem = inputSystem;
      _staticDataService = staticDataService;
      _sceneGraphService = sceneGraphService;
    }

    public PlayerCharacter Player { get; private set; }
    

    public async UniTask<PlayerCharacter> Create()
    {
      if (Player != null)
      {
        Player.Death.Reset();
        return Player;
      }      
      Player = await _assetProvider.Instantiate<PlayerCharacter>(_assetPath.Player);

      RegistrySaveLoadProgress(Player.gameObject);
      Player.gameObject.name = "PLAYER";
        
      Player.SetInput(_inputSystem);
      Player.Death.Initialize(_stateMachine);
      InitializeFsm(Player.StateMachine);
      Player.ProgressSaveLoad.Initialize(_sceneGraphService);

      return Player;
    }

    public void Clear()
    {
      if (Player == null)
        return;
      Unregister(Player.gameObject);
      _assetProvider.ReleaseInstance(Player.gameObject);
      Player = null;
    }
    
    private void RegistrySaveLoadProgress(GameObject gameObject)
    {
      RegisterSaveProgress(gameObject);
      LoadProgress(gameObject);

    }
    
    private void Unregister(GameObject registered)
    {
      foreach (ILoad load in registered.GetComponentsInChildren<ILoad>())
        _saveLoadService.ProgressReaders.Remove(load);

      foreach (ISave save in registered.GetComponentsInChildren<ISave>())
        _saveLoadService.ProgressWriters.Remove(save);
    }

    private void InitializeFsm(PlayerFsm playerFsm)
    {
      PlayerFsmData fsmData = _staticDataService.GetFsmData();
      State[] states = FsmBuilder.DefaultStates(Player, _inputSystem, fsmData);
      playerFsm.Initialize(states);
      Player.StateMachine.Launch(StateID.Move);
    }

    private void RegisterSaveProgress(GameObject instantiate)
    {
      foreach (ISave save in instantiate.GetComponentsInChildren<ISave>())
        _saveLoadService.ProgressWriters.Add(save);
    }

    private void LoadProgress(GameObject instantiate)
    {
      foreach (ILoad load in instantiate.GetComponentsInChildren<ILoad>())
        _saveLoadService.ProgressReaders.Add(load);
    }
  }
}