﻿using Code.Runtime.Logic.Player;

namespace Code.Runtime.Infrastructure.Services.Factory.Player
{
  public interface IPlayerRegistry
  {
    PlayerCharacter Player { get; }
  }
}