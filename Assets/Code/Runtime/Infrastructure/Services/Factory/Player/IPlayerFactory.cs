﻿using Code.Runtime.Logic.Player;
using Cysharp.Threading.Tasks;

namespace Code.Runtime.Infrastructure.Services.Factory.Player
{
  public interface IPlayerFactory
  {
    UniTask<PlayerCharacter> Create();
    void Clear();
  }
}