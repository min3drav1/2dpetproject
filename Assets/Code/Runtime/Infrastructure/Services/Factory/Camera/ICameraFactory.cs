﻿using Cinemachine;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Code.Runtime.Infrastructure.Services.Factory.Camera
{
  public interface ICameraFactory
  {
    CinemachineVirtualCamera Camera { get; }
    UniTask<CinemachineVirtualCamera> CreateCamera(Transform follow);
  }
}