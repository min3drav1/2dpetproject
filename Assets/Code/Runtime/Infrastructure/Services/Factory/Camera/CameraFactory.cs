﻿using System;
using Cinemachine;
using Code.Runtime.Infrastructure.Services.AssetManager;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Code.Runtime.Infrastructure.Services.Factory.Camera
{
  public class CameraFactory : ICameraFactory
  {
    private const string CameraBounds = "CameraBounds";
    private readonly IAssetProvider _assetProvider;
    private readonly IAssetsPath _assetPath;
    public CinemachineVirtualCamera Camera { get; private set; }
    
    public CameraFactory(IAssetProvider assetProvider,
      IAssetsPath assetPath)
    {
      _assetPath = assetPath;
      _assetProvider = assetProvider;
    }
    
    public async UniTask<CinemachineVirtualCamera> CreateCamera(Transform follow)
    {
      if (Camera != null)
      {
        throw new Exception("Camera already created");
      }

      Camera = await _assetProvider.Instantiate<CinemachineVirtualCamera>(_assetPath.Camera);
      Camera.Follow = follow;
      PolygonCollider2D polygonCollider2D = GameObject.Find(CameraBounds).GetComponent<PolygonCollider2D>();
      Camera.GetComponent<CinemachineConfiner2D>().m_BoundingShape2D = polygonCollider2D;
      return Camera;
    }

  }
}