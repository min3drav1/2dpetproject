﻿using System.Collections.Generic;
using Code.Runtime.Logic.Spawner;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Code.Runtime.Infrastructure.Services.Factory.Enemy
{
  public interface IEnemyFactory
  {
    UniTask<GameObject> SpawnEnemy(EnemyId enemyId, Vector3 position, List<Vector2> patrolPoints);
  }
}