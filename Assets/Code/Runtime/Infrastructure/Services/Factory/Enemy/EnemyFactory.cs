﻿using System;
using System.Collections.Generic;
using Code.BehaviorTree.Runtime;
using Code.Runtime.Infrastructure.Services.AssetManager;
using Code.Runtime.Infrastructure.Services.Pools.Gem;
using Code.Runtime.Logic.Enemy;
using Code.Runtime.Logic.Loot;
using Code.Runtime.Logic.Spawner;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Code.Runtime.Infrastructure.Services.Factory.Enemy
{
  public sealed class EnemyFactory : IEnemyFactory
  {
    private readonly IGemPool _gemPool;
    private readonly IAssetProvider _assetProvider;
    private readonly IAssetsPath _assetPath;

    public EnemyFactory(IAssetProvider assetProvider,
      IAssetsPath assetPath,
      IGemPool gemPool)
    {
      _assetPath = assetPath;
      _assetProvider = assetProvider;
      _gemPool = gemPool;
    }

    public async UniTask<GameObject> SpawnEnemy(EnemyId enemyId, Vector3 position, List<Vector2> patrolPoints)
    {
      BehaviourRunner behaviourRunner = null; 
      switch (enemyId)
      {
        case EnemyId.None:
          return null;
        case EnemyId.Frog:
          behaviourRunner = await CreateEnemy<BehaviourRunner>(_assetPath.Frog, position);
          break;
        case EnemyId.Opossum:
          behaviourRunner = await CreateEnemy<BehaviourRunner>(_assetPath.Opossum, position);
          break;
        default:
          throw new ArgumentOutOfRangeException(nameof(enemyId), enemyId, null);
      }
      
      behaviourRunner.Blackboard.Set("PatrolPoints", patrolPoints);
      behaviourRunner.Run();
      return behaviourRunner.gameObject;
    }
    
    private async UniTask<TComponent> CreateEnemy<TComponent>(string path, Vector3 position) where TComponent : MonoBehaviour
    {
      TComponent enemy = await _assetProvider.Instantiate<TComponent>(path, position);
      if (enemy.TryGetComponent(out LootSpawner lootSpawner))
      {
        lootSpawner.Initialize(_gemPool);
      }
      if (enemy.TryGetComponent(out DeathEnemy enemyDeath))
      {
        enemyDeath.Initialize(_assetProvider);
      }
      return enemy;
    }
  }
}