﻿using Cysharp.Threading.Tasks;

namespace Code.Runtime.Infrastructure.Services
{
  public interface IAsyncPreInitialize
  {
    UniTask AsyncInitialize();
  }
}