﻿using UnityEngine;

namespace Code.Runtime.Infrastructure.Services.AssetManager
{
  public interface IReleaseAsset
  {
    void Release(GameObject gameObject);
    void Release(string key);
    void CleanUp();
  }
}