﻿namespace Code.Runtime.Infrastructure.Services.AssetManager
{
  public class AssetsPath : IAssetsPath
  {
    public string WindowsDataPath => "WindowData";
    public string PurchaseDataPath => "AbilityBuy";
    public string LevelCollectionData => "LevelCollection";
    public string PlayerFsmData => "PlayerFsmData";
    public string Player => "Player";
    public string Camera => "Cinemachine";
    public string Hud => "Hud";
    public string Frog => "Frog";
    public string Opossum => "Opossum";
    public string UIRoot => "UIRoot";
    public string Gem => "Gem";
  }
}