﻿using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Code.Runtime.Infrastructure.Services.AssetManager
{
  public interface IAssetProvider : IReleaseAsset
  {
    UniTask Initialize();
    UniTask<TComponent> Load<TComponent>(AssetReference assetReference) where TComponent : class;
    UniTask<TComponent> Load<TComponent>(string path) where TComponent : class;

    UniTask<GameObject> Instantiate(string path, Vector3 position = default, Quaternion rotation = default,
      Transform parent = null);

    UniTask<TComponent> Instantiate<TComponent>(string path, Vector3 position = default, Quaternion rotation = default,
      Transform parent = null) where TComponent : MonoBehaviour;

    UniTask<GameObject> Instantiate(AssetReference assetReference, Vector3 position = default,
      Quaternion rotation = default,
      Transform parent = null);

    UniTask<TComponent> Instantiate<TComponent>(AssetReference assetReference, Vector3 position = default,
      Quaternion rotation = default, Transform parent = null) where TComponent : MonoBehaviour;

    void ReleaseInstance(GameObject gameObject);
  }
}