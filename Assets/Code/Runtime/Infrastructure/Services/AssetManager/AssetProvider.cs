﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Code.Runtime.Infrastructure.Services.AssetManager
{
  public class AssetProvider : IAssetProvider
  {
    private readonly Dictionary<string, AsyncOperationHandle> _operationHandleCache = new();

    public async UniTask Initialize()
    {
      AsyncOperationHandle<IResourceLocator> handle = Addressables.InitializeAsync();
      await handle.Task;
    }
    
    public async UniTask<TComponent> Load<TComponent>(AssetReference assetReference) where TComponent : class
    {
      string key = assetReference.AssetGUID;
      if (_operationHandleCache.TryGetValue(key, out AsyncOperationHandle operationHandle))
      {
        return operationHandle.Task as TComponent;
      }

      return await RunWithCacheOnCompleted(Addressables.LoadAssetAsync<TComponent>(assetReference), key);
    }

    public async UniTask<TComponent> Load<TComponent>(string path) where TComponent : class
    {
      if (_operationHandleCache.TryGetValue(path, out AsyncOperationHandle operationHandle))
      {
        return operationHandle.Task as TComponent;
      }

      return await RunWithCacheOnCompleted(Addressables.LoadAssetAsync<TComponent>(path), path);
    }

    public async UniTask<GameObject> Instantiate(string path, Vector3 position = default, Quaternion rotation = default, Transform parent = null)
    {
      return await Addressables.InstantiateAsync(path, position, rotation, parent).Task;
    }

    public async UniTask<TComponent> Instantiate<TComponent>(string path, Vector3 position = default, Quaternion rotation = default, Transform parent = null) where TComponent : MonoBehaviour
    {
      UniTask<GameObject> task = Instantiate(path, position, rotation, parent);
      GameObject gameObject = await task;
      if (gameObject.TryGetComponent(out TComponent component))
      {
        return component;
      }

      Debug.LogError($"Cant find {typeof(TComponent)} component in {path} object");

      return null;
    }

    public async UniTask<GameObject> Instantiate(AssetReference assetReference, Vector3 position = default, Quaternion rotation = default, Transform parent = null)
    {
      return await Addressables.InstantiateAsync(assetReference, position, rotation, parent).Task;
    }

    public async UniTask<TComponent> Instantiate<TComponent>(AssetReference assetReference, Vector3 position = default, Quaternion rotation = default, Transform parent = null) where TComponent : MonoBehaviour
    {
      UniTask<GameObject> task = Instantiate(assetReference, position, rotation, parent);
      GameObject gameObject = await task;
      if (gameObject.TryGetComponent(out TComponent component))
      {
        return component;
      }

      Debug.LogError($"Cant find {typeof(TComponent)} component in {assetReference.RuntimeKey} object");

      return null;
    }

    public void CleanUp()
    {
      foreach (AsyncOperationHandle handle in _operationHandleCache.Values)
      {
        Addressables.Release(handle);
      }

      _operationHandleCache.Clear();
    }

    public void Release(string key)
    {
      if (_operationHandleCache.TryGetValue(key, out AsyncOperationHandle handle) == false)
      {
        Debug.LogError($"Cant find AsyncOperationHandle by key {key}");
        return;
      }
      
      Addressables.Release(handle);
      _operationHandleCache.Remove(key);
    }


    public void Release(GameObject gameObject)
    {
      Addressables.ReleaseInstance(gameObject);
    }

    public void ReleaseInstance(GameObject gameObject)
    {
      Addressables.ReleaseInstance(gameObject);
    }

    
    private async UniTask<TComponent> RunWithCacheOnCompleted<TComponent>(AsyncOperationHandle<TComponent> handle, string path) where TComponent : class
    {
      handle.Completed += completeHandle => { _operationHandleCache[path] = completeHandle; };
      try
      {
        TComponent result = await handle.Task;
        return result;
      }
      catch (Exception exception)
      {
        Debug.Log(exception);
        throw;
      }
    }
  }
}