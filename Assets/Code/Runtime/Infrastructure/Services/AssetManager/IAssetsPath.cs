﻿namespace Code.Runtime.Infrastructure.Services.AssetManager
{
  public interface IAssetsPath
  {
    string Player { get; }
    string Camera { get; }
    string Hud { get; }
    string Frog { get; }
    string Opossum { get; }
    string WindowsDataPath { get; }
    string UIRoot { get; }
    string PurchaseDataPath { get; }
    string Gem { get; }
    string PlayerFsmData { get; }
    string LevelCollectionData { get; }
  }
}