﻿using System;

namespace Code.Runtime.Infrastructure.Services.PersistantProgress.Data
{
  [Serializable]
  public class PositionOnLevel
  {
    public string LevelName;
    public Vector2Data Position;
  }
}