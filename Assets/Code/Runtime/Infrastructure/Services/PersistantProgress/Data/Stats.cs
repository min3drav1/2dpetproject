﻿using System;
using Code.Runtime.Logic.PlayerStateMachine;

namespace Code.Runtime.Infrastructure.Services.PersistantProgress.Data
{
  [Serializable]
  public class Stats
  {
    public int CurrentHp = 3;
    public int MaxHp = 3;
    
    public int MaxJumpInAir = 1;
    public int MaxDashCount = 1;
    public StateID ActiveStates = StateID.Move | StateID.Fall | StateID.Jump | StateID.Crouch | StateID.AirJump | StateID.Dash;
  }
}