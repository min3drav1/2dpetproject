﻿using System;

namespace Code.Runtime.Infrastructure.Services.PersistantProgress.Data
{
  [Serializable]
  public class PlayerProgress
  {
    public PositionOnLevel PositionOnLevel;
    public CollectedData CollectedData;
    public Stats Stats;

    public PlayerProgress()
    {
      PositionOnLevel = new PositionOnLevel();
      CollectedData = new CollectedData();
      Stats = new Stats();
    }
  }
}