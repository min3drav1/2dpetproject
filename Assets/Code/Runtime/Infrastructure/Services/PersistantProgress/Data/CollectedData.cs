﻿using System;

namespace Code.Runtime.Infrastructure.Services.PersistantProgress.Data
{
  [Serializable]
  public class CollectedData
  {
    public int Gems;
    public event Action Changed;

    public void ChangeGemCountAt(int value)
    {
      Gems += value;
      Changed?.Invoke();
    }
  }
}