﻿using System;

namespace Code.Runtime.Infrastructure.Services.PersistantProgress.Data
{
  [Serializable]
  public class Vector3Data
  {
    public float X;
    public float Y;
    public float Z;

    public Vector3Data(float x, float y, float z)
    {
      X = x;
      Y = y;
      Z = z;
    }
  }

  [Serializable]
  public class Vector2Data
  {
    public float X;
    public float Y;

    public Vector2Data(float x, float y)
    {
      X = x;
      Y = y;
    }
  }
}