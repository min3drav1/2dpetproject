﻿using Code.Runtime.Infrastructure.Services.PersistantProgress.Data;

namespace Code.Runtime.Infrastructure.Services.PersistantProgress
{
  public interface IPersistantProgressService
  {
    PlayerProgress PlayerProgress { get; set; }
  }
}