﻿using Code.Runtime.Infrastructure.Services.PersistantProgress.Data;

namespace Code.Runtime.Infrastructure.Services.PersistantProgress
{
  public class PersistantProgressService : IPersistantProgressService
  {
    public PlayerProgress PlayerProgress { get; set; }
  }
}