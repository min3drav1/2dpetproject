﻿namespace Code.Runtime.Utility
{
  public static class LayerUtility
  {
    public static bool LayerMashIncludeLayer(int layerMask, int layer)
    {
      return ((1 << layer) & layerMask) != 0;
    }

    public static bool LayerMashNotIncludeLayer(int layerMask, int layer)
    {
      return ((1 << layer) & layerMask) == 0;
    }
  }
}