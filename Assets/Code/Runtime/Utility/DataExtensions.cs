﻿using Code.Runtime.Infrastructure.Services.PersistantProgress.Data;
using UnityEngine;

namespace Code.Runtime.Utility
{
  public static class DataExtensions
  {
    public static Vector3Data AsVector3Data(this Vector3 vector3)
    {
      return new Vector3Data(vector3.x, vector3.y, vector3.z);
    }

    public static Vector3 AsUnityVector(this Vector3Data vector2)
    {
      return new Vector3(vector2.X, vector2.Y, vector2.Z);
    }

    public static Vector2Data AsVector2Data(this Vector2 vector2)
    {
      return new Vector2Data(vector2.x, vector2.y);
    }

    public static Vector2 AsUnityVector(this Vector2Data vector2)
    {
      return new Vector2(vector2.X, vector2.Y);
    }

    public static string ToJson(this object obj)
    {
      return JsonUtility.ToJson(obj);
    }

    public static T FromJson<T>(this string json)
    {
      return JsonUtility.FromJson<T>(json);
    }
  }
}