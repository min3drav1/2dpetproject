﻿using UnityEngine;

namespace Code.Runtime.Utility
{
  public static class PhysicsExtension
  {
    private const float Accuracy = 0.001f;
    private const float SqrAccuracy = Accuracy * Accuracy;
    public static void SetHorizontalVelocity(this Rigidbody2D rigidbody, float xVelocity)
    {
      Vector2 velocity = rigidbody.velocity;
      velocity.x = xVelocity;
      rigidbody.velocity = velocity;
    }

    public static void SetJumpVelocityByHeight(this Rigidbody2D rigidbody, float height)
    {
      Vector2 velocity = rigidbody.velocity;
      velocity.y = Mathf.Sqrt(-2f * height * Physics.gravity.y * rigidbody.gravityScale);
      rigidbody.velocity = velocity;
    }

    public static bool IsNull(this float value)
    {
      return Mathf.Abs(value) < Accuracy;
    }

    public static bool IsNotNull(this float value)
    {
      return Mathf.Abs(value) > Accuracy;
    }

    public static bool IsEqual(float leftValue, float rightValue)
    {
      return Mathf.Abs(rightValue - leftValue) < Accuracy;
    }

    public static bool IsEqual(Vector2 leftValue, Vector2 rightValue)
    {
      return Mathf.Abs(Vector2.SqrMagnitude(rightValue - leftValue)) < SqrAccuracy;
    }
  }
}