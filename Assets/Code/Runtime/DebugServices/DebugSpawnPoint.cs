﻿using Code.BehaviorTree.Runtime;
using Code.Runtime.Logic.Enemy;
using UnityEngine;

namespace Code.Runtime.DebugServices
{
  public class DebugSpawnPoint : MonoBehaviour
  {
    [SerializeField] private BehaviourRunner _behaviourRunner;
    [SerializeField] private PatrolPoints _patrolPoints;

    private void Awake()
    {
      _behaviourRunner.gameObject.SetActive(true);
      _behaviourRunner.Blackboard.Set("PatrolPoints", _patrolPoints.Points);
      _behaviourRunner.Run();
    }
  }
}