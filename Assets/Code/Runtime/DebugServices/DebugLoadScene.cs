﻿using Code.Attribute;
using UnityEngine;

namespace Code.Runtime.DebugServices
{
#if UNITY_EDITOR
  public class DebugLoadScene : MonoBehaviour
  {
    [field:SerializeField] public bool IsLoadDebugScene { get; private set; } = false;
    [field:SerializeField, SceneName] public string DebugScene { get; private set; }
  }
#endif
}