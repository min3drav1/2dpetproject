﻿using UnityEngine;

namespace Code.Runtime.StaticData
{
  [CreateAssetMenu(fileName = "FrogData", menuName = "StaticData/Frog", order = 0)]
  public class FrogData : EnemyData
  {
    [SerializeField] private float _waitTime;
    [SerializeField] private float _speed;

    public float WaitTime => _waitTime;
    public float Speed => _speed;
  }
}