﻿using Code.Runtime.UI.Window;
using UnityEngine;

namespace Code.Runtime.StaticData
{
  [CreateAssetMenu(fileName = "Window", menuName = "StaticData/WindowData")]
  public class WindowData : ScriptableObject
  {
    [field: SerializeField] public WindowID WindowID { get; private set; }
    [field: SerializeField] public BaseWindow WindowPrefab { get; private set; }
  }
}