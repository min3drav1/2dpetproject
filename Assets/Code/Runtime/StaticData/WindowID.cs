﻿namespace Code.Runtime.StaticData
{
  public enum WindowID
  {
    Unknown = 0,
    Abilities = 1,
    GameOver = 2,
    Menu = 3
  }
}