﻿using UnityEngine;

namespace Code.Runtime.StaticData
{
  [CreateAssetMenu(fileName = "Ability", menuName = "StaticData/Ability", order = 0)]
  public class PurchaseActionData : ScriptableObject
  {
    public Sprite Icon;
    public string Text;
    public int Price;
    public PurchaseActionId ActionId;
  }
}