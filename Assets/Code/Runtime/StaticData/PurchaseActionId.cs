﻿namespace Code.Runtime.StaticData
{
  public enum PurchaseActionId
  {
    None = 0,
    AddHeart = 1,
    DoubleJump = 2,
    TripleJump = 3,
    Dash = 4,
  }
}